import {BrowserRouter} from 'react-router-dom'

export const routerDecorator = (Story: any) => (
    <BrowserRouter>
        <Story/>
    </BrowserRouter>
)

function range(start: number, end: number) {
    return Array(end - start + 1)
        .fill('')
        .map((_, idx) => start + idx)
}

export const makeControlForObjectArray = (
    items: any[],
    key: string,
    type = 'select',
    defaultValue: any = null,
) => {
    return {
        options: range(0, items.length - 1),
        mapping: items,
        control: {
            type,
            labels: items.reduce((acumm, value, index) => {
                acumm[index] = value[key]
                return acumm
            }, {}),
        },
        defaultValue,
    }
}

export const makeControlForRender = (item: any, key: string) => ({
    options: Object.keys(item),
    defaultValue: key,
    control: {type: 'select'},
})

export const disableDummyArgTypes = {
    as: {
        table: {
            disable: true,
        },
    },
    forwardedAs: {
        table: {
            disable: true,
        },
    },
    ref: {
        table: {
            disable: true,
        },
    },
    theme: {
        table: {
            disable: true,
        },
    },
}
