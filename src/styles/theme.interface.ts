export type ThemeColorIndex =
    'text' |
    'textButton' |
    'calendarText' |
    'tag' |

    'border' |

    'gray.white' |
    'gray.lightest' |
    'gray.lighten' |
    'gray.light' |
    'gray.normal' |
    'gray.dark' |
    'gray.darken' |
    'gray.darkest' |
    'gray.black' |

    'primary.darkest' |
    'primary.darken' |
    'primary.dark' |
    'primary.normal' |
    'primary.light' |
    'primary.lighten' |
    'primary.lightest' |

    'success.darkest' |
    'success.darken' |
    'success.dark' |
    'success.normal' |
    'success.light' |
    'success.lighten' |
    'success.lightest' |

    'error.darkest' |
    'error.darken' |
    'error.dark' |
    'error.normal' |
    'error.light' |
    'error.lighten' |
    'error.lightest' |

    'info.darkest' |
    'info.darken' |
    'info.dark' |
    'info.normal' |
    'info.light' |
    'info.lighten' |
    'info.lightest' |

    'warn.darkest' |
    'warn.darken' |
    'warn.dark' |
    'warn.normal' |
    'warn.light' |
    'warn.lighten' |
    'warn.lightest' |

    'typographic.solid' |
    'typographic.fade' |

    'card.background' |

    'sidebar.background.normal' |
    'sidebar.background.hover' |
    'sidebar.background.active' |
    'sidebar.background.pressed' |
    'sidebar.text.normal' |
    'sidebar.text.hover' |
    'sidebar.text.active' |

    'bar.background.hover' |
    'bar.background.pressed' |
    'bar.text.normal' |
    'bar.text.hover'

export type FontSize = 'xxs' |
    'xs' |
    'sm' |
    'md' |
    'lg' |
    'xlg'

export interface ConsusColors {
    text: string,
    textButton: string,
    calendarText: string,
    tag: string,

    body: string;

    primary: {
        darkest: string;
        darken: string;
        dark: string;
        normal: string;
        light: string;
        lighten: string;
        lightest: string;
    },

    success: {
        darkest: string;
        darken: string;
        dark: string;
        normal: string;
        light: string;
        lighten: string;
        lightest: string;
    },

    info: {
        darkest: string;
        darken: string;
        dark: string;
        normal: string;
        light: string;
        lighten: string;
        lightest: string;
    },

    error: {
        darkest: string;
        darken: string;
        dark: string;
        normal: string;
        light: string;
        lighten: string;
        lightest: string;
    },

    warn: {
        darkest: string;
        darken: string;
        dark: string;
        normal: string;
        light: string;
        lighten: string;
        lightest: string;
    },

    gray: {
        white: string;
        lightest: string;
        lighten: string;
        light: string;
        normal: string;
        dark: string;
        darken: string;
        darkest: string;
        black: string;
    },

    card: {
        background: string;
    }

    sidebar: {
        background: {
            normal: string;
            hover: string;
            active: string;
            pressed: string;
        },
        text: {
            normal: string;
            hover: string;
            active: string;
        }
    }

    bar: {
        background: {
            hover: string;
            pressed: string;
        },
        text: {
            normal: string;
            hover: string;
        }
    }

    typographic: {
        solid: string,
        fade: string,
    }

    inputs: {
        placeholder: string,
        border: string,
        disabled: string,
    }

    scrollbar: string,
    disabled: string,
    border: string,
    boxShadow: string,
}

export interface ConsusTheme {
    colors: ConsusColors,
    font: {
        size: {
            xxs: number,
            xs: number,
            sm: number,
            md: number,
            lg: number,
            xlg: number,
            xxlg: number,
            xxxlg: number,
        },
        weight: {
            bold: number,
            semiBold: number,
            regular: number,
            light: number,
        }
    },
    inputs: {
        borderRadius: number;
    },
    boxShadow: string,
    transitionDuration: string,
    border: string,
    borderRadius: number,
    spacing: {
        xxs: number,
        xs: number,
        sm: number,
        md: number,
        lg: number,
        xlg: number,
        x1: number,
        x2: number,
        x3: number,
        x4: number,
    }
    breakpoints: {
        mobile: {
            max: number,
        }
    }
}
