export const mobileMediaQuery = ({theme}: any) => `@media (max-width: ${theme.breakpoints.mobile.max}px)`;
