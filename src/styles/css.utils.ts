import {darken} from 'polished';
import {mobileMediaQuery} from "./common";
import {ConsusTheme} from "./theme.interface";
import {css} from "styled-components";

export const cssHorizontalScroll = css`
  overflow-x: auto;
  white-space: nowrap;

  ${mobileMediaQuery} {
    ::-webkit-scrollbar-track {
      display: none;
    }

    &::-webkit-scrollbar {
      display: none;
    }

    &::-webkit-scrollbar-thumb {
      display: none;
    }

    scrollbar-width: none;
    padding-bottom: 0;
  }
`

export const cssScrollBar = ({
                                 size = '8px',
                                 radius = '5px',
                                 color = 'orange',
                                 boxShadow = 'inset 0 0 3px grey',
                             } = {}) => (
    css`
      &::-webkit-scrollbar {
        width: ${size};
        height: ${size};
      }

      &::-webkit-scrollbar-track {
        box-shadow: ${boxShadow};
        border-radius: ${radius}
      }

      &::-webkit-scrollbar-thumb {
        background: ${color};
        border-radius: ${radius}
      }

      &::-webkit-scrollbar-thumb:hover {
        background: ${darken(0.1, color)}
      }
    `
)

export const discreetScrollBar = (theme: ConsusTheme) => cssScrollBar({
    size: '6px',
    boxShadow: 'none',
    color: theme.colors.gray.dark,
})

export const swiperScrollBar = (theme: ConsusTheme) => css`
  ${cssHorizontalScroll};
  ${discreetScrollBar(theme)};
  padding-bottom: ${theme.spacing.xxs}px;
  
  ${mobileMediaQuery} {
    padding-bottom: 0;
  }
`
