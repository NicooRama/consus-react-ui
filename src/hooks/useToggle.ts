import {useState} from 'react';

export const useToggle = (initialState: boolean) => {
    const [value, setValue] = useState(initialState);
    const toggle = () => setValue(!value);
    const turnOn = () => setValue(true);
    const turnOff = () => setValue(false);
    return [value, {toggle, turnOn, turnOff}];
}
