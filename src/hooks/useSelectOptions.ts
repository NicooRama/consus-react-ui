import {useEffect, useState} from "react";
import {ComparatorFn} from "../utils/types";

export const useSelectOptions = (options: any[],
                                 selecteds: any[],
                                 onChange: (selecteds: any[]) => void,
                                 comparator?: ComparatorFn<any>,
                                 ) => {
    const [innerSelecteds, setInnerSelecteds] = useState(selecteds || []);

    comparator = comparator || ((option: any) => ((selected: any) => selected === option));

    useEffect(() => {
        setInnerSelecteds(selecteds);
    }, [JSON.stringify(selecteds)]);

    const handleChange = (i: number) => {
        const updatedSelecteds: any[] = [...innerSelecteds];
        const option = options[i];
        if(innerSelecteds.includes(option)){
            updatedSelecteds.splice(updatedSelecteds.findIndex(u => u === option), 1);
        } else {
            updatedSelecteds.push(option);
        }

        setInnerSelecteds(updatedSelecteds);
        onChange(updatedSelecteds);
    };

    const isSelected = (option: any) => {
        return innerSelecteds.some((comparator as ComparatorFn<any>)(option));
    }

    return {selecteds: innerSelecteds, handleChange, isSelected};
}
