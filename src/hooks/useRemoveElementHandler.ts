import { useState } from "react";
import {useOriginalDimensions} from "./useOriginalDimensions";

export const useRemoveElementHandler = (onRemove: (() => any) | undefined) => {
    const [removing, setRemoving] = useState(false);
    const {ref, originalWidth, originalHeight} = useOriginalDimensions();

    const handleRemove = async () => {
        if(!onRemove) return;
        setRemoving(true);
        try {
            await onRemove();
        } finally {
            setRemoving(false);
        }
    }

    return {
        removing,
        ref,
        originalWidth,
        originalHeight,
        handleRemove
    }
}
