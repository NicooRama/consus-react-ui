import { useEffect } from "react";

export const useOnResize = (callback: (width: number) => void, {fireOnInit}: {fireOnInit?: boolean}) => {
    useEffect(() => {
        if(fireOnInit) {
            callback(window.innerWidth);
        }
    }, [fireOnInit]);

    useEffect(() => {
        const normalizedCallback = () => callback(window.innerWidth);
        window.addEventListener('resize', normalizedCallback);
        return () => window.removeEventListener('resize', normalizedCallback);
    }, [callback]);
}
