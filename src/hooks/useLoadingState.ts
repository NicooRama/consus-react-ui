import {useState} from 'react';

/**
 *
 * @param initialData
 * @param initiaLoading
 * @returns {*[]}
 */
export const useLoadingState = (initialData: any, initiaLoading = true) => {
    const [dataState, setDataState] = useState(initialData);
    const [loading, setLoading] = useState(initiaLoading);

    const setData = (data: any, loading: boolean) => {
        setDataState(data);
        if(loading !== null && loading !== undefined){
            setLoading(loading)
        }
    };

    return [dataState, setData, loading, setLoading];
};
