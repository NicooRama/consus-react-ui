import {useCallback} from "react";
import {queryParamsObjectToString} from "../utils/router.utils";
import {scrollToTop} from "../utils/window.utils";
import {useQueryParams, useQueryParamValue} from "./router.hooks";
import { useHistory } from "react-router-dom";
const onSingleValueFilterChange = (filterKey: string,
                                          filterValueAccessor: string,
                                          unselectEnabled: boolean,
                                          queryParams: any,
                                          history: any,
) => {
    return (filterEntity: any) => {
        //TODO: no funciona para strings
        const updatedQueryParams = {...queryParams};
        if (updatedQueryParams[filterKey] !== filterEntity[filterValueAccessor]) {
            updatedQueryParams[filterKey] = filterEntity[filterValueAccessor];
        } else if(unselectEnabled) {
            delete updatedQueryParams[filterKey];
        }
        history.push({
            search: queryParamsObjectToString(updatedQueryParams)
        });
        scrollToTop();
    }
}


export const useSingleValueFilter = <T>(
    entities: T[],
    filterKey: string,
    filterValueAccessor: string,
    unselectEnabled = true
): [T | undefined, () => (selected: T) => void] => {
    const history = useHistory();
    //@ts-ignore
    const selectedStringValue = useQueryParamValue(filterKey, entities);
    //@ts-ignore
    const selected = entities.find((entity) => entity[filterValueAccessor] === selectedStringValue);
    const queryParams = useQueryParams();

    //TODO: este callback deberia devolver el valor y listo
    const onChange = useCallback(() => {
        return onSingleValueFilterChange(filterKey, filterValueAccessor, unselectEnabled, queryParams, history);
    }, [queryParams])

    return [selected, onChange]
}
