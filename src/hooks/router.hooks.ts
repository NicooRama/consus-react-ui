import {useLocation} from "react-router-dom";
import qs from "qs";

export const useQueryParams = () => {
    const {search} = useLocation();
    return qs.parse(search, { ignoreQueryPrefix: true });
}

export const useQueryParamValue = (value: string) => {
    const search = useQueryParams();
    return search[value];
}

export const useQueryParamArrayValue = (value: string, separator = ',') => {
    const values = useQueryParamValue(value);
    if(!values) {
        return [];
    }
    return (values as string).split(separator);
}
