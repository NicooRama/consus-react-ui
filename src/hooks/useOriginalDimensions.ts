import {MutableRefObject, useEffect, useRef, useState } from "react";

export const useOriginalDimensions = (): {ref: MutableRefObject<any>, originalWidth: number, originalHeight: number} => {
    const ref = useRef(null);
    const [originalWidth, setOriginalWidth] = useState(0);
    const [originalHeight, setOriginalHeight] = useState(0);

    useEffect(() => {
        const width = ref.current ? ((ref.current as any)?.offsetWidth) : 0;
        const height = ref.current ? ((ref.current as any)?.offsetHeight) : 0;
        if(width > 0) {
            setOriginalWidth(width);
        }
        if(height > 0) {
            setOriginalHeight(height);
        }
    }, []);

    return {ref, originalWidth, originalHeight};
}
