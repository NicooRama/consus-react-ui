import {useEffect, useState} from "react";
import {ComparatorFn} from "../utils/types";

export const useSelectOption = (options: any[],
                                selected: any,
                                onChange: (selected: any) => void,
                                comparator?: ComparatorFn<any>,
) => {
    const [innerSelected, setInnerSelected] = useState(selected);

    comparator = comparator || ((option) => ((selected) => selected === option));

    useEffect(() => {
        setInnerSelected(selected);
    }, [selected]);
    const handleChange = (i: number) => {
        const option = options[i];
        if (option === innerSelected) {
            setInnerSelected(undefined);
            onChange(undefined);
            return;
        }
        setInnerSelected(option);
        onChange(option);
    };
    const isSelected = (option: any) => {
        return (comparator as ComparatorFn<any>)(option)(innerSelected);
    };

    return {innerSelected, handleChange, isSelected};
}
