import {useState} from "react";

export const useLoadingOnRemove = (onRemove: () => any) => {
    const [removing,setRemoving] = useState(false);
    const handleOnRemove = async () => {
        setRemoving(true);
        try {
            await onRemove();
        } finally {
            setRemoving(false)
        }
    };

    return [removing, handleOnRemove]
}
