import {useRef} from "react";

export const useFocus = (): [any, () => void] => {
    const htmlElRef = useRef(null)
    const setFocus = () => {htmlElRef.current &&  (htmlElRef.current as any).focus()}

    return [ htmlElRef, setFocus ]
}
