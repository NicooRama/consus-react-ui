import {ObjectUtils} from "@consus/js-utils";

export const queryParamsObjectToString = (queryParams: any, overridePage = true) => {
    if(ObjectUtils.isObjectEmpty(queryParams)) {
        return ''
    }

    if(overridePage) {
        delete queryParams.page;
    }

    const queryParamsString = Object.keys(queryParams).map((key) => `${key}=${queryParams[key]}`).join('&');

    return `?${queryParamsString}`
}

export const getUpdatedQueryParams = (queryParams: any, value: object) => {
    const updatedQueryParams = {...queryParams};
    Object.keys(value).forEach((key) => {
        if (updatedQueryParams[key] !== (value as any)[key]) {
            updatedQueryParams[key] = (value as any)[key];
        } else {
            delete updatedQueryParams[key];
        }
    })
    return updatedQueryParams;
}
