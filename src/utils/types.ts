export type Spacing = 'xxs' | 'xs' | 'sm' | 'md' | 'lg' | 'xlg' | 'x1' | 'x2' | 'x3' | 'x4';
export type Size = 'xxs' | 'xs' | 'sm' | 'md' | 'lg' | 'xlg' | 'xxlg' | 'xxxlg';
export type Weight = 'bold' | 'semiBold' | 'regular';
export type ConsusRender = string | ((item: any) => any);
export type ComparatorFn<T> = (option: T) => (selected: T) => boolean;
