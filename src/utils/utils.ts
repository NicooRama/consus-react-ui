export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

// @ts-ignore
export function getIn(object: any, keys: string | string[], defaultVal?: any) {
    keys = Array.isArray(keys) ? keys : keys.split('.');
    object = object[keys[0]];
    if (object && keys.length > 1) {
        return getIn(object, keys.slice(1), defaultVal);
    }
    return object === undefined ? defaultVal : object;
}
