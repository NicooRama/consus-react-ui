export interface Notifier {
    showError: (message?: string | boolean) => void;
    showSuccess: (message: string) => void;
    showWarning: (message: string) => void;
    showInfo: (message: string) => void;
}
