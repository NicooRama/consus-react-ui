import {toast} from "react-toastify";
import {Notifier} from "./notifier.interface";

export const createToastNotifier = (defaultMessage: string): Notifier => (
    {
        showError: (message?: string | boolean) => toast.error(message || defaultMessage),
        showSuccess: (message: string) => toast.success(message),
        showWarning: (message: string) => toast.warn(message),
        showInfo: (message: string) => toast.info(message),
    }
)
