import React from "react"

import styled from "styled-components";
import {Text} from '../text/Text';

const ColorSquare = styled.div`
  width: 50px;
  height: 50px;
  background-color: ${({color}) => color};
  border-radius: 4px;
  box-shadow: inset 0px 1px 3px #8080806e;;
`

const Container = styled.div`
  display: flex;
  gap: 16px;
`

const ColorSquareContainer = styled.div`
  display: flex;
  gap: 8px;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`

export interface PaletteChooserProps {
    color900: string;
    color800: string;
    color700: string;
    color600: string;
    color500: string;
    color400: string;
    color300: string;
    color200: string;
    color100: string;
    [props: string]: any;
}

export const PaletteChooser: React.FC<PaletteChooserProps> = (props) => {
    const keys = ['color900',
        'color800',
        'color700',
        'color600',
        'color500',
        'color400',
        'color300',
        'color200',
        'color100',
    ];
    const {className, ['data-testid']: dataTestId} = props;
    return (<Container data-testid={dataTestId} className={className}>
        {
            keys.map((key: any) => (
                <ColorSquareContainer key={key}>
                    <ColorSquare color={(props as any)[key] as string}/>
                    <Text size={'sm'}>{key.substring(key.length - 3)}</Text>
                </ColorSquareContainer>
            ))
        }
    </Container>)
};
