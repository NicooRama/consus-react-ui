import { PaletteChooser } from './PaletteChooser'
import { render } from '../../../test/test.utils'

describe('<PaletteChooser />', () => {
  it('renders <PaletteChooser /> successfully', () => {
    const { getByTestId } = render(
      <PaletteChooser
        data-testid={'palette-chooser'}
        color900={'#000000'}
        color800={'#000000'}
        color700={'#000000'}
        color600={'#000000'}
        color500={'#000000'}
        color400={'#000000'}
        color300={'#000000'}
        color200={'#000000'}
        color100={'#000000'}
      />,
    )
    expect(getByTestId('palette-chooser')).toBeInTheDocument()
  })
})
