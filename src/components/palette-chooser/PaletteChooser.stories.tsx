import type {Meta, StoryObj} from '@storybook/react';
import {PaletteChooser} from './PaletteChooser';
import {Text} from "../text/Text";

const meta: any = {
    title: 'Utils/PaletteChooser',
    component: PaletteChooser,
    tags: ['autodocs'],
    decorators: [
        (Story: any) => (
            <div>
                <Text size={'md'} weight={'semiBold'}>Instrucciones</Text>
                <ol>
                    <li>Elegir el 500 y verificar como se ve en el boton de ejemplo</li>
                    <li>Elegir el 900 y el 100</li>
                    <li>Elegir el 700 y el 300</li>
                    <li>Elegir los restantes</li>
                </ol>
                <Story />
            </div>
        )
    ]
} satisfies Meta<typeof PaletteChooser>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
    argTypes: {
        color900: {
            control: 'color',
        },
        color800: {
            control: 'color',
        },
        color700: {
            control: 'color',
        },
        color600: {
            control: 'color',
        },
        color500: {
            control: 'color',
        },
        color400: {
            control: 'color',
        },
        color300: {
            control: 'color',
        },
        color200: {
            control: 'color',
        },
        color100: {
            control: 'color',
        },
    }
};
