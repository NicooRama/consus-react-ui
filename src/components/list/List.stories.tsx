import type {Meta, StoryObj} from '@storybook/react';
import {List} from './List';
import {ListCard} from "../list-card/ListCard";

const meta: any = {
    title: 'List/List',
    component: List,
    tags: ['autodocs'],
} satisfies Meta<typeof List>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Strings: Story = {
    args: {
        items: ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5"],
        onClick: () => {},
    },
};

const objectItems = [
    {
        value: 'item1',
        description: 'Item 1',
    },
    {
        value: 'item2',
        description: 'Item 2',
    },
    {
        value: 'item3',
        description: 'Item 3',
    },
    {
        value: 'item4',
        description: 'Item 4',
    },
    {
        value: 'item5',
        description: 'Item 5',
    },
    {
        value: 'item6',
        description: 'Item 6',
    },
    {
        value: 'item7',
        description: 'Item 7',
    },
    {
        value: 'item8',
        description: 'Item 8',
    },
    {
        value: 'item9',
        description: 'Item 9',
    },
    {
        value: 'item10',
        description: 'Item 10',
    },
    {
        value: 'item11',
        description: 'Item 11',
    },
    {
        value: 'item12',
        description: 'Item 12',
    },
]

export const Objects: Story = {
    args: {
        items: objectItems,
        render: 'description',
        onClick: () => {},
    }
}

export const Paginated: Story = {
    args: {
        items: objectItems,
        paginated: true,
        render: 'description',
        onClick: () => {},
    }
}

export const Cards: Story = {
  args: {
    items: objectItems,
    paginated: true,
    render: (item: any, active: boolean) => (
      <ListCard onClick={() => {}} active={active}>
        {item.description}
      </ListCard>
    ),
  },
}
