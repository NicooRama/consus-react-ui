import type {Meta, StoryObj} from '@storybook/react';
import {ListItems} from './ListItems';

const meta: any = {
    title: 'List/ListItems',
    component: ListItems,
    tags: ['autodocs'],
} satisfies Meta<typeof ListItems>;

export default meta;
type Story = StoryObj<typeof meta>;

const children = <>
    <li>Item 1</li>
    <li>Item 2</li>
    <li>Item 3</li>
    <li>Item 4</li>
    <li>Item 5</li>
</>

export const Unordered: Story = {
    args: {
        children
    },
};

export const Ordered: Story = {
    args: {
        children,
        tag: 'ol'
    },
};
