import React from "react"
import {css} from "styled-components";
import styled from "styled-components";
import {Spacing} from "../../../utils/types";

const styles = (theme: any, spacing: Spacing) => css`
  margin: 0;
  padding: 0 0 0 24px;
  display: flex;
  flex-direction: column;
  gap: ${theme.spacing[spacing]}px;
`

type StyleProps = {
    spacing: Spacing
}

const UnorderedList = styled.ul<StyleProps>`
  ${({theme, spacing}: any) => styles(theme, spacing)}
`

const OrderedList = styled.ol<StyleProps>`
  ${({theme, spacing}: any) => styles(theme, spacing)}
`

export interface ListItemsProps {
    children: any;
    spacing?: Spacing;
    tag?: 'ol' | 'ul'
}

export const ListItems: React.FC<ListItemsProps> = ({children, tag = 'ul', spacing = 'xxs', ...props}) => {
        const innerProps = {
        spacing,
        ...props
    }
    if(tag === 'ul') {
        return <UnorderedList {...innerProps} >{children}</UnorderedList>
    }
    return <OrderedList {...innerProps}>{children}</OrderedList>
};
