import {ListItems} from "./ListItems";
import {render} from "../../../../test/test.utils";

describe('<ListItems />', () => {
    it('renders <ListItems /> successfully', () => {
        const {getByTestId} = render(<ListItems data-testid={'list-items'}><li>Foo</li></ListItems>)
        expect(getByTestId('list-items')).toBeInTheDocument();
    });
});
