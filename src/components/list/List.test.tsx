import {screen} from '@testing-library/react';
import {List} from "./List";
import {render} from "../../../test/test.utils";

describe('<List />', () => {
    it('renders string items successfully', () => {
        render(<List items={["Hello world"]} onClick={jest.fn} />)
        const listItem = screen.getByRole('listitem')
        expect(listItem).toBeInTheDocument();
    })

    it('re-renders string items successfully', () => {
        const {rerender} = render(<List items={[]} onClick={jest.fn}/>)
        rerender(<List items={["Rerendered!"]} onClick={jest.fn}/>);
        const textItem = screen.getByText("Rerendered!");
        expect(textItem).toBeInTheDocument();
    })

    it('renders object items successfully', () => {
        const item = {description: "Hello world"};
        render(<List items={[item]} render={'description'} onClick={jest.fn}/>)
        const listItem = screen.getByRole('listitem')
        const textItem = screen.getByText(item.description);
        expect(listItem).toBeInTheDocument();
        expect(textItem).toBeInTheDocument();
    })

    it('renders component successfully', () => {
        const item = {description: "Hello world"};
        render(<List items={[item]} render={(item) => <div>{item.description}</div>} onClick={jest.fn}/>)
        const listItem = screen.getByRole('listitem')
        const textItem = screen.getByText(item.description);
        expect(listItem).toBeInTheDocument();
        expect(textItem).toBeInTheDocument();
    })

    it('renders empty message because items have length equals 0 successfully', () => {
        const emptyMessage = 'Not elements in list';
        render(<List items={[]} emptyMessage={emptyMessage} onClick={jest.fn}/>)
        const emptyMessageElement = screen.getByText(emptyMessage);
        expect(emptyMessageElement).toBeInTheDocument();
    })

    it('renders empty message react element because items have length equals 0 successfully', () => {
        const emptyMessage = 'Not elements in list';
        render(<List items={[]} emptyMessage={<div>{emptyMessage}</div>} onClick={jest.fn}/>)
        const emptyMessageElement = screen.getByText(emptyMessage);
        expect(emptyMessageElement).toBeInTheDocument();
    })

    it('renders empty message function because items have length equals 0 successfully', () => {
        const emptyMessage = 'Not elements in list';
        const emptyMessageFunc = jest.fn().mockReturnValue(emptyMessage);

        render(<List items={[]} emptyMessage={emptyMessageFunc} onClick={jest.fn}/>)
        const emptyMessageElement = screen.getByText(emptyMessage);

        expect(emptyMessageFunc).toHaveBeenCalled();
        expect(emptyMessageElement).toBeInTheDocument();
    })

    it('set itemSpacing class correctly', () => {
        render(<List items={["Hello world"]} itemSpacing={'lg'} onClick={jest.fn}/>)
        const list = screen.getByRole('list');
        expect(list).toHaveClass('lg')
    })

    it('handles item click successfully', () => {
        const onClick = jest.fn();
        const item = "Hello world";
        render(<List items={[item]} itemSpacing={'lg'} onClick={onClick} />)
        const button = screen.getByRole('button');
        button.click();
        expect(onClick).toBeCalledWith(item);
    })

    it('rendered component recives active flag successfully', () => {
        const item = {description: "Hello world"};
        const renderItem: any = jest.fn((item) => <div>{item.description}</div>);
        render(<List items={[item]} render={renderItem} onClick={jest.fn}/>)
        expect(renderItem).toBeCalledWith(item, false);
    })
})
