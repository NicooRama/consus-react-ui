import {normalizeDisplay, StrUtils} from "@consus/js-utils";
import {ConsusRender, Spacing} from "../../utils/types";
import {LoaderWrapper} from "../loader-wrapper/LoaderWrapper";
import React, {ReactElement, useEffect, useState} from "react";
import {ListPaginationBar, ListPaginationBarProps} from "../list-pagination-bar/ListPaginationBar";
import styled from "styled-components";

const ListContent = styled.div<{spacing: Spacing}>`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: ${({theme, spacing}) => theme.spacing[spacing]}px;
  
  ul {
    grid-gap: ${({theme, spacing}) => theme.spacing[spacing]}px;
  }
`

const Container = styled.div`
  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    display: grid;
    grid-template-columns: 1fr;
  }

  .button-wrapper {
    cursor: pointer;
    width: 100%;
    text-align: inherit
  }
`
export interface ListProps {
    items: any[],
    activeItem?: any,
    onClick?: (item: any) => void,
    render?: ConsusRender,
    emptyMessage?: string | ((values?: any) => any) | ReactElement,
    className?: string,
    itemSpacing?: Spacing,
    paginated?: boolean,
    loading?: boolean,
    paginationProps?: Partial<ListPaginationBarProps>,

    }

//TODO: handlear active item
export const List: React.FC<ListProps> = ({
                         items = [],
                         activeItem,
                         onClick,
                         render,
                         itemSpacing = 'sm',
                         emptyMessage = "No hay elementos en la lista",
                         loading = false,
                         paginated = false,
                         className = "",
                         paginationProps = {},
                         ...props
                     }) => {
    const [renderedItems, setRenderedItems] = useState(paginated ? items.slice(0, paginationProps.pageSize || 5) : items);
    const [pageNumber, setPageNumber] = useState(paginationProps?.initialPage || 0);

    //TODO: ver por que se esta rerendizando cuando cambia un item
    useEffect(() => {
        const pageSize = paginationProps.pageSize || 5
        setRenderedItems(paginated ? items.slice(pageNumber * pageSize, (pageNumber * pageSize) + pageSize) : items);
    }, [items, paginationProps.pageSize])

    render = normalizeDisplay(render) as (item: any) => string;
    const emptyMessageRender = (StrUtils.isString(emptyMessage) || React.isValidElement(emptyMessage) ? () => emptyMessage : emptyMessage) as () => any;

    function renderItem(item: any) {
        return (render as (item: any, active: boolean) => string)(item, activeItem === item)
    }

    const changeItems = (newItems: any[], newPageNumber: number) => {
        setPageNumber(newPageNumber);
        setRenderedItems(newItems)
    };

    return (<Container className={className} {...props}>
        <LoaderWrapper loading={loading} center={true}>
            {
                renderedItems.length > 0 ? <ListContent spacing={itemSpacing}>
                    <ul className={itemSpacing}>
                        {renderedItems.map((item, i) => (
                            <li key={i}>
                                {
                                    !!onClick ?
                                        <div role={'button'} onClick={() => onClick(item)} className={'button-wrapper'}>
                                            {renderItem(item)}
                                        </div> :
                                        renderItem(item)
                                }
                            </li>
                        ))}
                    </ul>
                    {
                        paginated && <ListPaginationBar items={items} onPageChange={changeItems} {...paginationProps}/>
                    }
                </ListContent> : (emptyMessageRender)()
            }
        </LoaderWrapper>
    </Container>)
};
