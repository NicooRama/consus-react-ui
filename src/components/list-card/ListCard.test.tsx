import {ListCard} from "./ListCard";
import {render} from "../../../test/test.utils";

describe('<ListCard />', () => {
    it('renders <ListCard /> successfully', () => {
        const {getByTestId} = render(<ListCard data-testid={'list-card'}>Card</ListCard>)
        expect(getByTestId('list-card')).toBeInTheDocument();
    });
});
