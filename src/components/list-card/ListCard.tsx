import React, { ReactElement } from 'react'
import { ActiveIndicator } from '../active-indicator/ActiveIndicator'
import { Card } from '../card/Card'
import styled from "styled-components";

const StyledCard = styled(Card)`
  padding: 0;
  display: flex;

  .card-content {
    padding: 24px 24px 22px 16px;
    width: 98%;
  }

  &.clickeable {
    cursor: pointer;
  }

`

export interface ListCardProps {
  children: ReactElement | string
  clickeable?: boolean
  onClick?: () => void
  active?: boolean
  className?: string
}

export const ListCard: React.FC<ListCardProps> = ({
  children,
  onClick,
  active,
  className = '',
  ...props
}) => {
  const handleClick = onClick || (() => {})

  return (
    <StyledCard className={`${className} ${!!onClick ? 'clickeable' : ''}`} {...props}>
      <ActiveIndicator active={active} />
      <div onClick={handleClick} className={'card-content'} role={'button'}>
        <div>{children}</div>
      </div>
    </StyledCard>
  )
}
