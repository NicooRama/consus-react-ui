import type {Meta, StoryObj} from '@storybook/react';
import {ListCard} from './ListCard';

const meta: any = {
    title: 'Card/ListCard',
    component: ListCard,
    tags: ['autodocs'],
} satisfies Meta<typeof ListCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: "I'm a Card",
    },
};
