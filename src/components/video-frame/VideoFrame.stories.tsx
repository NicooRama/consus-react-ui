import type {Meta, StoryObj} from '@storybook/react';
import {VideoFrame} from './VideoFrame';

const meta: any = {
    title: 'Commons/VideoFrame',
    component: VideoFrame,
    tags: ['autodocs'],
} satisfies Meta<typeof VideoFrame>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        videoUrl: 'https://youtube.com/embed/6NDGEh9zWxg?start=537'
    },
};
