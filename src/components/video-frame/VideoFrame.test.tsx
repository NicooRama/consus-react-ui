import {VideoFrame} from "./VideoFrame";
import {render} from "../../../test/test.utils";

describe('<VideoFrame />', () => {
    it('renders <VideoFrame /> successfully', () => {
        const {getByTestId} = render(<VideoFrame data-testid={'video-frame'} videoUrl={'https://youtube.com/embed/6NDGEh9zWxg?start=537'}/>)
        expect(getByTestId('video-frame')).toBeInTheDocument();
    });
});
