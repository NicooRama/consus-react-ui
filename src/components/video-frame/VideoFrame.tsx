import styled from "styled-components";
import React, {useCallback, useEffect, useRef, useState } from 'react';

const IFrame = styled.iframe`
  max-height: 100%;
`

export interface VideoFrameProps {
    className?: string;
    videoUrl: string;
    normalizeUrl?: boolean;
    }

const urlNormalizer = (url: string) => url.replace("watch?v=", "embed/");

export const VideoFrame: React.FC<VideoFrameProps> = ({
                               className = '',
                               videoUrl,
                               normalizeUrl = true,
                               ...props
                           }) => {
    const iframeRef = useRef<HTMLIFrameElement>(null);
    const defaultHeight = 495;
    const [videoHeight, setVideoHeight] = useState<number>(
        iframeRef.current ? iframeRef.current.offsetWidth * 0.5625 : defaultHeight
    );

    const handleChangeVideoWidth = useCallback(() => {
        const ratio =
            window.innerWidth > 990
                ? 1.0
                : window.innerWidth > 522
                    ? 1.2
                    : window.innerWidth > 400
                        ? 1.45
                        : 1.85;
        const height = iframeRef.current
            ? iframeRef.current.offsetWidth * 0.5625
            : defaultHeight;
        return setVideoHeight(Math.floor(height * ratio));
    }, []);

    useEffect(() => {
        window.addEventListener("resize", handleChangeVideoWidth);
        const ratio =
            window.innerWidth > 990
                ? 1.0
                : window.innerWidth > 522
                    ? 1.2
                    : window.innerWidth > 400
                        ? 1.45
                        : 1.85;
        const height = iframeRef.current
            ? iframeRef.current.offsetWidth * 0.5625
            : defaultHeight;
        setVideoHeight(Math.floor(height * ratio));
        return function cleanup() {
            window.removeEventListener("resize", handleChangeVideoWidth);
        };
    }, [videoHeight, handleChangeVideoWidth]);


    return (<IFrame className={`embed-responsive-item ${className}`}
                    src={normalizeUrl ? urlNormalizer(videoUrl) : videoUrl}
                    ref={iframeRef}
                    title={"video"}
                    width="100%"
                    height={`${videoHeight}px`}
                    frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen
                    {...props}/>)
};
