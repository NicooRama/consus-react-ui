import React from "react"
import {faCheckCircle, faTimesCircle} from "@fortawesome/free-regular-svg-icons";
import {fontAwesomeToIcon} from '../icon/icon.utils';
import {FormFrame} from '../form-frame/FormFrame';
import {Text} from '../text/Text';
import { Button } from "../button/Button";
import styled from "styled-components";


const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: ${({ theme }) => theme.spacing.sm}px;
  text-align: center;

  .title,
  .message,
  .button-container {
    text-align: center;

    button {
      margin: 0 auto;
    }
  }
`;

export interface ResultMessageProps {
    message: string,
    title: string,
    error?: boolean,
    errorMessage?: string,
    buttonText?: string,
    onButtonClick?: () => void,
    className?: string,
    }

export const ResultMessage: React.FC<ResultMessageProps> = ({
                                  message,
                                  title,
                                  error = false,
                                  errorMessage = '¡Algo salio mal!',
                                  buttonText = '',
                                  onButtonClick,
                                  className = "",
                                  ...props
                              }) => {
    title = !error ? title : errorMessage

    return (<FormFrame icon={fontAwesomeToIcon(error ? faTimesCircle : faCheckCircle)}
                       color={error ? 'error.normal' : 'success.dark'}
                       className={className} {...props}>
        <Container>
            <Text size={'lg'} weight={'semiBold'} className={'title'}>{title}</Text>
            <Text className={'message'} size={'sm'}>
                {message}
            </Text>
            {
                !!onButtonClick && <div className={'button-container'}>
                    <Button onClick={onButtonClick}>{buttonText}</Button>
                </div>
            }
        </Container>
    </FormFrame>)
};
