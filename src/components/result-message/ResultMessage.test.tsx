import {ResultMessage} from "./ResultMessage";
import {render} from "../../../test/test.utils";

describe('<ResultMessage />', () => {
    it('renders <ResultMessage /> successfully', () => {
        const {getByTestId} = render(<ResultMessage data-testid={'result-message'} message={'Message'} title={'Title'}/>)
        expect(getByTestId('result-message')).toBeInTheDocument();
    });
});
