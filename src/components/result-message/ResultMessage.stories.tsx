import type {Meta, StoryObj} from '@storybook/react';
import {ResultMessage} from './ResultMessage';

const meta: any = {
    title: 'Commons/ResultMessage',
    component: ResultMessage,
    tags: ['autodocs'],
} satisfies Meta<typeof ResultMessage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        title: 'Resultado',
        message: 'Un mensaje descriptivo',
        onButtonClick: () => console.log('Hi'),
        buttonText: 'Saludar'
    },
};
