import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { mobileMediaQuery } from '../../styles/common'
import { Card } from '../card/Card'
import { Text } from '../text/Text'

const StyledCard = styled(Card)`
  &.select-entity-card {
    display: flex;
    height: 200px;

    ${mobileMediaQuery} {
      height: -moz-available;
      height: -webkit-fill-available;
    }
  }

  .select-description {
    margin: auto;
  }
`

export interface SelectEntityCardProps {
  text: string
  className?: string
}

export const SelectEntityCard: React.FC<SelectEntityCardProps> = ({
  className = '',
  text,
  ...props
}) => {
  return (
    <StyledCard className={`select-entity-card ${className}`} {...props}>
      <Text size={'md'} className={'select-description'} color={'fade'}>
        {text}
      </Text>
    </StyledCard>
  )
}

SelectEntityCard.propTypes = {
  text: PropTypes.string.isRequired,
  className: PropTypes.string,
}
