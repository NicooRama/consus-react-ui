import type {Meta, StoryObj} from '@storybook/react';
import {SelectEntityCard} from './SelectEntityCard';

const meta: any = {
    title: 'Commons/SelectEntityCard',
    component: SelectEntityCard,
    tags: ['autodocs'],
} satisfies Meta<typeof SelectEntityCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        text: 'Selects an entity'
    },
};
