import {SelectEntityCard} from "./SelectEntityCard";
import {render} from "../../../test/test.utils";

describe('<SelectEntityCard />', () => {
    it('renders <SelectEntityCard /> successfully', () => {
        const {getByTestId} = render(<SelectEntityCard data-testid={'select-entity-card'} text={'Card'} />)
        expect(getByTestId('select-entity-card')).toBeInTheDocument();
    });
});
