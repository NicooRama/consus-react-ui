import PropTypes from 'prop-types';
import ReactPaginate from "react-paginate";
import {paginationConfig} from "./pagination.config";
import styled from "styled-components";

const Container = styled.div`
  .pagination {
    padding: 0;
    display: flex;
    flex-wrap: wrap;
    list-style-type: none;
    gap: ${({theme}) => theme.spacing.xs}px;
    margin: 0;

    a {
      display: block;
      padding: ${({theme}) => theme.spacing.xs}px;
      min-width: 18px;
      text-align: center;
      cursor: pointer;
      text-decoration: none;
      color: ${({theme}) => theme.colors.primary.normal};
      outline: none;
      font-weight: ${({theme}) => theme.font.weight.semiBold};

      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;

      &:hover {
        color: ${({theme}) => theme.colors.primary.darkest};
      }
    }

    li {
      &.active {
        a {
          color: ${({theme}) => theme.colors.primary.darkest};
          background-color: ${({theme}) =>
                  theme.colors.primary.light};
          border-radius: 50px;
        }
      }

      &:active {
        a {
          color: ${({theme}) => theme.colors.primary.darkest};
          background-color: ${({theme}) =>
                  theme.colors.primary.normal};
          border-radius: 50px;
        }
      }

      &.selected {
        a {
          color: ${({theme}) => theme.colors.primary.darkest};
          background-color: ${({theme}) =>
                  theme.colors.primary.light};
          border-radius: 50px;
        }
      }

      &:hover {
        a {
          color: ${({theme}) => theme.colors.primary.darkest};
          background-color: ${({theme}) =>
                  theme.colors.primary.normal};
          border-radius: 50px;
        }
      }
    }

    .previous {
      a {
      }
    }

    .next {
      a {
      }
    }
  }
`;

export interface PaginationBarProps {
    pageSize?: number,
    pageNumber?: number,
    pageCount: number,
    hideOnSinglePage?: boolean,
    onChange: ({selected}: {selected: number}) => void,
    className?: string,
    containerClassName?: string,
    subContainerClassName?: string,
    [props: string]: any
}

export const PaginationBar: React.FC<PaginationBarProps> = ({
                                  pageSize = 5,
                                  pageNumber = 0,
                                  pageCount,
                                  hideOnSinglePage = true,
                                  onChange,
                                  className = "react-paginate",
                                  containerClassName = 'pagination',
                                  subContainerClassName = 'pages pagination',
                                  ...props
                              }) => {
    return (
        (pageCount >= 1 || !hideOnSinglePage) ?
            <Container className={className} data-testid={props['data-testid']}>
                <ReactPaginate
                    {...paginationConfig}
                    containerClassName={containerClassName}
                    // subContainerClassName={subContainerClassName}
                    previousClassName={"previous"}
                    nextClassName={"next"}
                    forcePage={pageNumber}
                    pageCount={pageCount}
                    onPageChange={onChange}
                    pageRangeDisplayed={pageSize}
                    {...props}
                />
            </Container> : <div/>
    )
};

PaginationBar.propTypes = {
    onChange: PropTypes.func.isRequired,
    className: PropTypes.string,
    pageSize: PropTypes.number,
    pageNumber: PropTypes.number,
    pageCount: PropTypes.number.isRequired,
    hideOnSinglePage: PropTypes.bool,
    containerClassName: PropTypes.string,
};
