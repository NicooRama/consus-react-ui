export const paginationConfig = {
    previousLabel: 'Anterior',
    nextLabel: 'Siguiente',
    breakLabel: '...',
    pageRangeDisplayed: 3,
    marginPagesDisplayed: 2,
};
