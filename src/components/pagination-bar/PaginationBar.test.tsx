import {PaginationBar} from "./PaginationBar";
import {render} from "../../../test/test.utils";

describe('<PaginationBar />', () => {
    it('renders <PaginationBar /> successfully', () => {
        const {getByTestId} = render(<PaginationBar data-testid={'pagination-bar'} pageCount={15} onChange={() => {}}/>)
        expect(getByTestId('pagination-bar')).toBeInTheDocument();
    });
});
