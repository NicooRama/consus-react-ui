import type {Meta, StoryObj} from '@storybook/react';
import {PaginationBar} from './PaginationBar';

const meta: any = {
    title: 'Pagination/PaginationBar',
    component: PaginationBar,
    tags: ['autodocs'],
} satisfies Meta<typeof PaginationBar>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        pageCount: 15,
        onChange: () => {}
    },
};
