import type {Meta, StoryObj} from '@storybook/react';
import {DroppablePanel} from "./droppable-panel/DroppablePanel";
import {useState} from "react";
import styled from "styled-components";
import {Card} from "../card/Card";
import {Button} from "../button/Button";
import {DraggableCard} from "./draggable-card/DraggableCard";
import {Text} from "../text/Text";

const meta: any = {
    title: 'Drag and Drop/DragAndDrop',
    component: DroppablePanel,
    tags: ['autodocs'],
} satisfies Meta<typeof DroppablePanel>;

export default meta;
type Story = StoryObj<typeof meta>;

const Container = styled.div`
  display: grid;
  grid-gap: ${({ theme }) => theme.spacing.md}px;

  .panel {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(85px, 1fr));
    grid-gap: ${({ theme }) => theme.spacing.sm}px;
  }

  .droppable-panel {
    border: ${({ theme }) => theme.border};
    border-radius: ${({ theme }) => theme.borderRadius}px;
    padding: ${({ theme }) => theme.spacing.sm}px;
    max-width: 100%;
    height: 200px;

    .card {
      height: fit-content;
    }
  }

  .reset-button {
    width: fit-content;
  }
`;

export const Primary: Story = {
    render: () => {
      const [titles] = useState(["Card 1", "Card 2", "Card 3"]);
      const [titlesDropped, setTitlesDropped] = useState([] as string[]);

      // @ts-ignore
      const onDrop = (event: any, id: string) => {
        const index = Number(id.split('-')[1]);
        const title = titles[index] as any;

        const titlesDroppedUpdated: string[] = [...titlesDropped];
        if(!titlesDroppedUpdated.includes(title)){
          titlesDroppedUpdated.push(title);
        };

        setTitlesDropped(titlesDroppedUpdated);
      }

      const reset = () => {
        setTitlesDropped([]);
      }

      return <Container>
        <div className={'panel'}>
          {titles.map((title, i) => <DraggableCard dragId={`draggable-${i}`} key={i}>{title}</DraggableCard>)}
        </div>
        <DroppablePanel onDrop={onDrop} className={'panel droppable-panel'}>
          {
            titlesDropped.length > 0 ? titlesDropped.map((titleDropped, i) => <Card className={'card'} key={i}>{titleDropped}</Card>) : (
                <Text size={'md'} weight={'semiBold'} className={'drop-here-text'} color={'fade'}>Drop here</Text>
            )
          }
        </DroppablePanel>
        <Button className={'reset-button'} onClick={reset}>Reset</Button>
      </Container>
    }
};
