import React from "react"
import {ReactElement} from "react";
import styled from "styled-components";
import {Card} from "../../card/Card";

const StyledCard = styled(Card)`
    cursor: grab;
`

export interface DraggableCardProps {
    children: ReactElement | string,
    onClick?: () => void,
    onWheelClick?: () => void,
    dragId: string,
    className?: string;
    }

export const DraggableCard: React.FC<DraggableCardProps> = ({
                                  onClick = () => {},
                                  onWheelClick = () => {},
                                  children,
                                  dragId,
                                  className = "",
                                  ...props
                              }) => {
    const onDragStart = (event: any) => {
        event.dataTransfer.setData("id", dragId);
    };

    const onMouseDown = (event: any) => {
        if (event.nativeEvent.button === 1) {
            onWheelClick()
        }
    };

    return (
        <StyledCard
              className={`draggable-card ${className}`}
            //@ts-ignore
              id={dragId}
              onDragStart={onDragStart}
              onTouchStart={onDragStart}
              onClick={onClick}
              onMouseDown={onMouseDown}
            //Ver en mobile de usar onTouchStart y onTouchLeave
              draggable
              {...props}
        >
            {children}
        </StyledCard>
    )
};
