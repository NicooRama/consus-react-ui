import {DraggableCard} from "./DraggableCard";
import {render} from "../../../../test/test.utils";

describe('<DraggableCard />', () => {
    it('renders <DraggableCard /> successfully', () => {
        const {getByTestId} = render(<DraggableCard dragId={'1'} data-testid={'draggable-card'}>Draggable card</DraggableCard>)
        expect(getByTestId('draggable-card')).toBeInTheDocument();
    });
});
