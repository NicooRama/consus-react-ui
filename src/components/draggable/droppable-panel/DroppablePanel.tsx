import React, {ReactElement} from 'react';
export interface DroppablePanelProps {
    onDrop: (event: any, id: string) => void;
    children: ReactElement | ReactElement[] | string;
    className?: string;
    }

const onDragOver = (event: any) => event.preventDefault();

// TODO: ver si vale la pena agregarle mas cosas
export const DroppablePanel: React.FC<DroppablePanelProps> = ({
                                   onDrop,
                                   children,
                                   className = '',
                                   ...props
                               }) => {
    const handleDrop = (event: any) => {
        const id = event.dataTransfer.getData("id");
        onDrop(event, id);
    }

    return (
        <div className={`droppable-panel ${className}`}
             onDragOver={onDragOver}
             onDrop={handleDrop}
             {...props}
        >
            {
                children
            }
        </div>
    )
};
