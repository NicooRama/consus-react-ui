import {DroppablePanel} from "./DroppablePanel";
import {render} from "../../../../test/test.utils";

describe('<DroppablePanel />', () => {
    it('renders <DroppablePanel /> successfully', () => {
        const {getByTestId} = render(<DroppablePanel data-testid={'droppable-panel'} onDrop={() => {}}>Droppable panel</DroppablePanel>)
        expect(getByTestId('droppable-panel')).toBeInTheDocument();
    });
});
