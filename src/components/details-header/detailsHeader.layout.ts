import styled from "styled-components";

export const Container = styled.div`
  display: grid;
  gap: ${({theme}) => theme.spacing.sm}px;
  grid-template-columns: 1fr auto;
  align-items: center;
`
