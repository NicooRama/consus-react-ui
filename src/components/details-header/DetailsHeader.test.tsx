import {DetailsHeader} from "./DetailsHeader";
import {renderWithRouter as render} from "../../../test/test.utils";

describe('<DetailsHeader />', () => {
    it('renders <DetailsHeader /> successfully', () => {
        // @ts-ignore
        const {getByTestId} = render(<DetailsHeader data-testid={'details-header'}/>)
        expect(getByTestId('details-header')).toBeInTheDocument();
    });
});
