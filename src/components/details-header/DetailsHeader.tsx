import React from 'react';
import styled from 'styled-components';
import {ButtonsPanel} from "../buttons-panel/ButtonsPanel";
import {Container} from "./detailsHeader.layout";

export interface DetailsHeaderProps {
    editLink: string;
    confirmationText: string;
    detailsLink?: string;
    showRemove: boolean;
    onRemoveConfirmation: () => void | Promise<void>;
    [props: string]: any;
}

export const DetailsHeader: React.FC<DetailsHeaderProps> = ({
                                                                editLink,
                                                                confirmationText,
                                                                detailsLink,
                                                                onRemoveConfirmation,
                                                                showRemove,
                                                                children,
                                                                ...props
                                                            }) => {
    return (<Container {...props}>
        {children}
        <ButtonsPanel detailsLink={detailsLink}
                      showDetails={!!detailsLink}
                      editLink={editLink}
                      confirmationText={confirmationText}
                      onRemoveConfirmation={onRemoveConfirmation}
                      showRemove={showRemove}
        />
    </Container>)
};
