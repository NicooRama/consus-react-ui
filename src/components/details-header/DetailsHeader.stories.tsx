import type {Meta, StoryObj} from '@storybook/react';
import {DetailsHeader} from './DetailsHeader';
import {routerDecorator} from "../../storybook.utils";

const meta: any = {
    title: 'Details/DetailsHeader/DetailsHeader',
    component: DetailsHeader,
    tags: ['autodocs'],
} satisfies Meta<typeof DetailsHeader>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
    decorators: [routerDecorator],
};
