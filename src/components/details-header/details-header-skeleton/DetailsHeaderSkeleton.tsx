import React from 'react';
import {ButtonsPanelSkeleton} from "../../buttons-panel/buttons-panel-skeleton/ButtonsPanelSkeleton";
import {Container} from "../detailsHeader.layout";

export interface DetailsHeaderSkeletonProps {
    showDetails?: boolean;
    showEdit?: boolean;
    showRemove?: boolean;

    [props: string]: any;
}

export const DetailsHeaderSkeleton: React.FC<DetailsHeaderSkeletonProps> = ({
                                                                                showDetails = false,
                                                                                showEdit= false,
                                                                                showRemove= false,
                                                                                children,
                                                                                ...props
                                                                            }) => {
    return (<Container {...props}>
        {children}
        <ButtonsPanelSkeleton showDetails={showDetails}
                              showEdit={showEdit}
                              showRemove={showRemove}/>
    </Container>)
};
;
