import {DetailsHeaderSkeleton} from "./DetailsHeaderSkeleton";
import {render} from "../../../../test/test.utils";

describe('<DetailsHeaderSkeleton />', () => {
    it('renders <DetailsHeaderSkeleton /> successfully', () => {
        const {getByTestId} = render(<DetailsHeaderSkeleton data-testid={'details-header-skeleton'}/>)
        expect(getByTestId('details-header-skeleton')).toBeInTheDocument();
    });
});
