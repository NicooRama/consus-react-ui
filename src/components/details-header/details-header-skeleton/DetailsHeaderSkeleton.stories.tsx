import type {Meta, StoryObj} from '@storybook/react';
import {DetailsHeaderSkeleton} from './DetailsHeaderSkeleton';

const meta: any = {
    title: 'Details/DetailsHeader/DetailsHeaderSkeleton',
    component: DetailsHeaderSkeleton,
    tags: ['autodocs'],
} satisfies Meta<typeof DetailsHeaderSkeleton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        showDetails: false,
        showEdit: true,
        showRemove: true,
    },
};
