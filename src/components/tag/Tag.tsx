import React from "react"
import {ThemeColorIndex} from "../../styles/theme.interface";
import {ReactElement} from "react";
import {lighten, darken} from "polished";
import {getIn} from "../../utils/utils";
import styled from "styled-components";


const StyledButton = styled.button<{ color: ThemeColorIndex }>`
  background-color: ${({theme, color}) => getIn(theme.colors, color)};
  color: white;
  border-radius: 4px;
  font-size: 14px;
  padding: ${({theme}: any) => theme.spacing.xs}px;
  border: 0;

  &:focus {
    outline: none;
    box-shadow: 0 0 0 0.15rem ${({theme, color}) => lighten(0.2, getIn(theme.colors, color))};
  }

  &:last-of-type {
    margin-right: 0;
  }

  &.clickeable {
    transition-duration: 0.2s;
    cursor: pointer;

    &:hover {
      background-color: ${({theme, color}) => darken(0.1, getIn(theme.colors, color))};
    }
  }

  &.active {
    background-color: ${({theme, color}) => darken(0.1, getIn(theme.colors, color))}
  }
}
`;

export interface TagProps {
    children: ReactElement | string;
    color?: ThemeColorIndex;
    className?: string;
    active?: boolean;
    onClick?: (value: any) => void;
}

export const Tag: React.FC<TagProps> = ({
                                            color = 'tag',
                                            children,
                                            className = "",
                                            active = false,
                                            onClick,
                                            ...props
                                        }) => {
    className = `tag ${className} ${!!onClick ? 'clickeable' : ' '} ${active ? 'active' : ''}`;
    const handleClick = !!onClick ? onClick : () => {
    };
    return <StyledButton type={'button'}
                         color={color}
                         className={className}
                         onClick={handleClick} {...props}>
        {children}
    </StyledButton>
};
