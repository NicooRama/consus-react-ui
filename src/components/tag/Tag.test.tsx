import {Tag} from "./Tag";
import {render} from "../../../test/test.utils";

describe('<Tag />', () => {
    it('renders <Tag /> successfully', () => {
        const {getByTestId} = render(<Tag data-testid={'tag'}>Tag</Tag>)
        expect(getByTestId('tag')).toBeInTheDocument();
    });
});
