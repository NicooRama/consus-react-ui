import React from 'react'
import styled from 'styled-components'
import { css } from 'styled-components'
import { Text } from '../text/Text'
import { mobileMediaQuery } from '../../styles/common'

const Container = styled.div<{ alignment: string }>`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: ${({ alignment }) => alignment};
  position: relative;
`

const Content = styled.div`
  display: flex;
  gap: ${({ theme }: any) => theme.spacing.xxs}px;
  flex-direction: column;
  align-items: center;
`

const circleSize = 40

const NumberCircle = styled(Text)<{
  status: 'actual' | 'completed' | 'uncompleted'
  clickeable: boolean
}>`
  background-color: ${({ theme, status }: any) =>
    status === 'completed' || status === 'actual'
      ? theme.colors.primary.normal
      : theme.colors.inputs.disabled};
  border-radius: ${circleSize * 2}px;
  height: ${circleSize}px;
  width: ${circleSize}px;
  display: flex;
  place-items: center;
  place-content: center;

  ${({ clickeable }) =>
    clickeable &&
    css`
      cursor: pointer;
    `}
`

const calculateBorderColor = (theme: any, status: any, alignment: any, second: boolean) => {
  if (alignment === 'end' && (status === 'actual' || status === 'completed')) {
    return theme.colors.primary.normal
  }
  if (alignment === 'center') {
    if (status === 'completed') {
      return theme.colors.primary.normal
    } else if (status === 'actual') {
      return second ? theme.colors.inputs.disabled : theme.colors.primary.normal
    }
    return theme.colors.inputs.disabled
  }
  if (alignment === 'start') {
    if (status === 'actual') {
      return theme.colors.inputs.disabled
    } else {
      return theme.colors.primary.normal
    }
  }
  return theme.colors.inputs.disabled
}

const StepDivider = styled.div<{
  alignment: string
  status: 'actual' | 'completed' | 'uncompleted'
  second?: boolean
}>`
  width: ${({ alignment, status }: any) =>
    alignment === 'center' && status === 'actual' ? '50%' : '100%'};
  height: 1px;

  border-bottom: 1px solid
    ${({ theme, status, alignment, second }: any) =>
      `${calculateBorderColor(theme, status, alignment, second)}`};

  position: absolute;
  top: 20px;
  z-index: -1;

  ${({ theme, status, alignment, second }: any) =>
    second &&
    css`
      border-bottom: 1px solid ${calculateBorderColor(theme, status, alignment, second)};
    `}

  ${({ alignment, second }: any) =>
    alignment === 'center' &&
    !second &&
    css`
      margin-right: auto;
      display: flex;
      align-self: start;
    `}

  ${({ alignment, second }: any) =>
    alignment === 'center' &&
    second &&
    css`
      margin-left: auto;
      display: flex;
      align-self: end;
    `}

  ${({ alignment }) =>
    alignment === 'start' &&
    css`
      margin-left: ${circleSize}px;
      width: calc(100% - ${circleSize}px);
    `}
  ${({ alignment }) =>
    alignment === 'end' &&
    css`
      margin-right: ${circleSize}px;
      width: calc(100% - ${circleSize}px);
    `}
`

const Title = styled(Text)`
  padding-left: ${({ theme }: any) => theme.spacing.xs}px;
  padding-right: ${({ theme }: any) => theme.spacing.xs}px;
  text-align: center;

  ${mobileMediaQuery} {
    display: none;
  }
`

export interface StepProps {
  title: string
  number: number
  status: 'actual' | 'completed' | 'uncompleted'
  alignment: 'start' | 'center' | 'end'
  onClick?: () => void
}

export const Step: React.FC<StepProps> = ({
  title,
  number,
  alignment,
  status,
  onClick,
  ...props
}) => {
  return (
    <Container alignment={alignment} {...props}>
      <Content>
        <NumberCircle
          size={'md'}
          weight={'semiBold'}
          status={status}
          onClick={onClick || (() => {})}
          clickeable={!!onClick}
        >
          {number}
        </NumberCircle>
        <Title size={'xs'}>{title}</Title>
      </Content>
      <StepDivider alignment={alignment} status={status} />
      {alignment === 'center' && status === 'actual' && (
        <StepDivider alignment={alignment} status={status} second={true} />
      )}
    </Container>
  )
}
