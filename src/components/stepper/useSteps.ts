import {useState} from "react";
import { Step } from "./step.interface";

//TODO: mejorar, permitir navegacion hacia adelante cuando estas en edit mode (Manejarlo con dirty)
export const useSteps = (initialSteps: Step[], {
    initialNumberStep = 1,
    //@ts-ignore
    onBack = (activeStep: number) => {},
    //@ts-ignore
    onNext = (activeStep: number) => {},
} = {}) => {
    const [steps, setSteps] = useState(initialSteps);
    const [activeStep, setActiveStep] = useState(initialNumberStep);

    const goNext = () => {
        onNext(activeStep);
        setActiveStep(activeStep + 1);
    }

    const goBack = () => {
        onBack(activeStep);
        setActiveStep(activeStep - 1);
    }

    const isFirstStep = activeStep > 1;
    const isLastStep = activeStep === steps.length;
    const stepsCount = steps.length;

    return {
        steps,
        setSteps,
        activeStep,
        goNext,
        goBack,
        setActiveStep,
        isFirstStep,
        isLastStep,
        stepsCount,
    }
}
