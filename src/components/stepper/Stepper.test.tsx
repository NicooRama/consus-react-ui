import {Stepper} from "./Stepper";
import {render} from "../../../test/test.utils";

describe('<Stepper />', () => {
    it('renders <Stepper /> successfully', () => {
        const {getByTestId} = render(<Stepper data-testid={'stepper'} activeStep={1} onClick={() => {}} steps={[]}/>)
        expect(getByTestId('stepper')).toBeInTheDocument();
    });
});
