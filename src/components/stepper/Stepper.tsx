import React from "react"
import styled from "styled-components";
import { Step as StepComponent } from './Step';
import { Step } from './step.interface';

const Container = styled.div`
  display: flex;
`

export interface StepperProps {
    steps: Step[];
    activeStep: number;
    onClick: (step: number) => void;
    }

const calculateAlignment = (index: number, totalSteps: number) => {
    if (index === 0) return 'start';
    if (index === totalSteps - 1) return 'end';
    return 'center';
}

const calculateStatus = (index: number, activeStep: number) => {
    if(index < activeStep - 1) {
        return 'completed';
    }
    if(index === activeStep - 1) {
        return 'actual';
    }
    return 'uncompleted';
}

export const Stepper: React.FC<StepperProps> = ({
                            steps,
                            activeStep,
                            onClick,
                            ...props
                        }) => {
    return (<Container {...props}>
        {
            steps.map((step, index) => {
                const status = calculateStatus(index, activeStep);
                return <StepComponent title={step.title}
                                      alignment={calculateAlignment(index, steps.length)}
                                      key={step.title}
                                      status={calculateStatus(index, activeStep)}
                                      number={index + 1}
                                      onClick={status === 'completed' ? (() => onClick(index + 1)) : undefined}
                />
            })
        }
    </Container>)
};
