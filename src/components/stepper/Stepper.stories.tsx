import type {Meta, StoryObj} from '@storybook/react';
import {Stepper} from './Stepper';

const meta: any = {
    title: 'Commons/Stepper',
    component: Stepper,
    tags: ['autodocs'],
} satisfies Meta<typeof Stepper>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        steps: [
            {
                title: 'Paso 1',
                status: 'completed'
            },
            {
                title: 'Paso 2',
                status: 'actual'
            },
            {
                title: 'Paso 3',
                status: 'uncompleted'
            },
        ],
        activeStep: 2,
    },
};
