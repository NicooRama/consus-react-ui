export interface Step {
    id: string;
    title: string;
    status: 'completed' | 'actual' | 'uncompleted';
}
