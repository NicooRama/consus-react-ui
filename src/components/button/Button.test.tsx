import {Button} from "./Button";
import {render} from "../../../test/test.utils";
import {screen} from "@testing-library/react";

describe('<Button />', () => {
    it('renders <Button /> successfully', () => {
        const {getByTestId} = render(<Button data-testid={'button'}>Button</Button>)
        expect(getByTestId('button')).toBeInTheDocument();
    });

    it('onClick fire event successfully', () => {
        const handleClick = jest.fn();
        render(<Button onClick={handleClick}>foo</Button>);
        const button = screen.getByRole('button');
        button.click();
        expect(handleClick).toHaveBeenCalled();
    });

    it('onClick not fires event because button is disabled', () => {
        const handleClick = jest.fn();
        render(<Button onClick={handleClick} disabled={true}>foo</Button>);
        const button = screen.getByRole('button');
        button.click();
        expect(handleClick).not.toHaveBeenCalled();
    });

    it('text render successfully', () => {
        render(<Button onClick={() => {}}>Hello world</Button>)
        const text = screen.getByText('Hello world');
        expect(text).toBeInTheDocument();
    })

    it('set active class when active flag is setted true', () => {
        render(<Button onClick={() => {}} active={true}>Hello world </Button>)
        const button = screen.getByRole('button')
        expect(button).toHaveClass('active')
    })
});
