import type {Meta, StoryObj} from '@storybook/react';
import {Button} from './Button';

const meta: any = {
    title: 'Button/Button',
    component: Button,
    tags: ['autodocs'],
} satisfies Meta<typeof Button>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: 'Button'
    },
};

export const Secondary: Story = {
    args: {
        ...Primary.args,
        variant: 'secondary',
    },
};

export const Tertiary: Story = {
    args: {
        ...Primary.args,
        variant: 'tertiary',
    }
}
