import React, { ReactElement } from 'react'
import { ThemeColorIndex } from '../../styles/theme.interface'
import { adjustHue, lighten } from 'polished'

import styled, {css} from 'styled-components'

import { mobileMediaQuery } from '../../styles/common'
import { getIn } from '../../utils/utils'


const Component = styled.button<{ color: ThemeColorIndex; tag: any, variant: ButtonVariant }>`
  display: block;
  width: ${({ tag }) => (tag === 'button' ? 'auto' : 'fit-content')};
  user-select: none;
  line-height: ${({ theme }) => theme.spacing.sm * 2}px;
  font-size: ${({ theme }) => theme.font.size.sm}px;
  font-weight: ${({ theme }) => theme.font.weight.semiBold};
  border: 2px solid ${({ theme, color }) => getIn(theme.colors, color)};
  border-radius: ${({ theme }) => theme.borderRadius}px;
  outline: none;
  padding: ${({ theme }) => theme.spacing.xs}px ${({ theme }) => theme.spacing.sm}px;

  cursor: pointer;

  ${mobileMediaQuery} {
    width: 100%;
    justify-content: center;
  }

  &:disabled {
    opacity: 0.5;
    pointer-events: none;
  }

  ${({theme,color, variant}) =>
      variant === 'primary' &&
      css`
          background-color: ${getIn(theme.colors, color)};
          color: ${theme.colors.textButton};
        
          &:hover,
          &.active {
            transition-duration: 0.2s;
            background-color: ${adjustHue(347, getIn(theme.colors, color))};
            border-color: ${adjustHue(347, getIn(theme.colors, color))};
          }
        
          &:focus,
          &.focus {
            transition-duration: 0.2s;
            box-shadow: ${`0 0 0 0.2rem ${lighten(0.2, getIn(theme.colors, color))}`};
          }
      `}
  
  ${({theme,color, variant}) =>
      variant === 'secondary' &&
      css`
        background: transparent;
        color: ${getIn(theme.colors, color)};
        // Substract 1 for the border width
        padding-top: ${theme.spacing.xs - 1}px;
        padding-bottom: ${theme.spacing.xs - 1}px;

        &:hover, &.active {
          transition-duration: 0.2s;
          background-color: ${adjustHue(347, getIn(theme.colors, color))};
          border-color: ${adjustHue(347, getIn(theme.colors, color))};
          color: ${theme.colors.textButton};
        }

        &:focus, &.focus {
          transition-duration: 0.2s;
          box-shadow: ${`0 0 0 0.2rem ${lighten(0.2, getIn(theme.colors, color))}`};
        }
      `}
  
  ${({theme,color, variant}) =>
      variant === 'tertiary' &&
      css`
        background: transparent;
        border-color: transparent;
        color: ${getIn(theme.colors, color)};

        &:hover,
        &.active {
          transition-duration: 0.2s;
          text-decoration: underline;
        }
        &:focus,
        &.focus {
          transition-duration: 0.2s;
          box-shadow: ${`0 0 0 0.2rem ${lighten(0.2, getIn(theme.colors, color))}`};
        }
      `}
`

type ButtonVariant = 'primary' | 'secondary' | 'tertiary'

export interface ButtonProps {
  onClick?: (e?: any) => void
  children: ReactElement | string | boolean | ReactElement[]
  active?: boolean
  className?: string
  color?: ThemeColorIndex
  disabled?: boolean
  variant?: ButtonVariant
  tag?: 'button' | 'span' | any
  [props: string]: any;
}

export const Button: React.FC<ButtonProps> = ({
  tag = 'button',
  onClick = () => {},
  children,
  active = false,
  className = '',
  color = 'primary.normal',
  disabled = false,
  variant = 'primary',
  ...props
}) => {
  const handleClick = !disabled ? onClick : () => {}

  return (
    <Component
      color={color}
      className={`${className} ${active ? 'active' : ''} ${variant}`}
      disabled={disabled}
      onClick={handleClick}
      type={'button'}
      tag={tag}
      as={tag}
      variant={variant}
      {...props}
    >
      {children}
    </Component>
  )
}
