import {createGlobalStyle} from "styled-components";
import {darken} from "polished";

export const ScrollbarStyles = createGlobalStyle`
  *::-webkit-scrollbar {
    width: 8px;
    height: 8px;
  }

  *::-webkit-scrollbar-track {
    box-shadow: inset 0 0 3px grey;
    border-radius: 5px;
  }

  *::-webkit-scrollbar-thumb {
    background: ${({ theme }: any) => theme.colors.primary.normal};
    border-radius: 5px;
  }

  *::-webkit-scrollbar-thumb:hover {
    background: ${({ theme }: any) => darken(0.1, theme.colors.primary.normal)};
  }
`
