import {ButtonWrapper} from "./ButtonWrapper";
import {render} from "../../../test/test.utils";
import {screen} from "@testing-library/react";

describe('<ButtonWrapper />', () => {
    it('renders <ButtonWrapper /> successfully', () => {
        const {getByTestId} = render(<ButtonWrapper data-testid={'button-wrapper'}>Button</ButtonWrapper>)
        expect(getByTestId('button-wrapper')).toBeInTheDocument();
    });

    it('onClick fire event successfully', () => {
        const handleClick = jest.fn();
        render(<ButtonWrapper onClick={handleClick}>foo</ButtonWrapper>);
        const button = screen.getByRole('button');
        button.click();
        expect(handleClick).toHaveBeenCalled();
    });

    it('text render successfully', () => {
        render(<ButtonWrapper onClick={() => {}}>Hello world</ButtonWrapper>)
        const text = screen.getByText('Hello world');
        expect(text).toBeInTheDocument();
    })

    it('add inherit class when inherit flag is setted true', () => {
        render(<ButtonWrapper onClick={() => {}} inherit>Hello world</ButtonWrapper>)
        const button = screen.getByRole('button');
        expect(button).toHaveClass('inherit');
    })
});
