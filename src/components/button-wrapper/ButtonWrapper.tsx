import React from 'react'
import { ButtonProps } from '../button/Button'
import styled from 'styled-components'

const Component = styled.button`
  padding: 0;
  border: none;
  background-color: transparent;
  outline: none;
  cursor: pointer;

  &.inherit {
    width: 100%;
    text-align: inherit;
  }
`

export interface ButtonWrapperProps extends ButtonProps {
  inherit?: boolean;
  buttonRef?: any;
  [props: string]: any;
}

export const ButtonWrapper: React.FC<ButtonWrapperProps> = ({
  children,
  onClick,
  className = '',
  inherit = false,
  buttonRef,
  ...props
}) => {
  return (
    <Component
      onClick={onClick}
      className={`${className} ${inherit ? 'inherit' : ''}`}
      ref={buttonRef}
      type={'button'}
      {...props}
    >
      {children}
    </Component>
  )
}
