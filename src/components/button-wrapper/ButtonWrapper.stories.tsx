import type {Meta, StoryObj} from '@storybook/react';
import {ButtonWrapper} from './ButtonWrapper';

const meta: any = {
    title: 'Button/ButtonWrapper',
    component: ButtonWrapper,
    tags: ['autodocs'],
} satisfies Meta<typeof ButtonWrapper>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: "I'm a text inside the button",
    },
};
