import { Header } from './Header'
import { renderWithRouter } from '../../../test/test.utils'
import { screen } from '@testing-library/react'

describe('<Header />', () => {
  it('renders Header successfully', () => {
    renderWithRouter(
      <Header
        appName={'Test app'}
        centerButtons={[{ id: 'home', label: 'Home', link: '/' }]}
        rightButtons={[{ id: 'login', label: 'Login', link: '/login' }]}
      />,
    )
    expect(screen.getByText(/Test app/i)).toBeInTheDocument()
    expect(screen.getByText(/Home/i)).toBeInTheDocument()
    expect(screen.getByText(/Login/i)).toBeInTheDocument()
  })
})
