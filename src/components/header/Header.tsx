import React from "react"
import {Link} from "react-router-dom";
import styled from "styled-components";

const Navbar = styled.div`
  background-color: #343a40;
  position: sticky;
  padding-top: ${({ theme }) => theme.spacing.sm}px;
  padding-bottom: ${({ theme }) => theme.spacing.sm}px;
  padding-left: ${({ theme }) => theme.spacing.lg}px;
  padding-right: ${({ theme }) => theme.spacing.lg}px;
  display: grid;
  grid-template-columns: 1fr;
  grid-row-gap: ${({ theme }) => theme.spacing.md}px;
  grid-auto-rows: minmax(min-content, max-content);

  @media (min-width: 768px) {
    grid-template-columns: auto 1fr auto;
    grid-column-gap: ${({ theme }) => theme.spacing.md}px;
    align-items: center;
  }

  .home-button {
    color: ${({ theme }) => theme.colors.primary.darken};
    text-decoration: none;
    font-size: ${({ theme }) => theme.font.size.md}px;

    &:hover {
      color: white;
    }
  }

  .center-buttons {
    display: flex;
    flex-wrap: wrap;
    gap: ${({ theme }) => theme.spacing.sm}px;

    @media (min-width: 768px) {
      justify-self: center;
    }

    .link-button {
      color: hsla(0, 0%, 100%, 0.5);
      text-decoration: none;
      font-size: ${({ theme }) => theme.font.size.sm}px;

      &:hover,
      &.active {
        color: ${({ theme }) => theme.colors.primary.normal};
      }
    }
  }

  .right-buttons {
    display: flex;
    flex-wrap: wrap;
    gap: ${({ theme }) => theme.spacing.sm}px;

    .link-filled-button {
      display: inline-flex;
      color: ${({ theme }) => theme.colors.primary.darken};
      text-decoration: none;
      font-size: ${({ theme }) => theme.font.size.sm}px;

      &:hover {
        color: white;
      }
    }
  }
`;

export interface HeaderLink {
    id: string,
    link: string,
    label: string,
}

export interface HeaderProps {
    appName: string,
    active?: string,
    className?: string,
    centerButtons?: HeaderLink[];
    rightButtons?: HeaderLink[];

    }

//TODO: real responsive
export const Header: React.FC<HeaderProps> = ({
                           appName,
                           className = "",
                           centerButtons = [],
                           rightButtons = [],
                           active,
                           ...props
                       }) => {
    // @ts-ignore
    return (<Navbar className={className} {...props}>
        <Link to={'/'} className={'home-button'}>{appName}</Link>
        <div className={'center-buttons'}>
            {
                centerButtons.map((button) => (
                    <Link to={button.link}
                          className={`link-button ${active === button.id ? 'active' : ''}`} key={button.id}>{button.label}</Link>
                ))
            }
        </div>
        <div className={'right-buttons'}>
            {
                rightButtons.map((button) => (
                    <Link to={button.link} className={'link-filled-button'} key={button.id}>{button.label}</Link>
                ))
            }
        </div>
    </Navbar>)
};
