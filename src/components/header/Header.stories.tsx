import type {Meta, StoryObj} from '@storybook/react';
import {Header} from './Header';
import {routerDecorator} from "../../storybook.utils";

const meta: any = {
    title: 'Commons/Header',
    component: Header,
    tags: ['autodocs'],
    decorators: [routerDecorator]
} satisfies Meta<typeof Header>;

export default meta;
type Story = StoryObj<typeof meta>;

const centerButtonsArg = [
    {
        id: 'page1',
        link: '/',
        label: 'Page1'
    },
    {
        id: 'page2',
        link: '/',
        label: 'Page2'
    },
    {
        id: 'page3',
        link: '/',
        label: 'Page3'
    },
    {
        id: 'page4',
        link: '/',
        label: 'Page4'
    },
    {
        id: 'page5',
        link: '/',
        label: 'Page5'
    },
    {
        id: 'page6',
        link: '/',
        label: 'Page6'
    },
];

export const Primary: Story = {
    args: {
        appName: 'React UI',
        centerButtons: centerButtonsArg,
        rightButtons: [
            {
                link: '/',
                label: 'Button1'
            },
            {
                link: '/',
                label: 'Button2'
            },
            {
                link: '/',
                label: 'Button3'
            },
            {
                link: '/',
                label: 'Button4'
            },
        ],
    },
    argTypes: {
        active: {
            options: [...centerButtonsArg.map(x => x.id), 'Nothing...'],
            control: { type: 'select' },
        },
    },
    decorators: [routerDecorator]
};
