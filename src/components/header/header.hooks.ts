import { useLocation } from 'react-router-dom'
import {HeaderLink} from "./Header";

export const useActiveHeaderButton = (centerButtons: HeaderLink[]) => {
    const {pathname} = useLocation();
    const buttons = [...centerButtons];
    buttons.sort((a, b) => (
        b.link.length - a.link.length
    ));
    const button = buttons.find((button) => pathname.startsWith(button.link));
    return button?.id;
}
