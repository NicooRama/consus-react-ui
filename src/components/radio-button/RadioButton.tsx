import React from 'react'
import { ButtonWrapper } from '../button-wrapper/ButtonWrapper'
import styled from 'styled-components'

const StyledButtonWrapper = styled(ButtonWrapper)`
  gap: ${({theme}) => theme.spacing.xs}px;

  &.radio {
    font-size: ${({theme}) => theme.font.size.sm}px;
    display: flex;
    align-items: center;
    position: relative;

    .box {
      border-radius: ${({theme}) => theme.font.size.xxs + 2}px;
      box-shadow: 0 0 0 1px ${({theme}) => theme.colors.inputs.border};
      border: 2px solid white;
      position: relative;
      width: ${({theme}) => theme.font.size.xxs + 2}px;
      height: ${({theme}) => theme.font.size.xxs + 2}px;
      background-color: white;
      flex-shrink: 0;
    }

    &.checked .box {
      background-color: ${({theme}) => theme.colors.primary.normal};
    }

    .icon {
      border-radius: 0.5px;
      position: absolute;
      left: 0;
      top: 0;
      width: ${({theme}) => theme.font.size.xxs - 4}px;
      height: ${({theme}) => theme.font.size.xxs - 4}px;
      cursor: pointer;

      path {
        fill: ${({theme}) => theme.colors.primary.normal};
      }
    }

    &.disabled .icon {
      cursor: default;

      path {
        fill: ${({theme}) => theme.colors.primary.normal};
      }
    }
  }
`

export interface RadioButtonProps {
  children?: any
  checked?: boolean
  onCheck: () => void
  className?: string
  disabled?: boolean
}

export const RadioButton: React.FC<RadioButtonProps> = ({
  children,
  checked,
  onCheck,
  className = '',
  disabled,
  ...props
}) => {
  return (
    <StyledButtonWrapper
      className={`radio ${className} ${checked ? 'checked' : ''} ${disabled ? 'disabled' : ''}`}
      onClick={!disabled ? onCheck : () => {}}
      {...props}
    >
      <div className="box">
        <div className="icon"></div>
      </div>
      {children}
    </StyledButtonWrapper>
  )
}
