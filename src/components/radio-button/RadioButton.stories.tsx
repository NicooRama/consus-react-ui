import type {Meta, StoryObj} from '@storybook/react';
import {RadioButton} from './RadioButton';

const meta: any = {
    title: 'RadioButton/RadioButton',
    component: RadioButton,
    tags: ['autodocs'],
} satisfies Meta<typeof RadioButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: "I'm a radio button",
        onCheck: () => console.log('pressed on check'),
        checked: true,
    },
};
