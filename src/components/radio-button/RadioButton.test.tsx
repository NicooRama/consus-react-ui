import {RadioButton} from "./RadioButton";
import {render} from "../../../test/test.utils";

describe('<RadioButton />', () => {
    it('renders <RadioButton /> successfully', () => {
        const {getByTestId} = render(<RadioButton data-testid={'radio-button'} onCheck={() => {}}/>)
        expect(getByTestId('radio-button')).toBeInTheDocument();
    });
});
