import type {Meta, StoryObj} from '@storybook/react';
import {DatePicker} from './DatePicker';

const meta: any = {
    title: 'Date Picker/DatePicker',
    component: DatePicker,
    tags: ['autodocs'],
} satisfies Meta<typeof DatePicker>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        onChange: (date: Date) => { console.log(date) },
        selected: new Date(),
        inline: true,
    },
    argTypes: {
        selected: {
            control: 'date',
        }
    }
};
