import React from 'react'
import DatePickerLib from 'react-datepicker'
import { configDatePicker, datePickerCustomOptions } from './datePickerConfig'

configDatePicker()

export interface DatePickerProps {
  onChange: (date: Date) => void
  selected?: Date
  startDate?: Date
  endDate?: Date
  inline?: boolean
  showTimeSelect?: boolean
  selectsStart?: boolean
  selectsEnd?: boolean
  [props: string]: any;
}

export const DatePicker: React.FC<DatePickerProps> = ({ onChange, inline = true, ...props }) => {
  return (
    <DatePickerLib {...datePickerCustomOptions} onChange={onChange} inline={inline} {...props} />
  )
}
