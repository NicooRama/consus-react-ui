import {createGlobalStyle} from "styled-components";
import pickerStyles from 'react-datepicker/dist/react-datepicker.css?inline'
export const DatePickerGlobalStyles = createGlobalStyle`
  ${pickerStyles}
  
  .react-datepicker-popper {
    z-index: 20;
  }

  .react-datepicker {
    display: inline-flex;
    border-color: ${({ theme }: any) => theme.colors.border};
  }

  .react-datepicker-wrapper {
    width: 100%;
  }
  
  .react-datepicker-wrapper > div > input {
    max-width: -moz-available;
    max-width: -webkit-fill-available;
    width: 100%;
    padding: ${({ theme }: any) => theme.spacing.sm}px;
    border: ${({ theme }: any) => theme.border};
    border-color: ${({ theme }: any) => theme.colors.inputs.border};
    border-radius: ${({ theme }: any) => theme.inputs.borderRadius}px;
    line-height: 0;
  }

  .react-datepicker__header {
    border-color: ${({ theme }: any) => theme.colors.border};
    background-color: ${({ theme }: any) => theme.colors.inputs.disabled};
  }
  
  .react-datepicker__month-container .react-datepicker__header {
    border-color: ${({ theme }: any) => theme.colors.border};
  }

  .react-datepicker__current-month {
    text-transform: capitalize;
  }

  .react-datepicker__day-name {
    text-transform: capitalize;
  }

  .react-datepicker__day--selected,
  .react-datepicker__day--keyboard-selected {
    background-color: ${({ theme }: any) => theme.colors.primary.normal};
    color: ${({ theme }: any) => theme.colors.calendarText};
    transition-duration: ${({ theme }: any) => theme.transitionDuration};

    &:hover {
      background-color: ${({ theme }: any) => theme.colors.primary.darken};
    }
  }

  .react-datepicker__time-container {
    border-color: ${({ theme }: any) => theme.colors.border};

    .react-datepicker__time {
      .react-datepicker__header.react-datepicker__header--time {
        border-color: ${({ theme }: any) => theme.colors.border};
      }

      .react-datepicker__time-box {
        ul.react-datepicker__time-list {
          li.react-datepicker__time-list-item {
            height: auto;

            &.react-datepicker__time-list-item--selected {
              background-color: ${({ theme }: any) => theme.colors.primary.normal};
              color: ${({ theme }: any) => theme.colors.calendarText};
              transition-duration: 0.2s;

              &:hover {
                background-color: ${({ theme }: any) => theme.colors.primary.darken};
              }
            }
          }
        }
      }
    }
  }

  .react-datepicker__day--in-selecting-range {
    background-color: ${({ theme }: any) => theme.colors.primary.dark};
    opacity: 0.5;
    color: ${({ theme }: any) => theme.colors.calendarText};

    &:not(.react-datepicker__day--in-range) {
      background-color: ${({ theme }: any) => theme.colors.primary.darken};
    }
  }

  .react-datepicker__day--in-range:not(.react-datepicker__day--selected) {
    background-color: ${({ theme }: any) => theme.colors.primary.dark};
    opacity: 0.5;
    color: ${({ theme }: any) => theme.colors.calendarText};
  }
`;
