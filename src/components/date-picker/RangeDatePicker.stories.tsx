import type {Meta, StoryObj} from '@storybook/react';
import {RangeDatePicker} from './RangeDatePicker';

const meta: any = {
    title: 'Date Picker/RangeDatePicker',
    component: RangeDatePicker,
    tags: ['autodocs'],
} satisfies Meta<typeof RangeDatePicker>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        startDate: new Date(),
        endDate: new Date(),
        onStartChange: (date: Date) => { console.log(date) },
        onEndChange: (date: Date) => { console.log(date) },
    }
};
