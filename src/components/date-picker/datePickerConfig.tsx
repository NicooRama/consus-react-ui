import {registerLocale, setDefaultLocale} from "react-datepicker";
import es from 'date-fns/locale/es';
import {Portal} from "react-overlays";

//https://reactdatepicker.com/
export function configDatePicker(){
    registerLocale('es', es);
    setDefaultLocale('es');
}

const CalendarContainer = ({ children }: {children: any}) => {
    const el = document.getElementById('calendar-portal');
    return (
        <Portal container={el}>
            {children}
        </Portal>
    )
};

export const datePickerCustomOptions = {
    dateFormat: "dd/MM/yyyy HH:mm",
    showTimeSelect: true,
    timeIntervals: 10,
    timeCaption: "Hora",
    fixedHeight: true,
    timeFormat: "HH:mm",
    popperContainer: CalendarContainer
};
