import {DatePicker} from "./DatePicker";
import {render} from "../../../test/test.utils";
import {screen} from "@testing-library/react";

describe('<DatePicker />', () => {
    it('renders <DatePicker /> successfully', () => {
        render(<DatePicker data-testid={'date-picker'} onChange={() => {}}/>)
        expect(screen.getAllByRole('button')[0]).toBeInTheDocument();
    });
});
