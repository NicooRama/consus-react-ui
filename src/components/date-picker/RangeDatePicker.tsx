import React from 'react'
import { DatePicker } from './DatePicker'

import { mobileMediaQuery } from '../../styles/common'
import styled from 'styled-components'

const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: ${({ theme }: any) => theme.spacing.md}px;
  width: fit-content;

  ${mobileMediaQuery} {
    grid-template-columns: 1fr;
  }
`

export interface RangeDatePickerProps {
  startDate: Date
  endDate: Date
  onStartChange: (date: Date) => void
  onEndChange: (date: Date) => void
  className?: string
}

export const RangeDatePicker: React.FC<RangeDatePickerProps> = ({
  startDate,
  onStartChange,
  endDate,
  onEndChange,
  className = '',
  ...props
}) => {
  return (
    <Container className={className}>
      <DatePicker
        showTimeSelect={false}
        inline
        selectsStart
        {...props}
        selected={startDate}
        startDate={startDate}
        endDate={endDate}
        onChange={onStartChange}
      />
      <DatePicker
        showTimeSelect={false}
        inline
        selectsEnd
        {...props}
        selected={endDate}
        startDate={startDate}
        endDate={endDate}
        onChange={onEndChange}
      />
    </Container>
  )
}
