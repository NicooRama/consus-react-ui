import {Icon} from "./Icon";
import {render} from "../../../test/test.utils";
import {icons} from "./icons";

describe('<Icon />', () => {
    it('renders <Icon /> successfully', () => {
        const {getByTestId} = render(<Icon data-testid={'icon'} icon={icons.check}/>)
        expect(getByTestId('icon')).toBeInTheDocument();
    });
});
