export const fontAwesomeToIcon = (fontAwesomeIcon: any) => {
    const { icon }: any = fontAwesomeIcon;
    return {
        path: icon[4],
        viewBox: `0 0 ${icon[0]} ${icon[1]}`,
    }
};
