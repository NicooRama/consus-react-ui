import type { Meta, StoryObj } from '@storybook/react'
import { Icon } from './Icon'
import { IconIndex, icons } from './icons'
import styled from 'styled-components'
import { Text } from '../text/Text'

const meta: any = {
  title: 'Commons/Icon',
  component: Icon,
  tags: ['autodocs'],
} satisfies Meta<typeof Icon>

export default meta
type Story = StoryObj<typeof meta>

export const Primary: Story = {
  args: {
    icon: icons.check,
  },
}

const Container = styled.div`
  display: flex;
  gap: 8px;
  flex-direction: column;

  & > div {
    display: flex;
    gap: 8px;
    align-items: center;
  }
`

export const All: Story = {
  render: () => {
    return (
      <Container>
        {
          Object.keys(icons).map((iconKey: any) => <div key={iconKey}>
            <Icon icon={icons[iconKey as IconIndex]} />
            <Text size={'sm'}>{iconKey}</Text>
          </div>)
        }
      </Container>
    )
  },
}
