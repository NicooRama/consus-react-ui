import React from 'react'
import { ConsusTheme, ThemeColorIndex } from '../../styles/theme.interface'
import { getIn } from '../../utils/utils'
import styled, { useTheme } from 'styled-components'

export interface IconInterface {
  viewBox: string
  path: string

  [props: string]: any
}

export interface IconProps {
  icon: IconInterface
  className?: string
  width?: number
  height?: number
  title?: string
  color?: ThemeColorIndex
}

const Component = styled.svg<{ color: ThemeColorIndex, fill?: string }>`
  path {
    fill: ${({fill, color, theme}) => fill || getIn(theme.colors, color)}
  }
`

export const Icon: React.FC<IconProps> = ({
  icon,
  className = '',
  width = 20,
  height = 20,
  title,
  color = 'primary.normal',
  ...props
}) => {
  const theme = useTheme() as ConsusTheme
  return (
    <Component
      className={className}
      width={width}
      height={height}
      stroke={getIn(theme.colors, color)}
      color={color}
      fill={icon?.fill}
      {...icon}
      {...props}
    >
      <path d={icon.path} />
      <title>{title}</title>
    </Component>
  )
}
