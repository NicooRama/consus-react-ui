import type {Meta, StoryObj} from '@storybook/react';
import {InteractiveRemovePopover} from './InteractiveRemovePopover';
import {PopoverStorybookDecorator} from "../popover/popover.storybookDecorator";

const meta: any = {
    title: 'Popover/InteractiveRemovePopover',
    component: InteractiveRemovePopover,
    tags: ['autodocs'],
    decorators: [PopoverStorybookDecorator]
} satisfies Meta<typeof InteractiveRemovePopover>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        onAccept: () => console.log('clicked'),
        onCorner: false,
        message: 'Are you sure you want to remove this item?',
    },
};
