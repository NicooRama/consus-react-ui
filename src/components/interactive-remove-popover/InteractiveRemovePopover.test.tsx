import {InteractiveRemovePopover} from "./InteractiveRemovePopover";
import {render} from "../../../test/test.utils";

describe('<InteractiveRemovePopover />', () => {
    it('renders <InteractiveRemovePopover /> successfully', () => {
        const {getByTestId} = render(<InteractiveRemovePopover
            onAccept={() => {}}
            message={'message'}
            data-testid={'interactive-remove-popover'}/>)
        expect(getByTestId('interactive-remove-popover')).toBeInTheDocument();
    });
});
