import React from 'react';
import styled, {css} from 'styled-components';
import {ConfirmationPopoverButton} from "../popover/ConfirmationPopoverButton";
import {InteractiveRemoveButton} from "../interactive-remove-button/InteractiveRemoveButton";

const Container = styled.div<{ onCorner: boolean }>`
  ${({onCorner}: any) =>
          onCorner &&
          css`
            position: absolute;
            right: -10px;
            top: -9px;
            opacity: 0;
            z-index: 1;
          `}
`

export interface InteractiveRemovePopoverProps {
    onAccept: () => any;
    message: string;
    onCorner?: boolean;
    "data-testid"?: string;

    [props: string]: any;
}

export const InteractiveRemovePopover: React.FC<InteractiveRemovePopoverProps> = ({
                                                                                      onAccept,
                                                                                      message,
                                                                                      className = '',
                                                                                      onCorner = true,
                                                                                      ...props
                                                                                  }) => {
    return (
        <Container className={className} onCorner={onCorner}>
            <ConfirmationPopoverButton message={message}
                                       onAccept={onAccept}
                                       className={"interactive-remove-button"}
                                       {...props}
            >
                {
                    ({onClick}) => <InteractiveRemoveButton onClick={onClick}
                                                            data-testid={props['data-testid']}
                    />
                }
            </ConfirmationPopoverButton>
        </Container>
    )
};
