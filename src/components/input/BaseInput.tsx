import React from 'react'

import { inputStyles } from './input.styles'
import styled from 'styled-components'
import {Spacing} from "../../utils/types";

const Component = styled.input<{ size: Spacing }>`
  ${({ theme, size }) => inputStyles(size, theme)};
  resize: vertical;
`

export interface BaseInputProps {
  className?: string
  onFocus?: () => void
  onChange?: (event: Event) => void
  onTextChange?: (value: string) => void
  selectOnFocus?: boolean
  size?: Spacing
  tag?: 'input' | 'textarea'

  [props: string]: any
}

export const BaseInput: React.FC<BaseInputProps> = ({
  className = '',
  onFocus = () => {},
  onChange = () => {},
  onTextChange,
  selectOnFocus = true,
  size = 'sm',
  tag = 'input',
  inputRef,
  ...props
}) => {
  const handleFocus = (e: any) => {
    const target = e.target as any
    if (selectOnFocus) {
      target.select()
    }
    onFocus()
  }

  const handleChange = (e: any): void => {
    const target = e.target as any
    if (onTextChange) {
      onTextChange(target.value)
      return
    }
    onChange(e)
  }

  return (
    <Component
      as={tag}
      ref={inputRef}
      onFocus={handleFocus}
      onChange={handleChange}
      className={`${className}`}
      rows={5}
      size={size}
      {...props}
    />
  )
}
