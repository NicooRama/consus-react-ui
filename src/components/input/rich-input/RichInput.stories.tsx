import type {Meta, StoryObj} from '@storybook/react';
import {RichInput} from './RichInput';

const meta: any = {
    title: 'Input/RichInput',
    component: RichInput,
    tags: ['autodocs'],
} satisfies Meta<typeof RichInput>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        onChange: () => {},
    },
};
