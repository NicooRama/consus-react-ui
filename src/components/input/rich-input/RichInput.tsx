import { Editor, EditorState, getDefaultKeyBinding, RichUtils } from 'draft-js'
import React, { useRef, useState } from 'react'
import 'draft-js/dist/Draft.css'
import { RichInputControls } from './components/RichInputControls'
import styled from 'styled-components'
import { css } from 'styled-components'
import { convertToHTML, convertFromHTML } from 'draft-convert'

const Container = styled.div`
  background: #fff;
  border: ${({ theme }: any) => theme.border};
  border-radius: ${({ theme }: any) => theme.borderRadius}px;
  font-size: ${({ theme }: any) => theme.font.size.xs}px;
  padding: ${({ theme }: any) => theme.spacing.sm}px;
`

const EditorContainer = styled.div<{ hidePlaceholder: boolean }>`
  border-top: ${({ theme }: any) => theme.border};
  cursor: text;
  font-size: ${({ theme }: any) => theme.font.size.xs}px;
  margin-top: ${({ theme }: any) => theme.spacing.xs}px;

  .public-DraftEditorPlaceholder-root,
  .public-DraftEditor-content {
    padding-top: ${({ theme }: any) => theme.spacing.xs}px;
    overflow-wrap: anywhere !important;
  }

  .public-DraftEditor-content {
    min-height: 100px;
  }

  ${({ hidePlaceholder }) =>
    hidePlaceholder &&
    css`
      .public-DraftEditorPlaceholder-root {
        display: none;
      }
    `}
  
  .RichEditor-blockquote {
    border-left: 5px solid #eee;
    color: #666;
    font-family: 'Hoefler Text', 'Georgia', serif;
    font-style: italic;
    margin: 16px 0;
    padding: 10px 20px;
  }

  .public-DraftStyleDefault-pre {
    background-color: rgba(0, 0, 0, 0.05);
    font-family: 'Inconsolata', 'Menlo', 'Consolas', monospace;
    font-size: 16px;
    padding: 20px;
  }
`

// Custom overrides for "code" style.
const styleMap = {
  CODE: {
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2,
  },
}

function getBlockStyle(block: any) {
  switch (block.getType()) {
    case 'blockquote':
      return 'RichEditor-blockquote'
    default:
      return null
  }
}

export interface RichInputProps {
  onChange: (html: string) => void
  initialValue?: string
  containerClassName?: string
}

export const RichInput: React.FC<RichInputProps> = ({
  onChange,
  containerClassName = '',
  initialValue,
  ...props
}) => {
  const editorRef = useRef(null)
  const [editorState, setEditorState] = useState<EditorState>(
    initialValue
      ? EditorState.createWithContent(convertFromHTML(initialValue))
      : EditorState.createEmpty(),
  )
  const handleChange = (editorState: EditorState) => {
    setEditorState(editorState)
    onChange(convertToHTML(editorState.getCurrentContent()))
  }

  const handleKeyCommand = (command: any, editorState: EditorState) => {
    const newEditorState: EditorState | null = RichUtils.handleKeyCommand(editorState, command)
    if (newEditorState) {
      handleChange(newEditorState)
      return true
    }
    return false
  }

  const mapKeyToEditorCommand = (e: any) => {
    if (e.keyCode === 9 /* TAB */) {
      const newEditorState = RichUtils.onTab(e, editorState, 4 /* maxDepth */)
      if (newEditorState !== editorState) {
        handleChange(newEditorState)
      }
      return
    }
    return getDefaultKeyBinding(e)
  }

  const toggleBlockType = (blockType: any) => {
    handleChange(RichUtils.toggleBlockType(editorState, blockType))
  }

  const toggleInlineStyle = (inlineStyle: any) => {
    handleChange(RichUtils.toggleInlineStyle(editorState, inlineStyle))
  }

  let hidePlaceholder = false
  let contentState = editorState.getCurrentContent()
  if (!contentState.hasText()) {
    if (contentState.getBlockMap().first().getType() !== 'unstyled') {
      hidePlaceholder = true
    }
  }

  return (
    <Container className={containerClassName} data-testid={(props as any)['data-testid']}>
      <RichInputControls
        editorState={editorState}
        onToggleBlockType={toggleBlockType}
        onToggleInlineStyle={toggleInlineStyle}
      />
      <EditorContainer
        hidePlaceholder={hidePlaceholder}
        onClick={() => (editorRef?.current as any).focus()}
      >
        <Editor
          blockStyleFn={getBlockStyle as any}
          customStyleMap={styleMap}
          editorState={editorState}
          handleKeyCommand={handleKeyCommand as any}
          keyBindingFn={mapKeyToEditorCommand as any}
          onChange={handleChange}
          ref={editorRef}
          spellCheck={true}
          {...props}
        />
      </EditorContainer>
    </Container>
  )
}
