import { RichInputControls } from './RichInputControls'
import { render } from '../../../../../test/test.utils'
import { EditorState } from 'draft-js'

describe('<RichInputControls />', () => {
  it('renders <RichInputControls /> successfully', () => {
    const { getByTestId } = render(
      <RichInputControls
        data-testid={'rich-input-controls'}
        editorState={EditorState.createEmpty()}
        onToggleBlockType={() => {}}
        onToggleInlineStyle={() => {}}
      />,
    )
    expect(getByTestId('rich-input-controls')).toBeInTheDocument()
  })
})
