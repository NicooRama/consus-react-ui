import {RichInputControlButton} from "./RichInputControlButton";
import {render} from "../../../../../test/test.utils";
import {icons} from "../../../icon/icons";

describe('<RichInputControlButton />', () => {
    it('renders <RichInputControlButton /> successfully', () => {
        const {getByTestId} = render(<RichInputControlButton data-testid={'rich-input-control-button'} active={false} icon={icons.h5} label={'H5'} onMouseDown={() => {}}/>)
        expect(getByTestId('rich-input-control-button')).toBeInTheDocument();
    });
});
