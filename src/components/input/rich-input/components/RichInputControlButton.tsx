import React from 'react'
import styled from 'styled-components'
import { Icon, IconInterface } from '../../../icon/Icon'

export interface RichInputControlButtonProps {
  active: boolean
  label: string
  icon: IconInterface
  onMouseDown: (e: any) => void
}

const ControlButton = styled.span`
  cursor: pointer;
`

export const RichInputControlButton: React.FC<RichInputControlButtonProps> = ({
  label,
  icon,
  active,
  onMouseDown,
  ...props
}) => {
  return (
    <ControlButton onMouseDown={onMouseDown} {...props}>
      <Icon
        icon={icon}
        color={active ? 'primary.darken' : 'gray.darken'}
        width={16}
        height={16}
        title={label}
      />
    </ControlButton>
  )
}
