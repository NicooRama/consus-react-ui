import styled from 'styled-components'
import { EditorState } from 'draft-js'
import React from 'react'
import { icons } from '../../../icon/icons'
import { RichInputControlButton } from './RichInputControlButton'

const BLOCK_TYPES = [
  { label: 'Encabezado 1', icon: icons.h1, style: 'header-one' },
  { label: 'Encabezado 2', icon: icons.h2, style: 'header-two' },
  { label: 'Encabezado 3', icon: icons.h3, style: 'header-three' },
  { label: 'Encabezado 4', icon: icons.h4, style: 'header-four' },
  { label: 'Encabezado 5', icon: icons.h5, style: 'header-five' },
  { label: 'Encabezado 6', icon: icons.h6, style: 'header-six' },
  { label: 'Cita', icon: icons.quote, style: 'blockquote' },
  { label: 'Lista', icon: icons.unorderedList, style: 'unordered-list-item' },
  { label: 'Lista ordenada', icon: icons.orderedList, style: 'ordered-list-item' },
  { label: 'Bloque', icon: icons.code, style: 'code-block' },
]

var INLINE_STYLES = [
  { label: 'Negrita', icon: icons.bold, style: 'BOLD' },
  { label: 'Cursiva', icon: icons.italic, style: 'ITALIC' },
  { label: 'Subrayado', icon: icons.underline, style: 'UNDERLINE' },
  // {label: 'Monospace', style: 'CODE'},
]

export interface RichInputControlsProps {
  editorState: EditorState
  onToggleBlockType: (blockType: any) => void
  onToggleInlineStyle: (inlineStyle: any) => void
}

const Container = styled.div`
  font-size: ${({ theme }: any) => theme.font.size.xs}px;
  user-select: none;
  display: flex;
  flex-wrap: wrap;
  gap: ${({ theme }: any) => theme.spacing.xs}px;
  flex-direction: row;
  align-items: baseline;
`

export const RichInputControls: React.FC<RichInputControlsProps> = ({
  editorState,
  onToggleBlockType,
  onToggleInlineStyle,
  ...props
}) => {
  const selection = editorState.getSelection()
  const blockType = editorState
    .getCurrentContent()
    .getBlockForKey(selection.getStartKey())
    .getType()
  const currentStyle = editorState.getCurrentInlineStyle()

  const handleToggle = (style: any, onToggle: any): any => {
    return (e: any) => {
      e.preventDefault()
      onToggle(style)
    }
  }

  return (
    <Container {...props}>
      {BLOCK_TYPES.map((type) => (
        <RichInputControlButton
          label={type.label}
          icon={type.icon}
          key={type.label}
          active={type.style === blockType}
          onMouseDown={handleToggle(type.style, onToggleBlockType)}
        />
      ))}
      {INLINE_STYLES.map((type) => (
        <RichInputControlButton
          label={type.label}
          icon={type.icon}
          key={type.label}
          active={currentStyle.has(type.style)}
          onMouseDown={handleToggle(type.style, onToggleInlineStyle)}
        />
      ))}
    </Container>
  )
}
