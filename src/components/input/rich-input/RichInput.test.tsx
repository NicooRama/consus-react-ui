import {RichInput} from "./RichInput";
import {render} from "../../../../test/test.utils";

describe('<RichInput />', () => {
    it('renders <RichInput /> successfully', () => {
        const {getByTestId} = render(<RichInput data-testid={'rich-input'} onChange={() => {}}/>)
        expect(getByTestId('rich-input')).toBeInTheDocument();
    });
});
