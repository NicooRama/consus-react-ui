import type {Meta, StoryObj} from '@storybook/react';
import {PasswordInput} from './PasswordInput';

const meta: any = {
    title: 'Input/PasswordInput',
    component: PasswordInput,
    tags: ['autodocs'],
} satisfies Meta<typeof PasswordInput>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        value: "I'm a Input"
    },
};
