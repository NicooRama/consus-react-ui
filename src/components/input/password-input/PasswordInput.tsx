import React from 'react'
import styled from 'styled-components'
import { ToggleButton } from '../../icon-button/ToggleButton'
import { mobileMediaQuery } from '../../../styles/common'
import { faEye, faEyeSlash } from '@fortawesome/free-regular-svg-icons'
import { BaseInput, BaseInputProps } from '../BaseInput'
import { useToggle } from '../../../hooks/useToggle'

const Container = styled.div`
  position: relative;

  .password-input {
    padding-right: 45px;
  }
`

const SwitchMaskButton = styled(ToggleButton)`
  position: absolute;
  padding: ${({ theme }: any) => theme.spacing.sm - 1}px ${({ theme }: any) => theme.spacing.sm}px;
  right: 0;
  top: 0;

  width: fit-content;

  ${mobileMediaQuery} {
    width: fit-content;
  }
`

export interface PasswordInputProps extends BaseInputProps {
  initialHidden?: boolean
}

export const PasswordInput: React.FC<PasswordInputProps> = ({ initialHidden = true, ...props }) => {
  const [enabled, { toggle }]: any = useToggle(initialHidden)

  return (
    <Container>
      <BaseInput {...props} type={enabled ? 'password' : 'text'} className={'password-input'} />
      <SwitchMaskButton
        enabled={enabled}
        enabledIcon={faEyeSlash}
        disabledIcon={faEye}
        onClick={toggle}
        // @ts-ignore
        color={'gray.darkest'}
        variant={'tertiary'}
      />
    </Container>
  )
}
