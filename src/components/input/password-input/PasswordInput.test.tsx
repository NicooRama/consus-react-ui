import {PasswordInput} from "./PasswordInput";
import {render} from "../../../../test/test.utils";

describe('<PasswordInput />', () => {
    it('renders <PasswordInput /> successfully', () => {
        const {getByTestId} = render(<PasswordInput data-testid={'password-input'}/>)
        expect(getByTestId('password-input')).toBeInTheDocument();
    });
});
