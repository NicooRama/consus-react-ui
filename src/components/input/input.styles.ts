import { ConsusTheme } from '../../styles/theme.interface'
import { Spacing } from '../../utils/types'
import { css } from 'styled-components'

export const inputStyles = (size: Spacing, theme: ConsusTheme) => css`
  width: -moz-available;
  width: -webkit-fill-available;
  padding: ${theme.spacing[size]}px;
  border: ${theme.border};
  border-color: ${theme.colors.inputs.border};
  border-radius: ${theme.inputs.borderRadius}px;
  background-color: white;
  // line-height: 0;
  &.fix-firefox {
    width: -moz-available;
  }

  &:disabled {
    background-color: ${theme.colors.disabled};
  }
`
