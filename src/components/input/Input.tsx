import React from 'react'
import { BaseInput, BaseInputProps } from './BaseInput'
import { PasswordInput } from './password-input/PasswordInput'

export type InputProps = BaseInputProps
export const Input: React.FC<InputProps> = ({
                          ...props
                      }: BaseInputProps) => {
    if(props.type === 'password') return <PasswordInput {...props} />
    return <BaseInput {...props} />
};
