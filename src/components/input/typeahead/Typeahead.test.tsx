import {Typeahead} from "./Typeahead";
import {render} from "../../../../test/test.utils";

describe('<Typeahead />', () => {
    it('renders <Typeahead /> successfully', () => {
        const {getByRole} = render(<Typeahead id={'typeahead'} options={[]}/>)
        expect(getByRole('combobox')).toBeInTheDocument();
    });
});
