export const typeaheadConfig = {
    emptyLabel: "No hay opciones",
    paginationText: "Mostrar mas resultados",
    promptText: 'Buscando...',
    searchText: 'Buscando...'
}
