import {ConsusTheme} from "../../../styles/theme.interface";
import {Typeahead as ReactTypeahead, AsyncTypeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';

import {inputStyles} from "../input.styles";
import {Spacing} from "../../../utils/types";
import {optionsStyles, optionStyles} from "../../select/optionsStyles";
import {opacify} from "polished";
import {typeaheadConfig} from "./typeahead.config";
import React, {ChangeEvent, useEffect, useState} from 'react';
import {StrUtils, sortPrioritizingStart} from '@consus/js-utils';
import styled, {css} from "styled-components";

const styles = (size: Spacing, theme: ConsusTheme) => css`
  input {
    ${inputStyles(size, theme)}
  }
  
    .rbt-input-multi {
        ${inputStyles(size, theme)}
        input {
            height: ${(theme.spacing[size] * 2) - 3}px;
        }
      
        .rbt-input-main {
            margin: 0;
            padding-left: ${theme.spacing.xxs}px !important;
        }
      
        .rbt-input-hint {
            // width: auto !important
        }
      
        .rbt-close {
            padding: 0;
            background-color: transparent;
            border: 0;
            padding-right: 7px;
            cursor: pointer;
        }
      
        .rbt-token {
            background-color: ${opacify(0.5, theme.colors.primary.normal)};
            color: ${theme.colors.text};
            font-size: ${theme.font.size.xs}px;
            padding-top: 2px;
            padding-bottom: 2px;
          
            .rbt-token-label {
                padding: 0;
            }
        }
      
        .rbt-token-active {
            background-color: ${opacify(0.5, theme.colors.primary.normal)};
        }
    }
  
    .rbt-input-hint {
      
    }
  
    .rbt-menu {
      ${optionsStyles(theme)}
      
      a {
        ${optionStyles(theme)};
        width: auto;
        background-color: transparent;
        display: block;
        text-decoration: none;
        color: ${theme.colors.typographic.solid};
        cursor: pointer;
        
        .rbt-highlight-text {
          background-color: transparent;
        }
        
        &.active {
            background-color: ${theme.colors.primary.normal};
        }
      }
    }

  span.sr-only.visually-hidden {
    display: none;
  }

  div.rbt-aux > button {
    background-color: transparent;
    font-size: ${theme.font.size.lg}px;
    padding: 0;
    border: none;
    cursor: pointer
    
  }

`

export interface TypeaheadProps{
    controlledSort?: boolean,
    type?: 'normal' | 'async',
    loading?: boolean;
    resultsPerPage?: number;
    options: any[];

    [props: string]: any;
}

const StyledReactTypeahead = styled(ReactTypeahead)`
    ${({size, theme}: any) => styles(size, theme)}
`

const StyledAsyncTypeahead = styled(AsyncTypeahead)`
    ${({size, theme}: any) => styles(size, theme)}
`

export const Typeahead: React.FC<TypeaheadProps> = ({
                              size = 'sm',
                              options,
                              onInputChange,
                              controlledSort = false,
                              type = 'normal',
                              loading = false,
                              resultsPerPage,
                              onPaginate = () => {},
                              ...props
                          }) => {
    const Component = type === 'normal' ? StyledReactTypeahead : StyledAsyncTypeahead;

    const [sortedOptions, setSortedOptions] = useState<any[]>([]);

    useEffect(() => {
        if (!controlledSort) {
            return
        }
        setSortedOptions([...options]);
    }, [options, options.length, controlledSort])

    const setNormalizedSearchText = (text: string, e: ChangeEvent<HTMLInputElement>) => {
        if (onInputChange) {
            onInputChange(text, e);
        }
        if (!controlledSort) {
            return
        }
        const searchText = StrUtils.normalize(text.trim());
        if (!searchText) setSortedOptions([...options]);
        sortedOptions.sort(sortPrioritizingStart(searchText, props.labelKey as any))
    }

    return (
        //@ts-ignore
        <Component
            id={props.id || 'typeahead'}
            size={size}
            isLoading={loading}
            {...typeaheadConfig}
            clearButton={true}
            options={controlledSort ? sortedOptions : options}
            // @ts-ignore
            onInputChange={setNormalizedSearchText}
            onSearch={() => {
            }}
            onPaginate={onPaginate}
            // @ts-ignore
            maxResults={resultsPerPage - 1}
            // @ts-ignore
            minLength={2}
            // @ts-ignore
            paginate
            useCache={false}
            {...props}/>)
};
