/**
 * must to do use this function when allowNew prop is enabled
 */
export const parseTypeaheadNewItems = (items: any[]) => items.map((item: any) => {
    if(item._id) return item;
    return {name: item.name}
});

export const allowNew = (values: any[], comparatorKey: string | null) => (_: Array<Object|string>, props: Object) => {
    const newItem = (props as any).text;
    const options = [...(props as any).options, ...values];
    if(!comparatorKey) return options.every((item: any) => item !== newItem);
    return options.every((item: any) => item[comparatorKey] !== newItem);
}
