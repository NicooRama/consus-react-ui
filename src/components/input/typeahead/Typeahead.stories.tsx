import type { Meta, StoryObj } from '@storybook/react'
import { Typeahead } from './Typeahead'
import { useEffect, useState } from 'react'
import { useDebounce } from '../../../hooks/useDebounce'

const meta: any = {
  title: 'Input/Typeahead',
  component: Typeahead,
  tags: ['autodocs'],
} satisfies Meta<typeof Typeahead>

export default meta
type Story = StoryObj<typeof meta>

const argTypes = {
  multiple: {
    control: {
      type: 'boolean',
    },
  },
}

export const Primary: Story = {
  args: {
    options: ['Item 1', 'Item 2', 'Item 3'],
  },
  argTypes,
}

export const WithTwoInputs = {
  render: () => {
    const props = {
      options: [
        { name: 'Item 1' },
        { name: 'Item 2' },
        { name: 'Item 3' },
        { name: 'Item 4' },
        { name: 'Item 5' },
      ],
      labelKey: 'name',
      clearButton: true,
      id: 'typeahead',
    }
    return (
      <>
        <Typeahead {...props} />
        <Typeahead {...props} />
      </>
    )
  },
}

export const WithObjects = {
  args: {
    options: [
      { name: 'Item 1' },
      { name: 'Item 2' },
      { name: 'Item 3' },
      { name: 'Item 4' },
      { name: 'Item 5' },
    ],
    labelKey: 'name',
    clearButton: true,
    id: 'typeahead',
  },
  argTypes,
}

export const ControlledSort = {
  args: {
    options: [
      { name: 'Frutilla' },
      { name: 'Banana con chocolate' },
      { name: 'Chocolate amargo' },
      { name: 'Chocolate semiamargo' },
      { name: 'Item 4' },
      { name: 'Item 5' },
    ],
    labelKey: 'name',
    clearButton: true,
    id: 'typeahead',
  },
  argTypes,
}

export const Async = {
  render: () => {
    const [options, setOptions] = useState<string[]>([])
    const [itemsInPage, setItemsInPage] = useState<string[]>([])
    const [name, setName] = useState('')
    const [debounceName] = useDebounce<string>(name, 300)
    const [pageNumber, setPageNumber] = useState(0)
    const [loading, setLoading] = useState(false)
    const totalPages = 3

    const changePage = (pageNumber: number) => {
      setLoading(true)
      setTimeout(() => {
        setLoading(false)
        const updatedItems = []
        for (let i = 0; i < 5; i++) {
          updatedItems.push(`Item ${i + 1 + (pageNumber - 1) * 5}`)
        }
        console.log(updatedItems)
        setItemsInPage(updatedItems)
        setPageNumber(pageNumber + 1)
        //fechear items segun pagina
      }, 1000)
    }

    useEffect(() => {
      if (debounceName === '') return
      if (pageNumber === 0) {
        changePage(1)
      }
    }, [debounceName])

    useEffect(() => {
      setOptions([...options, ...itemsInPage])
    }, [itemsInPage])

    const handlePagination = () => {
      if (pageNumber < totalPages) {
        changePage(pageNumber + 1)
      }
    }

    const handleInputChange = (q: string) => {
      setName(q)
    }

    return (
      <Typeahead
        id={'typeahead'}
        emptyLabel={'No hay opciones'}
        paginationText={'Mostrar mas resultados'}
        promptText={'Escribí para buscar...'}
        searchText={'Buscando...'}
        placeholder={'Placeholder'}
        onInputChange={handleInputChange}
        resultsPerPage={5}
        onPaginate={handlePagination}
        loading={loading}
        options={options}
        controlledSort={false}
        type={'async'}
      />
    )
  },
}
