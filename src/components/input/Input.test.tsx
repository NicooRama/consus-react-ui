import {Input} from "./Input";
import {render} from "../../../test/test.utils";

describe('<Input />', () => {
    it('renders <Input /> successfully', () => {
        const {getByTestId} = render(<Input data-testid={'input'}/>)
        expect(getByTestId('input')).toBeInTheDocument();
    });
});
