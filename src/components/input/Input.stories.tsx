import type {Meta, StoryObj} from '@storybook/react';
import {Input} from './Input';

const meta: any = {
    title: 'Input/Input',
    component: Input,
    tags: ['autodocs'],
} satisfies Meta<typeof Input>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        value: "I'm a Input",
    },
};
