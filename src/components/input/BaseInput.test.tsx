import {BaseInput} from "./BaseInput";
import {render} from "../../../test/test.utils";

describe('<BaseInput />', () => {
    it('renders <BaseInput /> successfully', () => {
        const {getByTestId} = render(<BaseInput data-testid={'base-input'}/>)
        expect(getByTestId('base-input')).toBeInTheDocument();
    });
});
