import React from "react"
import {faTrashAlt} from "@fortawesome/free-regular-svg-icons";
import {Link} from 'react-router-dom';
import {useState} from "react";
import {Loader} from "../loader/Loader";
import {Button} from "../button/Button";
import {IconButton} from "../icon-button/IconButton";
import {ConfirmationPopoverButton} from "../popover/ConfirmationPopoverButton";
import styled from "styled-components";
import {EditButton} from "../icon-button/EditButton";

const Container = styled.div`
  display: flex;
  gap: ${({theme}: any) => theme.spacing.xs}px;
  padding: ${({theme}: any) => theme.spacing.xs}px ${({theme}: any) => theme.spacing.lg}px;
  background-color: #F2F2F2;
  border-top: ${({theme}: any) => theme.border};
`

const ButtonsContainer = styled.div`
  margin-left: auto;
  display: flex;
  gap: ${({theme}: any) => theme.spacing.xs}px;
  align-items: center;
`

export interface CardFooterProps {
    editLink: string;
    editTitle: string;
    removeTitle: string;
    showRemoveButton?: boolean;
    showEditButton?: boolean;
    onClickRemove: () => void;
    onActionClick?: () => void;
    confirmationRemoveMessage: string;
    actionText?: string;
    showActionButton?: boolean;
    className?: string;
    children?: any;
    }

export const CardFooter: React.FC<CardFooterProps> = ({
                               onActionClick,
                               actionText,
                               editLink,
                               editTitle,
                               removeTitle,
                               onClickRemove,
                               confirmationRemoveMessage,
                               showActionButton = false,
                               showRemoveButton = true,
                               showEditButton = true,
                               className = "",
                               children,
                               ...props
                           }) => {

    const [loading, setLoading] = useState(false);

    const handleRemove = async () => {
        !!onClickRemove && await onClickRemove();
    }

    const handleActionClick = async () => {
        setLoading(true);
        !!onActionClick && await onActionClick();
        setLoading(false);
    }

    // @ts-ignore
    return (<Container className={className}  {...props}>
        {
            (showActionButton && !!onActionClick) && (
                loading ? <Loader/> : <Button onClick={handleActionClick}>
                    {actionText as string}
                </Button>
            )
        }
        <ButtonsContainer>
            {children}
            {
                showEditButton && <Link to={editLink}>
                    <EditButton circle
                                title={editTitle}
                                variant={'secondary'}
                                onClick={() => {}}/>
                </Link>
            }
            {
                showRemoveButton &&
                <ConfirmationPopoverButton message={confirmationRemoveMessage}
                                           onAccept={handleRemove as any}
                                           className={'confirmation-popover'}
                >
                    {
                        ({onClick}: any) => <IconButton
                            circle
                            color="error.normal"
                            icon={faTrashAlt}
                            onClick={onClick}
                            variant="secondary"
                            title={removeTitle}
                            className={'remove-button'}
                        />
                    }
                </ConfirmationPopoverButton>

            }
        </ButtonsContainer>
    </Container>)
};
