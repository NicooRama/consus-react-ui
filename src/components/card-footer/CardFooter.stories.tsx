import type { Meta, StoryObj } from '@storybook/react'
import { CardFooter } from './CardFooter'
import { routerDecorator } from '../../storybook.utils'
import { faCopy } from '@fortawesome/free-regular-svg-icons';
import {IconButton} from "../icon-button/IconButton";

const decorators = [
  routerDecorator,
  (Story: any) => (
      <div style={{ paddingTop: 120 }}>
        <Story />
      </div>
  ),
]

const meta: any = {
  title: 'Card/CardFooter',
  component: CardFooter,
  tags: ['autodocs'],
} satisfies Meta<typeof CardFooter>

export default meta
type Story = StoryObj<typeof meta>

export const Primary: Story = {
  args: {
    editLink: '/editar',
    editTitle: 'Editar',
    removeTitle: 'Eliminar entidad',
    showRemoveButton: true,
    confirmationRemoveMessage: '¿Estas seguro que queres eliminar la entidad?',
    actionText: 'Ver detalle',
    showActionButton: true,
  },
  decorators
}

export const WithChildren: Story = {
  args: {
    editLink: '/editar',
    editTitle: 'Editar',
    removeTitle: 'Eliminar entidad',
    showRemoveButton: true,
    confirmationRemoveMessage: '¿Estas seguro que queres eliminar la entidad?',
    actionText: 'Ver detalle',
    showActionButton: true,
    children: <IconButton
        circle
        color="info.normal"
        icon={faCopy}
        onClick={() => {}}
        variant="secondary"
        title={'Copiar'}
        className={'remove-button'}
    />
  },
  decorators
}
