import { CardFooter } from './CardFooter'
import {renderWithRouter} from '../../../test/test.utils'

describe('<CardFooter />', () => {
  it('renders <CardFooter /> successfully', () => {
    const { getByTestId } = renderWithRouter(
      <CardFooter
        data-testid={'card-footer'}
        confirmationRemoveMessage={'Message'}
        editLink={'/edit'}
        editTitle={'Title'}
        onClickRemove={() => {}}
        removeTitle={'Remove title'}
      />,
    )
    expect(getByTestId('card-footer')).toBeInTheDocument()
  })
})
