import type {Meta, StoryObj} from '@storybook/react';
import {ErrorImage} from './ErrorImage';

const meta: any = {
    title: 'Commons/ErrorImage',
    component: ErrorImage,
    tags: ['autodocs'],
} satisfies Meta<typeof ErrorImage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
};
