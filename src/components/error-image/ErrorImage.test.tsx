import {ErrorImage} from "./ErrorImage";
import {render} from "../../../test/test.utils";

describe('<ErrorImage />', () => {
    it('renders <ErrorImage /> successfully', () => {
        const {getByTestId} = render(<ErrorImage data-testid={'error-image'}/>)
        expect(getByTestId('error-image')).toBeInTheDocument();
    });
});
    