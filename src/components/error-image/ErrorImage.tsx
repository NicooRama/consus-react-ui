import React from 'react'; 
import styled from 'styled-components';

const Container = styled.div`
`

export interface ErrorImageProps {
    "data-testid"?: string;
    [props: string]: any;
}

export const ErrorImage: React.FC<ErrorImageProps> = ({...props}) => {
    return (<Container {...props}>

    </Container>)
};
;