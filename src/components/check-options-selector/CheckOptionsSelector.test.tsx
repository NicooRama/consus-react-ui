import {CheckOptionsSelector} from "./CheckOptionsSelector";
import {render} from "../../../test/test.utils";
import {screen} from '@testing-library/react';

describe('<CheckOptionsSelector />', () => {
    it('renders selecteds options correctly', () => {
        render(<CheckOptionsSelector onChange={() => {}} options={['Hello', 'World']} />)
        const options = screen.getAllByTestId('checkbox');
        expect(options).toHaveLength(2);
    })

    it('rerenders selecteds options correctly when new element is added to options', () => {
        const options = ['Hello'];
        const {rerender} = render(<CheckOptionsSelector onChange={() => {}} options={options} />);
        options.push("World");
        rerender(<CheckOptionsSelector onChange={() => {}} options={options} />);
        const renderedOptions = screen.getAllByTestId('checkbox');
        expect(renderedOptions).toHaveLength(2);
    })

    it('rerenders selecteds options correctly when element changes in options', () => {
        const options = ['Hello', 'World'];
        const {rerender} = render(<CheckOptionsSelector onChange={() => {}} options={options} />);
        options[0] = 'Hi';
        rerender(<CheckOptionsSelector onChange={() => {}} options={options} />);
        const renderedOptions = screen.getAllByTestId('checkbox');
        expect(renderedOptions[0]).toHaveTextContent('Hi')
    })

    it('selects a option correctly', () => {
        const options = ["Hello", "World"];
        const onChange = jest.fn();
        render(<CheckOptionsSelector onChange={onChange} options={options} />)
        const buttons = screen.getAllByRole('button');
        buttons[0].click();
        expect(onChange).toBeCalledWith([options[0]])
    })

    it('deselects a option correctly', () => {
        const options = ["Hello", "World"];
        const onChange = jest.fn();
        render(<CheckOptionsSelector onChange={onChange} options={options} selecteds={options} />)
        const buttons = screen.getAllByRole('button');
        buttons[0].click();
        expect(onChange).toBeCalledWith([options[1]])
    })
});
