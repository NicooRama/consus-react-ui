import type {Meta, StoryObj} from '@storybook/react';
import {CheckOptionsSelector} from './CheckOptionsSelector';
import {makeControlForObjectArray, makeControlForRender} from "../../storybook.utils";

const meta: any = {
    title: 'Commons/CheckOptionsSelector',
    component: CheckOptionsSelector,
    tags: ['autodocs'],
} satisfies Meta<typeof CheckOptionsSelector>;

export default meta;
type Story = StoryObj<typeof meta>;

const stringOptions = ['Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5'];

export const Strings: Story = {
    args: {
        options: stringOptions,
    },
    argTypes:  {
        selecteds: {
            options: stringOptions,
            control: {type: 'multi-select'},
        },
    }
};

const objectOptions = [
    {
        value: 'option1',
        description: 'Option 1'
    },
    {
        value: 'option2',
        description: 'Option 2'
    },
    {
        value: 'option3',
        description: 'Option 3'
    },
    {
        value: 'option4',
        description: 'Option 4'
    },
    {
        value: 'option5',
        description: 'Option 5'
    },
];

export const Objects: Story = {
    args: {
        options: objectOptions,
        render: 'description',
        onChange: (selecteds: any[]) => console.log(selecteds),
    },
    argTypes:  {
        selecteds: {
            ...makeControlForObjectArray(objectOptions, 'description', 'multi-select'),
            defaultValue: [],
        },
        render: makeControlForRender(objectOptions[0], 'description'),
    }
};
