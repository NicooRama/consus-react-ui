import React from "react"
import {Checkbox} from "../checkbox/Checkbox";
import {normalizeDisplay} from "@consus/js-utils";
import {ConsusRender} from "../../utils/types";

import {mobileMediaQuery} from "../../styles/common";
import styled, { css } from "styled-components";
import {swiperScrollBar} from "../../styles/css.utils";
import {useSelectOptions} from "../../hooks/useSelectOptions";

const Component = styled.div<{ withScroll: boolean }>`
    display: flex;
    gap: ${props => props.theme.spacing.xs}px;
    flex-wrap: wrap;

    ${mobileMediaQuery} {
        ${({theme, withScroll}: any) =>
                withScroll &&
                css`
                    flex-wrap: nowrap;
                    ${swiperScrollBar(theme)}
                `}
    }

    .checkbox {
        color: black;
        display: inline-flex;
        border: ${props => props.theme.border};
        border-radius: ${props => props.theme.borderRadius}px;
        padding: 10.5px 16px 10.5px 10px;

        &:focus {
            border-color: ${props => props.theme.colors.primary.normal};
        }

        ${mobileMediaQuery} {
            width: 100%;
        }
    }
`

export interface CheckOptionsSelectorProps {
    options: any[];
    selecteds?: any[];
    render?: ConsusRender,
    comparator?: (option: any) => ((selected: any) => boolean);
    onChange: (selecteds: any[]) => void;
    children?: any;
    withScroll?: boolean;
    //without slider
}

export const CheckOptionsSelector: React.FC<CheckOptionsSelectorProps> = ({
                                                                              options,
                                                                              selecteds = [],
                                                                              comparator,
                                                                              render,
                                                                              onChange,
                                                                              children,
                                                                              withScroll = true,
                                                                              ...props
                                                                          }) => {
    const {handleChange, isSelected} = useSelectOptions(options, selecteds, onChange, comparator);

    render = normalizeDisplay(render);

    return (<Component withScroll={withScroll} {...props}>
        {
            options.map((option, i) => (
                <Checkbox key={i}
                          className={'checkbox'}
                          checked={isSelected(option)}
                          handleLabelClick={true}
                          onChange={() => handleChange(i)}
                          data-testid={`checkbox`}
                >
                    {(render as (item: any) => string)(option)}
                </Checkbox>
            ))
        }
        {
            !!children && children
        }
    </Component>)
};
