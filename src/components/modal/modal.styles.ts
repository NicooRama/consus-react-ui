import {createGlobalStyle} from "styled-components";
import {mobileMediaQuery} from "../../styles/common";

export const ModalGlobalStyles = createGlobalStyle`
  .ReactModal__Overlay {
    z-index: 20;
  }

  .ReactModal__Content {
    height: fit-content;
    max-height: 85vh;
    width: fit-content;
    margin: 0 auto;
    ${({ theme }: any) => `
      inset: ${theme.spacing.md}px !important;
    `}
  }

  ${mobileMediaQuery} {
    .ReactModal__Content {
      width: auto;
      margin: 0;
    }
  }
`;

