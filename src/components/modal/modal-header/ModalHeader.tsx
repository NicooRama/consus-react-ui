import React from 'react';
import styled from "styled-components";
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import {ButtonWrapper} from '../../button-wrapper/ButtonWrapper';
import {fontAwesomeToIcon} from "../../icon/icon.utils";
import {Icon} from '../../icon/Icon';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  gap: 12px;
  align-items: center;
`

const StyledButtonWrapper = styled(ButtonWrapper as any)`
  margin-left: auto;
`

export interface ModalHeaderProps {
    onClose: () => void;
    children: any;

    }

export const ModalHeader: React.FC<ModalHeaderProps> = ({
                                onClose,
                                children,
                                ...props
                            }) => {
    return (<Container {...props}>
        {children}
        <StyledButtonWrapper onClick={onClose}>
            <Icon icon={fontAwesomeToIcon(faTimes)} color={'gray.darkest'}/>
        </StyledButtonWrapper>
    </Container>)
};
