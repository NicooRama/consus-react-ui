import type {Meta, StoryObj} from '@storybook/react';
import {ModalHeader} from './ModalHeader';

const meta: any = {
    title: 'Modal/ModalHeader',
    component: ModalHeader,
    tags: ['autodocs'],
} satisfies Meta<typeof ModalHeader>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: 'Title',
        onClose: () => {}
    },
};
