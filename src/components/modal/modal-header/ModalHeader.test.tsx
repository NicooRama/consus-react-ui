import {ModalHeader} from "./ModalHeader";
import {render} from "../../../../test/test.utils";

describe('<ModalHeader />', () => {
    it('renders <ModalHeader /> successfully', () => {
        const {getByTestId} = render(<ModalHeader data-testid={'modal-header'} onClose={() => {}}>Header</ModalHeader>)
        expect(getByTestId('modal-header')).toBeInTheDocument();
    });
});
