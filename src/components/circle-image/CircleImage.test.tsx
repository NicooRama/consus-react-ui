import {render} from "../../../test/test.utils";
import {CircleImage} from "./CircleImage";

describe('<CircleImage />', () => {
    it('renders <CircleImage /> successfully', () => {
        const {getByTestId} = render(<CircleImage data-testid={'circle-image'}
                                                  src={'https://us.123rf.com/450wm/aquir/aquir1909/aquir190907813/129839336-example-button-example-rounded-green-sign-example.jpg?ver=6'}
                                                  alt={'example'}
        />)
        expect(getByTestId('circle-image')).toBeInTheDocument();
    });
});
