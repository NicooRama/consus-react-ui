import React from "react"
import {css} from "styled-components";
import styled from "styled-components";
import {Image, ImageProps} from "../image/Image";

const StyledImage = styled(Image)<{clickeable: boolean, size: number}>`
  border: 1px solid ${({theme}: any) => theme.colors.border};
  border-radius: ${({size}: any) => size * 2}px;
  width: ${({size}: any) => size + 2}px;
  height: ${({size}: any) => size + 2}px;
  min-width: ${({size}: any) => size + 2}px;
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    max-width: ${({size}: any) => size}px;
    max-height: ${({size}: any) => size}px;
    border-radius: ${({size}: any) => size}px;
  }
  
    ${({ clickeable }) =>
    clickeable &&
    css`
          &:hover {
            transition-duration: 0.1s;
            cursor: pointer;
            filter: opacity(50%);  
          }
        `}
`

export interface CircleImageProps extends ImageProps {
    size?: number;
    clickeable?: boolean;
}

export const CircleImage: React.FC<CircleImageProps> = ({size = 50, clickeable = false, ...props}) => {
    return <StyledImage clickeable={clickeable} size={size} {...props}  />
};
