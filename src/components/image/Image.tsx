import React, { useState } from 'react';
import {mobileMediaQuery} from "../../styles/common";
import styled, {css} from "styled-components";

const Container = styled.div`
  border: 1px solid ${({theme}: any) => theme.colors.border};
  border-radius: ${({theme}: any) => theme.borderRadius}px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: ${({theme}: any) => theme.spacing.xxs}px;
  width: 100%;
  height: 100%;
  background-color: white;

  ${mobileMediaQuery} {
    max-height: 200px;
  }
`

const StyledImage = styled.img<{ placeholder: any }>`
  max-width: 100%;
  max-height: 100%;
  ${({placeholder}: any) =>
      placeholder &&
      css`
        background: url(${placeholder}) no-repeat;
      `}
  background-position: center;
  background-size: contain;
  object-fit: contain;
  min-width: 80%;
  min-height: 80%;
`

export interface ImageProps {
    src: string | undefined;
    alt: string;
    placeholder?: any;
}

export const Image: React.FC<ImageProps> = ({src, alt, placeholder, ...props}) => {
    const [loading,setLoading] = useState(true);
    const [error,setError] = useState(false);

    const onLoadFinish = () => {
        setLoading(false);
    }

    const onError = () => {
        setError(true);
    }

    return (<Container {...props}>
        <StyledImage src={src}
                     alt={!error ? alt : ''}
                     placeholder={loading && placeholder}
                     onLoad={onLoadFinish}
                     onError={onError}
        />
    </Container>)
};
