import {Image} from "./Image";
import {render} from "../../../test/test.utils";

describe('<Image />', () => {
    it('renders <Image /> successfully', () => {
        const {getByTestId} = render(<Image
            src={'https://us.123rf.com/450wm/aquir/aquir1909/aquir190907813/129839336-example-button-example-rounded-green-sign-example.jpg?ver=6'}
            alt={'example'}
            data-testid={'image'}/>)
        expect(getByTestId('image')).toBeInTheDocument();
    });
});
