import type {Meta, StoryObj} from '@storybook/react';

import {Image} from './Image';

const meta: any = {
    title: 'Image/Image',
    component: Image,
    tags: ['autodocs'],
} satisfies Meta<typeof Image>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        src: 'https://us.123rf.com/450wm/aquir/aquir1909/aquir190907813/129839336-example-button-example-rounded-green-sign-example.jpg?ver=6',
        alt: 'example',
    },
};
