import { Switch } from './Switch'
import { render } from '../../../test/test.utils'

describe('<Switch />', () => {
  it('renders <Switch /> successfully', () => {
    const { getByTestId } = render(
      <Switch
        data-testid={'switch'}
        checked={false}
        checkedText={'Foo'}
        onChange={() => {}}
        unCheckedText={'Bar'}
      />,
    )
    expect(getByTestId('switch')).toBeInTheDocument()
  })
})
