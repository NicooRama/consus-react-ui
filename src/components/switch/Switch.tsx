import React from 'react'
import styled, {keyframes} from 'styled-components'
import RcSwitch from 'rc-switch'
import {toDimension} from '../skeletons/skeleton.utils'

const switchPrefixCls = 'rc-switch'
const duration = '0.3s'

const rcSwitchOn = keyframes`
  0% {
    transform: scale(1);
  }
  50% {
    transform: scale(1.25);
  }
  100% {
    transform: scale(1.1);
  }
`

const rcSwitchOff = keyframes`
  0% {
    transform: scale(1.1);
  }
  100% {
    transform: scale(1);
  }
`

const height = 42

const Container = styled.div<{ width: string | number }>`
  .${switchPrefixCls} {
    position: relative;
    display: inline-block;
    box-sizing: border-box;
    width: ${({width}: any) => toDimension(width)};
    height: ${height}px;
    line-height: 20px;
    vertical-align: middle;
    border-radius: 4px 4px;
    border: 2px solid ${({theme}: any) => theme.colors.primary.normal};
    background-color: transparent;
    cursor: pointer;
    transition: all ${duration} cubic-bezier(0.35, 0, 0.25, 1);
    overflow: hidden;
    padding: 1px ${({theme}: any) => theme.spacing.xs}px;

    &-inner-checked,
    &-inner-unchecked {
      color: ${({theme}: any) => theme.colors.primary.normal};
      font-size: ${({theme}: any) => theme.font.size.sm}px;
      font-weight: ${({theme}: any) => theme.font.weight.bold};
      position: absolute;
      top: ${Math.abs(height - 32)}px;
      transition: left ${duration} cubic-bezier(0.35, 0, 0.25, 1);
    }

    &-inner-checked {
      left: calc(6px - ${({width}: any) => width - 30}px);
    }

    &-inner-unchecked {
      left: 30px;
    }

    &:after {
      position: absolute;
      width: ${({theme}: any) => theme.font.size.sm}px;
      height: ${({theme}: any) => theme.font.size.sm}px;
      left: 8px;
      top: ${Math.abs(height - 32)}px;
      border-radius: 4px;
      background-color: ${({theme}: any) => theme.colors.primary.normal};
      content: ' ';
      cursor: pointer;
      box-shadow: 0 2px 5px rgba(0, 0, 0, 0.26);
      transform: scale(1);
      transition: left ${duration} cubic-bezier(0.35, 0, 0.25, 1);
      animation-timing-function: cubic-bezier(0.35, 0, 0.25, 1);
      animation-duration: ${duration};
      animation-name: ${rcSwitchOff};
    }

    &:hover:after {
      transform: scale(1.1);
      animation-name: ${rcSwitchOn};
    }

    &:focus {
      box-shadow: 0 0 0 2px black;
      outline: none;
    }

    &-checked {
      .${switchPrefixCls}-inner-checked {
        left: 6px;
      }

      .${switchPrefixCls}-inner-unchecked {
        left: ${({width}: any) => width}px;
      }

      &:after {
        left: ${({width}: any) => width - 28}px;
      }
    }

    &-disabled {
      cursor: no-drop;
      background: #ccc;
      border-color: #ccc;

      &:after {
        background: #9e9e9e;
        animation-name: none;
        cursor: no-drop;
      }

      &:hover:after {
        transform: scale(1);
        animation-name: none;
      }
    }

    &-label {
      display: inline-block;
      line-height: 20px;
      font-size: 14px;
      padding-left: 10px;
      vertical-align: middle;
      white-space: normal;
      pointer-events: none;
      user-select: text;
    }
  }
`

export interface SwitchProps {
    checked: boolean
    onChange: (value: boolean, event: any) => void
    checkedText: string
    unCheckedText: string
    width?: number;
    className?: string;
    [props: string]: any
}

export const Switch: React.FC<SwitchProps> = ({
                                                  checked,
                                                  onChange,
                                                  checkedText,
                                                  unCheckedText,
                                                  width = 120,
                                                  className = '',
                                              ...props
}) =>
{
    const dataTestId = props['data-testid'];
    delete props['data-testid'];
    return (
        <Container width={width} className={className} data-testid={dataTestId}>
            <RcSwitch
                checked={checked}
                onChange={onChange}
                checkedChildren={checkedText}
                unCheckedChildren={unCheckedText}
                {...props}
            />
        </Container>
    )
}
