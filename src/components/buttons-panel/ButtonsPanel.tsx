import {faEdit, faEye, faTrashAlt} from "@fortawesome/free-regular-svg-icons";
import {Link} from "react-router-dom";
import styled from "styled-components";
import {Container} from "./ButtonsPanel.layout";
import {IconButton} from "../icon-button/IconButton";
import {ConfirmationPopoverButton} from "../popover/ConfirmationPopoverButton";

const StyledIconButton = styled(IconButton)`
  &.circle {
    padding: 6px 10px;
  }
`

interface Props {
    editLink: string;
    detailsLink?: string;
    confirmationText: string;
    onRemoveConfirmation: () => void | Promise<void>;
    showDetails?: boolean;
    showRemove?: boolean;
    className?: string;
    [props: string]: any;
}

export const ButtonsPanel = ({
                                 editLink,
                                 detailsLink,
                                 confirmationText,
                                 onRemoveConfirmation,
                                 showDetails = false,
                                 showRemove = true,
                                 className = "",
    ...props
                             }: Props) => {
    return (<Container className={className} {...props}>
        {
            showDetails && detailsLink && <Link to={detailsLink}>
                <IconButton icon={faEye} title={'Detalle'} onClick={() => {
                }}
                            variant="secondary"
                            circle
                            data-testid={'details-button'}
                />
            </Link>
        }
        <Link to={editLink}>
            <StyledIconButton icon={faEdit}
                        className={'edit-button'}
                        color={'warn.dark'}
                        circle
                        variant={'secondary'}
                        onClick={() => {
                        }}
                              data-testid={'edit-button'}
            />
        </Link>
        {
            showRemove && <ConfirmationPopoverButton message={confirmationText}
                                                     onAccept={onRemoveConfirmation}
                                                     positions={['bottom', 'left', 'right', 'top']}
            >
                {
                    ({onClick}) => <IconButton
                        circle
                        color="error.normal"
                        icon={faTrashAlt}
                        onClick={onClick}
                        variant="secondary"
                        data-testid={'delete-button'}
                    />
                }
            </ConfirmationPopoverButton>
        }
    </Container>)
};
