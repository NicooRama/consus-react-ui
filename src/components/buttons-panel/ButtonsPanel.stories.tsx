import type {Meta, StoryObj} from '@storybook/react';
import {ButtonsPanel} from './ButtonsPanel';
import {routerDecorator} from "../../storybook.utils";

const meta: any = {
    title: 'Commons/ButtonsPanel',
    component: ButtonsPanel,
    tags: ['autodocs'],
} satisfies Meta<typeof ButtonsPanel>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        detailsLink: '/details',
        editLink: '/edit',
        confirmationText: '¿Está seguro que desea eliminar esto?',
        onRemoveConfirmation: async () => {},
    },
    decorators: [routerDecorator]
};
