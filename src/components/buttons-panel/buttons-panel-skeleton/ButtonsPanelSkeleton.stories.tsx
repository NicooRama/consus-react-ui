import type {Meta, StoryObj} from '@storybook/react';
import {ButtonsPanelSkeleton} from './ButtonsPanelSkeleton';

const meta: any = {
    title: 'Commons/ButtonsPanel/ButtonsPanelSkeleton',
    component: ButtonsPanelSkeleton,
    tags: ['autodocs'],
} satisfies Meta<typeof ButtonsPanelSkeleton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        showDetails: false,
        showEdit: true,
        showRemove: true,
    },
};
