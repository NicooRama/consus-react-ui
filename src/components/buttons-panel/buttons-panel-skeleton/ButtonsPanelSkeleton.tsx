import React from 'react';
import {Container} from "../ButtonsPanel.layout";
import {SkeletonCircleImage} from "../../skeletons";

export interface ButtonsPanelSkeletonProps {
    showDetails?: boolean;
    showEdit?: boolean;
    showRemove?: boolean;

    [props: string]: any;
}

export const ButtonsPanelSkeleton: React.FC<ButtonsPanelSkeletonProps> = ({
                                                                              showDetails,
                                                                              showEdit = true,
                                                                              showRemove = true,
                                                                              ...props
                                                                          }) => {
    return (<Container {...props}>
        {
            showDetails && <SkeletonCircleImage size={41}/>
        }
        {
            showEdit && <SkeletonCircleImage size={41}/>
        }
        {
            showRemove && <SkeletonCircleImage size={41}/>
        }
    </Container>)
};
;
