import {ButtonsPanelSkeleton} from "./ButtonsPanelSkeleton";
import {render} from "../../../../test/test.utils";

describe('<ButtonsPanelSkeleton />', () => {
    it('renders <ButtonsPanelSkeleton /> successfully', () => {
        const {getByTestId} = render(<ButtonsPanelSkeleton data-testid={'buttons-panel-skeleton'}/>)
        expect(getByTestId('buttons-panel-skeleton')).toBeInTheDocument();
    });
});
