import {ButtonsPanel} from "./ButtonsPanel";
import {renderWithRouter as render} from "../../../test/test.utils";

describe('<ButtonsPanel />', () => {
    it('renders <ButtonsPanel /> successfully', () => {
        const {getByTestId} = render(<ButtonsPanel
            confirmationText={'¿Estas seguro que queres eliminar el producto?'}
            onRemoveConfirmation={async () => {
            }}
            detailsLink={'/details'}
            editLink={'/edit'}
            data-testid={'buttons-panel'}
        />)
        expect(getByTestId('buttons-panel')).toBeInTheDocument();
    });

    it('should not render details button beacuse showDetails is false', () => {
        const {queryByTestId} = render(<ButtonsPanel
            confirmationText={'¿Estas seguro que queres eliminar el producto?'}
            onRemoveConfirmation={async () => {
            }}
            detailsLink={'/details'}
            editLink={'/edit'}
            data-testid={'buttons-panel'}
            showDetails={false}
        />)
        expect(queryByTestId('details-button')).not.toBeInTheDocument();
    })
    it('should not render details button because detailsLink is undefined', () => {
        const {queryByTestId} = render(<ButtonsPanel
            confirmationText={'¿Estas seguro que queres eliminar el producto?'}
            onRemoveConfirmation={async () => {
            }}
            editLink={'/edit'}
            data-testid={'buttons-panel'}
            showDetails={true}
        />)
        expect(queryByTestId('details-button')).not.toBeInTheDocument();
    });

    it('should render details button', () => {
        const {getByTestId} = render(<ButtonsPanel
            confirmationText={'¿Estas seguro que queres eliminar el producto?'}
            onRemoveConfirmation={async () => {
            }}
            detailsLink={'/details'}
            editLink={'/edit'}
            data-testid={'buttons-panel'}
            showDetails={true}
        />)
        expect(getByTestId('details-button')).toBeInTheDocument();
    });

    it('should not render remove button because showRemove is false', () => {
        const {queryByTestId} = render(<ButtonsPanel
            confirmationText={'¿Estas seguro que queres eliminar el producto?'}
            onRemoveConfirmation={async () => {
            }}
            detailsLink={'/details'}
            editLink={'/edit'}
            data-testid={'buttons-panel'}
            showRemove={false}
        />)
        expect(queryByTestId('delete-button')).not.toBeInTheDocument();
    });

    it('should render remove button', () => {
        const {getByTestId} = render(<ButtonsPanel
            confirmationText={'¿Estas seguro que queres eliminar el producto?'}
            onRemoveConfirmation={async () => {
            }}
            detailsLink={'/details'}
            editLink={'/edit'}
            data-testid={'buttons-panel'}
            showRemove={true}
        />)
        expect(getByTestId('delete-button')).toBeInTheDocument();
    });
});
