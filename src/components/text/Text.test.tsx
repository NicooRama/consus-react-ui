import {Text} from "./Text";
import {render} from "../../../test/test.utils";

describe('<Text />', () => {
    it('renders <Text /> successfully', () => {
        const {getByTestId} = render(<Text data-testid={'text'}>Text</Text>)
        expect(getByTestId('text')).toBeInTheDocument();
    });
});
