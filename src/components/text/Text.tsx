import React from 'react'
import { Size, Weight } from '../../utils/types'
import { ThemeColorIndex } from '../../styles/theme.interface'
import styled from 'styled-components'
import {parseColor, parseFontSize, parseFontWeight} from "../CssComponents/css.utils";
import {commonCss} from "../CssComponents/css";
import {CommonStyleProps} from "../CssComponents/css.interface";

type TextColor = 'fade' | 'solid'

type Tag = 'p' | 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'div' | 'span'

export interface TextProps extends CommonStyleProps{
  tag?: Tag
  size?: Size
  weight?: Weight
  color?: TextColor | ThemeColorIndex
  className?: string
  textAlign?: string;
  uppercase?: boolean;
  underline?: boolean;
  children: any;
  [props: string]: any;
}


const Component = styled.div<TextProps>`
  margin: 0;
  font-size: ${({theme, size}) => parseFontSize(theme, size as Size)};
  text-align: ${({textAlign}) => textAlign || 'left'};
  font-weight: ${({theme, weight}) => parseFontWeight(theme, weight as Weight)};
  color: ${({theme, color}) => parseColor(theme, color as ThemeColorIndex)};
  text-transform: ${({uppercase}) => uppercase ? 'uppercase' : 'none'};
  text-decoration: ${({underline}) => underline ? 'underline' : 'none'};

  ${commonCss((props: any) => props)};
`

export const Text: React.FC<TextProps> = ({
  tag = 'p',
  size = 'sm',
  weight = 'regular',
  color = 'solid',
  children,
  className,
  href,
  ...props
}) => {
  return (
    <Component as={tag} weight={weight} color={color} size={size} ref={href} className={className} {...props}>
      {children}
    </Component>
  )
}
