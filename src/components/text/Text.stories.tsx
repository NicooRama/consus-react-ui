import type {Meta, StoryObj} from '@storybook/react';
import {Text} from './Text';

const meta: any = {
    title: 'Commons/Text',
    component: Text,
    tags: ['autodocs'],
} satisfies Meta<typeof Text>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        tag: 'p',
        size: 'sm',
        weight: 'regular',
        color: 'solid',
        children: 'Lorem ipsum'
    },
};

export const Xs: Story = {
    args: {
        ...Primary.args,
        size: 'xs'
    },
};
export const Sm: Story = {
    args: {
        ...Primary.args,
        size: 'sm'
    },
};
export const Md: Story = {
    args: {
        ...Primary.args,
        size: 'md'
    },
};
export const Lg: Story = {
    args: {
        ...Primary.args,
        size: 'lg'
    },
};

export const Colored: Story = {
    args: {
        ...Primary.args,
        color: 'primary.normal',
        size: 'lg'
    },
};
