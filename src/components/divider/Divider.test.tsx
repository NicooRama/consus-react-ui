import {Divider} from "./Divider";
import {render} from "../../../test/test.utils";

describe('<Divider />', () => {
    it('renders <Divider /> successfully', () => {
        const {getByTestId} = render(<Divider data-testid={'divider'}/>)
        expect(getByTestId('divider')).toBeInTheDocument();
    });
});
