import type {Meta, StoryObj} from '@storybook/react';
import {Divider} from './Divider';

const decorators = [
    (Story: any) => (
<div style={{display: 'flex', height: 100}}>
    <Story />
</div>
)
]

const meta: any = {
    title: 'Commons/Divider',
    component: Divider,
    tags: ['autodocs'],
} satisfies Meta<typeof Divider>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
};

export const Vertical: Story = {
    args: {
        orientation: 'vertical'
    },
    decorators
}
