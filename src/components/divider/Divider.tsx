import React from 'react'

import { ThemeColorIndex } from '../../styles/theme.interface'
import styled, { css } from 'styled-components'
import { getIn } from '../../utils/utils'

export type DividerOrientation = 'horizontal' | 'vertical'

const Container = styled.div<{
  color: ThemeColorIndex
  orientation: DividerOrientation
}>`
  ${({ theme, orientation, color }) =>
    orientation === 'horizontal'
      ? css`
          border-bottom: 1px solid ${getIn(theme.colors, color)};
          width: 100%;
        `
      : css`
          border-left: 1px solid ${getIn(theme.colors, color)};
          height: auto;
        `}
`

export interface DividerProps {
  color?: ThemeColorIndex
  orientation?: DividerOrientation
  className?: string
}

export const Divider: React.FC<DividerProps> = ({
  className = '',
  color = 'border',
  orientation = 'horizontal',
  ...props
}) => {
  return (
    <Container
      color={color}
      orientation={orientation}
      className={className}
      {...props}
    ></Container>
  )
}

export const VerticalDivider = (props: DividerProps) => <Divider orientation={'vertical'} {...props} />
export const HorizontalDivider = (props: DividerProps) => <Divider orientation={'horizontal'} {...props} />
