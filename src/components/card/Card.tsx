import React, { ReactElement } from 'react'
import styled from "styled-components";

const Component = styled.div`
  box-shadow: 0 0 3px 0px ${({theme}) => theme.colors.boxShadow};
  position: relative;
  display: inline-block;
  border: ${({theme}) => theme.border};
  border-radius: ${({theme}) => theme.borderRadius}px;
  padding: ${({theme}) => theme.spacing.md}px;
  height: fit-content;
  background-color: white;
`

export interface CardProps {
    children: ReactElement | ReactElement[] | string | any
    className?: string
    [props: string]: any
}

export const Card: React.FC<CardProps> = ({ children, className = '', ...props }) => {
    return (
        <Component className={className} data-testid={'card'} {...props}>
            {children}
        </Component>
    )
}
