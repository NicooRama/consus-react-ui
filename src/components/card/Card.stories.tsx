import type { Meta, StoryObj } from '@storybook/react'
import { Card } from './Card'

const meta: any = {
  title: 'Card/Card',
  component: Card,
  tags: ['autodocs'],
} satisfies Meta<typeof Card>

export default meta
type Story = StoryObj<typeof meta>

export const Primary: Story = {
  args: {
    children: "I'm a text inside the card",
  },
}
