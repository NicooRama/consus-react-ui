import {render} from "../../../test/test.utils";
import {Card} from "./Card";

describe('<Card />', () => {
    it('renders <Card /> successfully', () => {
        const {getByTestId} = render(<Card data-testid={'card'}>Card</Card>)
        expect(getByTestId('card')).toBeInTheDocument();
    });
});
