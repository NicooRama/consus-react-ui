import { Link } from "react-router-dom";
import styled, {css} from "styled-components";
import {mobileMediaQuery} from "../styles/common";
import {List} from "./list/List";

export const ApiPageContainer = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-template-columns: 1fr 1fr;
  grid-template-areas: "list details" "new new";
  grid-gap: ${({theme}: any) => theme.spacing.md}px;

  ${mobileMediaQuery} {
    grid-template-columns: 1fr;
    grid-template-areas: "list" "details" "new";
  }
`

export const entityCardsListStyles = css`
  overflow: auto;
  max-width: 100%;
  grid-area: list;
`

export const EntityCardsList = styled(List)`
  ${entityCardsListStyles}
`

export const entityDetailStyles = css`
  grid-area: details;
  height: fit-content;
`

export const skeletonEntityDetailStyles = css`
  grid-area: details;
`

export const EntityDetails = styled.div`
  ${entityDetailStyles}
`;

export const NewLink = styled(Link)`
  text-decoration: none;
  grid-area: new;
  width: fit-content;
`
