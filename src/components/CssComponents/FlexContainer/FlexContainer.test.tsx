import {FlexContainer} from "./FlexContainer";
import {render} from "../../../../test/test.utils";

describe('<FlexContainer />', () => {
    it('renders <FlexContainer /> successfully', () => {
        const {getByTestId} = render(<FlexContainer data-testid={'flex-container'}/>)
        expect(getByTestId('flex-container')).toBeInTheDocument();
    });
});
