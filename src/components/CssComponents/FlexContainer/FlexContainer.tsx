import React from 'react';
import styled, {css} from 'styled-components';
import {commonCss, containerCss, growConditional, shrinkConditional} from "../css";
import {CommonStyleProps, ContainerStyleProps} from "../css.interface";
import {mobileStyles} from "../mobile.styles";

const desktopDefaults = {
    direction: 'row',
    gap: 'sm',
    spanGridColumn: false,
    wrap: 'nowrap'
}

type FlexDirection = 'row' | 'column' | string;
type FlexWrap = 'nowrap' | 'wrap' | 'wrap-reverse' | 'inherit' | string;

interface StyleProps extends CommonStyleProps, ContainerStyleProps {
    direction?: FlexDirection;
    wrap?: FlexWrap;
    grow?: number,
}

export interface FlexContainerProps extends StyleProps {
    mobile?: StyleProps
}

const customCss = ({
                       defaults = {},
                       ...props
                   }: StyleProps & { defaults?: any }) => {
    const normalizedProps = {
        ...defaults,
        ...props
    }

    return css`
      ${commonCss(normalizedProps)};
      ${containerCss(normalizedProps)};
      flex-direction: ${props.direction};
      flex-wrap: ${props.wrap};
      ${growConditional(normalizedProps)};
      ${shrinkConditional(normalizedProps)};
    `
}

export const FlexContainer = styled.div<FlexContainerProps>`
  display: flex;
  ${(props: StyleProps) => customCss({...props, defaults: desktopDefaults})};

  ${mobileStyles(customCss, desktopDefaults)}
`
