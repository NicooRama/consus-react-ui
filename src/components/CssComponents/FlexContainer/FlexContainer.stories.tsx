import type {Meta, StoryObj} from '@storybook/react';
import {FlexContainer} from './FlexContainer';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Containers/FlexContainer',
    component: FlexContainer,
    tags: ['autodocs'],
} satisfies Meta<typeof FlexContainer>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: <>
            <div>Item 1</div>
            <div>Item 2</div>
            <div>Item 3</div>
        </>
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
