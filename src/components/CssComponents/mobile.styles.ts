import {css} from "styled-components";
import {mobileMediaQuery} from "../../styles/common";

export const mobileStyles = (cssGenerator: (params: any) => any, defaults = {}) => (props: any) => css`
  ${mobileMediaQuery} {
    ${cssGenerator({...props, ...props.mobile, defaults})}
  }
`
