import {NormalContainer} from "./NormalContainer";
import {render} from "../../../../test/test.utils";

describe('<NormalContainer />', () => {
    it('renders <NormalContainer /> successfully', () => {
        const {getByTestId} = render(<NormalContainer data-testid={'normal-container'}/>)
        expect(getByTestId('normal-container')).toBeInTheDocument();
    });
});
