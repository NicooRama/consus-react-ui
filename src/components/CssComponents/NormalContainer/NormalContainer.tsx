import React from 'react';
import {CommonStyleProps} from "../css.interface";
import styled, {css} from 'styled-components';
import {centerConditional, commonCss} from "../css";
import {mobileStyles} from "../mobile.styles";

interface StyleProps extends CommonStyleProps {
    width?: string;
    center?: boolean;
}

interface NormalContainerProps extends StyleProps {
    mobile?: StyleProps
}

const customCss = ({
                       defaults = {},
                       ...props
                   }: StyleProps & { defaults?: any }) => {
    const normalizedProps = {
        ...defaults,
        ...props
    }

    return css`
      width: ${normalizedProps.width};
      ${commonCss(normalizedProps)};
      ${centerConditional(normalizedProps)};
    `
}

const desktopDefaults = {
    center: false,
    width: 'fit-content'
}

export const NormalContainer = styled.div<NormalContainerProps>`
  ${(props: StyleProps) => customCss({...props, defaults: desktopDefaults})};
  ${mobileStyles(customCss, desktopDefaults)}
    
    `
