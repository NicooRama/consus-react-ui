import type {Meta, StoryObj} from '@storybook/react';
import {NormalContainer} from './NormalContainer';

const meta: any = {
    title: 'Containers/NormalContainer',
    component: NormalContainer,
    tags: ['autodocs'],
} satisfies Meta<typeof NormalContainer>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: 'Content'
    },
};
