import {Spacing as SpacingType} from "../../../utils/types";

export const parseMeasure = (theme: any, measure: SpacingType | string | number) => {
    if (measure !== 0 && !measure) {
        return 0;
    }

    if (!isNaN(Number(measure))) {
        return `${measure}px`;
    }

    // @ts-ignore
    if (measure.includes("%")) {
        return measure;
    }

    // @ts-ignore
    if (!measure.includes(" ")) {
        return `${theme.spacing[measure]}px`;
    }

    // @ts-ignore
    return measure.split(" ").map((m: string) => `${theme.spacing[m]}px`).join(" ");
}
