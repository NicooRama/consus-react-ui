import React from 'react';
import styled, {css} from 'styled-components';
import {makeGridLines} from "../css.utils";
import {commonCss, containerCss} from "../css";
import {CommonStyleProps, ContainerStyleProps} from "../css.interface";
import {mobileStyles} from "../mobile.styles";
import {parseMeasure} from "../utils/parseMeasure";


const desktopDefaults = {
    gap: 'sm',
    columns: 1,
    rows: 'auto'
}

interface StyleProps extends CommonStyleProps, ContainerStyleProps {
    columns?: number | string,
    rows?: number | string,
    rowGap?: number | string,
    columnGap?: number | string,
}

interface GridContainerProps extends StyleProps {
    mobile?: StyleProps
}

const customCss = ({
                       defaults = {},
                       ...props
                   }: StyleProps & { defaults: any }) => {
    const normalizedProps = {
        ...defaults,
        ...props
    }

    return css`
      ${commonCss(normalizedProps)};
      ${containerCss(normalizedProps)}
      grid-template-columns: ${makeGridLines(normalizedProps.columns)};
      grid-template-rows: ${makeGridLines(normalizedProps.rows)};
      ${({rowGap}: any) =>
              rowGap &&
              css`
                row-gap: ${parseMeasure(normalizedProps.theme, normalizedProps.rowGap)};
              `}
      ${({columnGap}: any) =>
              columnGap &&
              css`
                row-gap: ${parseMeasure(normalizedProps.theme, normalizedProps.columnGap)};
              `}
    `
}

export const GridContainer = styled.div<GridContainerProps>`
  display: grid;

  ${(props: StyleProps) => customCss({...props, defaults: desktopDefaults})};

  ${mobileStyles(customCss, desktopDefaults)}
`
