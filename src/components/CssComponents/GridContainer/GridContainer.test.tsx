import {GridContainer} from "./GridContainer";
import {render} from "../../../../test/test.utils";

describe('<GridContainer />', () => {
    it('renders <GridContainer /> successfully', () => {
        const {getByTestId} = render(<GridContainer data-testid={'grid-container'}/>)
        expect(getByTestId('grid-container')).toBeInTheDocument();
    });
});
