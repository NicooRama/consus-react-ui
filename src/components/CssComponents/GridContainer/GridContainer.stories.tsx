import type {Meta, StoryObj} from '@storybook/react';
import {GridContainer} from './GridContainer';

const meta: any = {
    title: 'Containers/GridContainer',
    component: GridContainer,
    tags: ['autodocs'],
} satisfies Meta<typeof GridContainer>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: (
            <>
                <div>Item 1</div>
                <div>Item 2</div>
                <div>Item 3</div>
            </>
        )
    },
};
