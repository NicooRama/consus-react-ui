import {css} from "styled-components";
import {parseMeasure} from "./utils/parseMeasure";

export const rightConditional = ({right}: any) =>
    right &&
    css`
      margin-left: auto;
    `

export const centerConditional = ({center}: any) =>
    center &&
    css`
      margin: 0 auto;
    `
;

export const alignConditional = ({align}: any) =>
    align &&
    css`
      align-items: ${align};
    `

export const shrinkConditional = ({shrink}: any) =>
    shrink &&
    css`
      flex-shrink: ${shrink};
    `

export const growConditional = ({grow}: any) =>
    grow &&
    css`
      flex-grow: ${grow};
    }
    `

export const spanGridColumnConditional = ({spanGridColumn}: any) => {
    return spanGridColumn &&
        css`
          grid-column: 1 / -1;
        `;
}


export const paddingCss = ({theme, padding}: any) => {
    return css`
      padding: ${parseMeasure(theme, padding)};
    `
}

export const gapCss = ({theme, gap}: any) => {
    return css`
      gap: ${parseMeasure(theme, gap)};
    `
}

export const marginCss = ({theme, margin}: any) => {
    return css`
      margin: ${parseMeasure(theme, margin)};
    `
}

export const containerCss = (defaults = {}) => (props: any) => {
    const normalizedProps = {
        ...defaults,
        ...props,
    }

    return css`
      ${gapCss(normalizedProps)};
      ${alignConditional(normalizedProps)};
    `
}

export const commonCss = (defaults = {}) => (props: any) => {
    const normalizedProps = {
        ...defaults,
        ...props,
    }

    return css`
      ${paddingCss(normalizedProps)}
      ${marginCss(normalizedProps)}

      ${rightConditional(normalizedProps)}
      ${centerConditional(normalizedProps)}
      ${spanGridColumnConditional(normalizedProps)}
    `
}
