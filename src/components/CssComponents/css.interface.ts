import {Spacing} from "../../utils/types";

export interface CommonStyleProps {
    padding?: Spacing | string,
    margin?: Spacing | string,
    spanGridColumn?: boolean,
    theme?: any,
    right?: boolean,
}

export interface ContainerStyleProps {
    align?: string,
    gap?: Spacing,
}
