import {ThemeColorIndex} from "../../styles/theme.interface";
import {getIn} from "../../utils/utils";

export const parseColor = (theme: any, color: ThemeColorIndex | string) => {
    const parsedColor = getIn(theme.colors, color === 'fade' || color === 'solid' ? `typographic.${color}` : color)
    return parsedColor || color;
}

export const parseFontSize = (theme: any, size: string | number) => {
    const fontSize = theme.font.size[size];
    return fontSize ? `${fontSize}px;` : size;
}

export const parseFontWeight = (theme: any, weight: string | number) => {
    const fontWeight = theme.font.weight[weight];
    return fontWeight || weight
}

export const makeGridLines = (columns: number | string) => {
    if (!columns) {
        return "";
    }

    if (typeof columns === "number") {
        return `repeat(${columns}, 1fr)`;
    }

    return columns;
}
