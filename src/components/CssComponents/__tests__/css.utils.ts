import {makeGridLines} from "../css.utils";
import {parseMeasure} from "../utils/parseMeasure";

describe('css.utils', () => {
    describe('parseMeasure', () => {
        it('parse empty measure successfully', () => {
            expect(parseMeasure({}, undefined as any)).toEqual(0);
        });

        it('parse number measure successfully', () => {
            expect(parseMeasure({}, 20)).toEqual('20px');
        });

        it('parse percentage measure successfully', () => {
            expect(parseMeasure({}, '20%')).toEqual('20%');
        });

        it('parse spacing measure successfully', () => {
            expect(parseMeasure({spacing: {small: 20}}, 'small')).toEqual('20px');
        });

        it('parse spacing measure with multiple values successfully', () => {
            expect(parseMeasure({spacing: {small: 20, medium: 30}}, 'small medium')).toEqual('20px 30px');
        });
    });

    describe('makeGridLines', () => {
        it('make grid lines with number successfully', () => {
            expect(makeGridLines(2)).toEqual('repeat(2, 1fr)');
        });

        it('make grid lines with string successfully', () => {
            expect(makeGridLines('auto')).toEqual('auto');
        });
    });
});
