import React from "react"
import PropTypes from 'prop-types';
import styled from "styled-components";

const Component = styled.div`
    max-width: 1128px;
    padding-left: ${props => props.theme.spacing.md}px;
    padding-right: ${props => props.theme.spacing.md}px;
    margin: ${props => props.theme.spacing.md}px auto;
`

export interface ContainerProps {
    children: any,
    className?: string,
    }

export const Container: React.FC<ContainerProps> = ({
                              children,
                              className = "",
                              ...props
                          }) => {
    return (<Component className={className} {...props}>
        {children}
    </Component>)
};

Container.propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
};
