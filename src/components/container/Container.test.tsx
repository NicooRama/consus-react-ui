import {Container} from "./Container";
import {render} from "../../../test/test.utils";

describe('<Container />', () => {
    it('renders <Container /> successfully', () => {
        const {getByTestId} = render(<Container data-testid={'container'}>Content</Container>)
        expect(getByTestId('container')).toBeInTheDocument();
    });
});
