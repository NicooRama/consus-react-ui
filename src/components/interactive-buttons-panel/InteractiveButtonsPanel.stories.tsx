import type {Meta, StoryObj} from '@storybook/react';
import {InteractiveButtonsPanel} from './InteractiveButtonsPanel';

const meta: any = {
    title: 'Commons/InteractiveButtonsPanel',
    component: InteractiveButtonsPanel,
    tags: ['autodocs'],
} satisfies Meta<typeof InteractiveButtonsPanel>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        showRemove: true,
        warningMessage: 'Warning message',
        onAcceptRemove: () => console.log('clicked'),
        onEditClick: () => console.log('clicked'),
    },
    decorators: [
        (Story: any) => <div style={{width: 50, height: 50, position: 'relative'}}>
            <Story />
        </div>
    ]
};
