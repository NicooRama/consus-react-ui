import React from 'react';
import styled, {css} from 'styled-components';
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import {InteractiveButton} from "../interactive-button/InteractiveButton";
import {InteractiveRemovePopover} from "../interactive-remove-popover/InteractiveRemovePopover";

const Container = styled.div`
  opacity: 0;
  position: absolute;
  right: -10px;
  top: -10px;
  z-index: 1;
  display: flex;
  align-items: flex-start;
  gap: ${({theme}: any) => theme.spacing.xxs}px;
`

export const interactiveButtonsPanelParentStyles = css`
  position: relative;
  &:hover {
    .interactive-buttons-panel {
      transition-duration: 0.5s;
      opacity: 1;
    }
  }
`

export interface InteractiveButtonsPanelProps {
    showRemove: boolean;
    warningMessage: string;
    onAcceptRemove: () => any;
    onEditClick: () => any;
    "data-testid"?: string;
    [props: string]: any;
}

export const InteractiveButtonsPanel: React.FC<InteractiveButtonsPanelProps> = ({
    showRemove,
    warningMessage,
    onAcceptRemove,
    onEditClick,
    className = '',
                                                                                    ...props}) => {
    return (<Container className={`interactive-buttons-panel ${className}`} {...props}>
        <InteractiveButton icon={faEdit} onClick={onEditClick} className={'edit-interactive-button'} onCorner={false}/>
        {!!showRemove && <InteractiveRemovePopover message={warningMessage}
                                                 onAccept={onAcceptRemove}
                                                 className={'interactive-remove-popover'}
                                                   onCorner={false}
        /> }
    </Container>)
};
;
