import {InteractiveButtonsPanel} from "./InteractiveButtonsPanel";
import {render} from "../../../test/test.utils";

describe('<InteractiveButtonsPanel />', () => {
    it('renders <InteractiveButtonsPanel /> successfully', () => {
        const {getByTestId} = render(<InteractiveButtonsPanel
            showRemove={true}
            warningMessage={'message'}
            onAcceptRemove={() => {}}
            onEditClick={() => {}}
            data-testid={'interactive-buttons-panel'}/>)
        expect(getByTestId('interactive-buttons-panel')).toBeInTheDocument();
    });
});
