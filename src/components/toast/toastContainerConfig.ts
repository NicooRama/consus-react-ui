import 'react-toastify/dist/ReactToastify.css';
import {Zoom} from "react-toastify";

export const toastContainerConfig = {
    toastClassName: "Toast-class",
    bodyClassName: "Toast-body-class",
    transition: Zoom,
    position: "top-right",
    autoClose: 4000,
    hideProgressBar: true,
    newestOnTop: true,
    closeOnClick: true,
    rtl: false,
    pauseOnVisibilityChange: true,
    draggable: true,
    pauseOnHover: true
};
