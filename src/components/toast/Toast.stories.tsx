import type {StoryObj} from '@storybook/react'
import {Button} from '../button/Button'
import styled from 'styled-components'
import {toastContainerConfig} from "./toastContainerConfig";
import {ToastContainer, toast} from "react-toastify";

const meta: any = {
    title: 'Commons/Toast',
    tags: ['autodocs'],
}

export default meta
type Story = StoryObj<typeof meta>

const Container = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(100px, 1fr));
    grid-gap: ${({theme}) => theme.spacing.xxs}px;
`

export const Primary: Story = {
    render: () => (
        <Container>
            <Button onClick={() => toast.success('Success')}>Success</Button>
            <Button onClick={() => toast.info('Info')}>Info</Button>
            <Button onClick={() => toast.warning('Warning')}>Warning</Button>
            <Button onClick={() => toast.error('Error')}>Error</Button>
            <ToastContainer {...(toastContainerConfig as any)} />
        </Container>
    ),
}
