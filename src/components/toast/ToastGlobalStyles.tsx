import {createGlobalStyle, css} from "styled-components";

const commonColors = css`
    color: white;
    svg {
        path {
            fill: white;
        }
    }
`;
export const ToastGlobalStyles = createGlobalStyle`
  .Toast-class {
    ${({ theme }: any) => `
      border-radius: ${theme.borderRadius};
      box-shadow: 0 0 12px #000;
    `}

    &.Toastify__toast--error {
      background-color: ${({ theme }: any) => theme.colors.error.normal};
      ${commonColors};
    }

    &.Toastify__toast--success {
      background-color: ${({ theme }: any) => theme.colors.success.dark};
      ${commonColors};
    }

    &.Toastify__toast--info {
      background-color: ${({ theme }: any) => theme.colors.info.dark};
      ${commonColors};
    }

    &.Toastify__toast--warning {
      background-color: ${({ theme }: any) => theme.colors.warn.dark};
      ${commonColors};
    }

    button {
      opacity: 1;
    }
  }

  .Toast-body-class {
    align-items: center;
    font-size: 15px;
    line-height: normal;
    letter-spacing: normal;
  }
`;
