import styled from 'styled-components'
import { NotFound } from '../not-found/NotFound'
import errorImg from './error.jpg'

export const StyledNotFound = styled(NotFound)`
  img {
    filter: grayscale(100%);
    margin-bottom: ${({ theme }: any) => theme.spacing.lg}px;
  }

  button {
    margin-top: ${({ theme }: any) => theme.spacing.lg}px;
  }
`

export interface ErrorPageProps {
  onClick: () => void
  message?: string
  buttonText?: string
}

export const ErrorPage = ({
  onClick,
  message = 'Ha ocurrido un error inesperado',
  buttonText = 'Volver a la página principal',
  ...props
}: ErrorPageProps) => {
  return (
    <StyledNotFound
      message={message}
      illustration={errorImg}
      buttonText={buttonText}
      onClick={onClick}
      {...props}
    />
  )
}
