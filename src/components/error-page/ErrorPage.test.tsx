import {ErrorPage} from "./ErrorPage";
import {render} from "../../../test/test.utils";

describe('<ErrorPage />', () => {
    it('renders <ErrorPage /> successfully', () => {
        const {getByTestId} = render(<ErrorPage data-testid={'error-page'} onClick={() => {}}/>)
        expect(getByTestId('error-page')).toBeInTheDocument();
    });
});
