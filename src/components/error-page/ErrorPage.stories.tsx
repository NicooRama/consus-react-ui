import type {Meta, StoryObj} from '@storybook/react';
import {ErrorPage} from './ErrorPage';

const meta: any = {
    title: 'Layout/ErrorPage',
    component: ErrorPage,
    tags: ['autodocs'],
} satisfies Meta<typeof ErrorPage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
};
