import type {Meta, StoryObj} from '@storybook/react';
import {Collapse} from './Collapse';

const meta: any = {
    title: 'Commons/Collapse',
    component: Collapse,
    tags: ['autodocs'],
} satisfies Meta<typeof Collapse>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        title: 'Click me!',
        children: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sit amet tortor sed ante ultricies ornare ut non est. Vestibulum ullamcorper iaculis vulputate. Phasellus bibendum massa ante, sed varius tellus malesuada sed. Proin consequat euismod urna, ut consequat turpis tristique non. Phasellus sit amet neque egestas, viverra metus placerat, eleifend massa. Suspendisse potenti. Ut laoreet nibh ac lacus porta feugiat. Ut sed varius tortor. Sed fringilla justo leo, vel malesuada nibh viverra vel. Mauris in pharetra ante. Curabitur venenatis pulvinar dui, nec fringilla felis pharetra id. Morbi non lectus id justo sollicitudin efficitur pulvinar eget massa. Fusce placerat, arcu id tempor mattis, urna metus elementum arcu, eu consectetur justo ligula in lacus. Nunc at efficitur nulla. Praesent orci risus, suscipit nec molestie vitae, ultrices quis nulla. Cras libero arcu, iaculis in libero malesuada, scelerisque viverra tellus.'
    },
};
