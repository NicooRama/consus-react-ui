import React from "react"
import {ReactElement} from "react";
import {Collapse as ReactCollapse} from 'react-collapse';
import {useState} from 'react';
import {StrUtils} from "@consus/js-utils";
import {Text, TextProps} from "../text/Text";
import styled from "styled-components";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAngleUp, faAngleDown} from '@fortawesome/free-solid-svg-icons';

const TitleContainer = styled.div`
  display: flex;
  flex-direction: row;
  gap: ${({theme}: any) => theme.spacing.xs}px;
  align-items: center;
  cursor: pointer;
`

const ArrowIcon = styled(FontAwesomeIcon)`
  margin-left: auto;
`

export interface CollapseProps {
    title: string | (() => ReactElement);
    titleProps?: Partial<TextProps>;
    initialOpen?: boolean;
    children: any;
    showArrow?: boolean;
    className?: string;
    }

export const Collapse: React.FC<CollapseProps> = ({
                             title: Title,
                             titleProps = {} as any,
                             children,
                             initialOpen = true,
                             showArrow = true,
                             className = "",
                             ...props
                         }) => {
    const [isOpened, setOpened] = useState(initialOpen);

    const renderTitle =
        <TitleContainer >
            {
                StrUtils.isString(Title) ? <Text size={'md'} {...titleProps}>
                    {Title as any}
                </Text> : <Title/>
            }
            {
                showArrow && <ArrowIcon icon={isOpened ? faAngleUp : faAngleDown} size={"1x"}/>
            }
        </TitleContainer>


    return (<div className={className} {...props}>
        {
            <div className={`collapse-title ${isOpened ? 'opened' : 'closed'}`} role={'button'}
                 onClick={() => setOpened(!isOpened)}>
                {renderTitle}
            </div>
        }
        <ReactCollapse isOpened={isOpened}>
            {children}
        </ReactCollapse>
    </div>)
};
