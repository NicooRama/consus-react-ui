import {Collapse} from "./Collapse";
import {render} from "../../../test/test.utils";

describe('<Collapse />', () => {
    it('renders <Collapse /> successfully', () => {
        const {getByTestId} = render(<Collapse data-testid={'collapse'} title={'Title'}>Content</Collapse>)
        expect(getByTestId('collapse')).toBeInTheDocument();
    });
});
