import type {Meta, StoryObj} from '@storybook/react';
import {LoaderWrapper} from './LoaderWrapper';

const meta: any = {
    title: 'Loader/LoaderWrapper',
    component: LoaderWrapper,
    tags: ['autodocs'],
} satisfies Meta<typeof LoaderWrapper>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: "I'm a loading content",
        loading: false,
    },
};
