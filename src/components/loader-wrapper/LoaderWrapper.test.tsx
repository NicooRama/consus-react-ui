import {LoaderWrapper} from "./LoaderWrapper";
import {render} from "../../../test/test.utils";

describe('<LoaderWrapper />', () => {
    it('renders <LoaderWrapper /> successfully', () => {
        const {getByTestId} = render(<LoaderWrapper data-testid={'loader-wrapper'} loading={true}>Content</LoaderWrapper>)
        expect(getByTestId('loader-wrapper')).toBeInTheDocument();
    });
});
