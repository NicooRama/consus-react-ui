import React from "react"
import { ReactElement } from 'react';
import {Loader} from "../loader/Loader";
import styled from "styled-components";

const Container = styled.div`
    &.center {
        margin: 0 auto;
    }
`

export interface LoaderWrapperProps {
    loading: boolean,
    size?: number,
    center?: boolean,
    centerLoader?: boolean,
    centerWrapper?: boolean,
    className?: string,
    children: ReactElement | string;
    }

export const LoaderWrapper: React.FC<LoaderWrapperProps> = ({
                                  loading,
                                  children,
                                  size = 36,
                                  center=false,
                                  centerLoader=false,
                                  centerWrapper=false,
                                  className = "",
                                  ...props}) => {
    if(center){
        centerLoader = true;
        centerWrapper = true;
    }

    return (<Container className={`${centerWrapper ? 'center' : ''} ${className}`} {...props}>
        {
            loading ? <Loader size={size} center={centerLoader}/> : children
        }
    </Container>)
};
