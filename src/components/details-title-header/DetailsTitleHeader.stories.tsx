import type {Meta, StoryObj} from '@storybook/react';
import {DetailsTitleHeader} from './DetailsTitleHeader';

const meta: any = {
    title: 'Details/DetailsTitleHeader/DetailsTitleHeader',
    component: DetailsTitleHeader,
    tags: ['autodocs'],
} satisfies Meta<typeof DetailsTitleHeader>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        title: 'Foo',
        subTitle: 'Bar'
    },
};
