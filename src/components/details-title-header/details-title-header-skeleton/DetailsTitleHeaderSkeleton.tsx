import React from 'react';
import {SkeletonText} from "../../skeletons";
import {Container} from "../DetailsTitleHeader.layout";

export interface DetailsTitleHeaderSkeletonProps {
    titleWidth?: string | number;
    subTitleWidth?: string | number;
    "data-testid"?: string;
    [props: string]: any;
}

export const DetailsTitleHeaderSkeleton: React.FC<DetailsTitleHeaderSkeletonProps> = ({titleWidth = 150, subTitleWidth = 50,...props}) => {
    return (<Container {...props}>
        <SkeletonText width={titleWidth} />
        <SkeletonText width={subTitleWidth} />
    </Container>)
};
