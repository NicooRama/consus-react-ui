import {DetailsTitleHeaderSkeleton} from "./DetailsTitleHeaderSkeleton";
import {render} from "../../../../test/test.utils";

describe('<DetailsTitleHeaderSkeleton />', () => {
    it('renders <DetailsTitleHeaderSkeleton /> successfully', () => {
        const {getByTestId} = render(<DetailsTitleHeaderSkeleton data-testid={'details-title-header-skeleton'}/>)
        expect(getByTestId('details-title-header-skeleton')).toBeInTheDocument();
    });
});
