import type {Meta, StoryObj} from '@storybook/react';
import {DetailsTitleHeaderSkeleton} from './DetailsTitleHeaderSkeleton';

const meta: any = {
    title: 'Details/DetailsTitleHeader/DetailsTitleHeaderSkeleton',
    component: DetailsTitleHeaderSkeleton,
    tags: ['autodocs'],
} satisfies Meta<typeof DetailsTitleHeaderSkeleton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        titleWidth: 100,
        subTitleWidth: 50,
    },
};
