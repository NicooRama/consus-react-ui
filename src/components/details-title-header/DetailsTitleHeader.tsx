import React from 'react';
import {Text} from "../text/Text";
import {Container} from "./DetailsTitleHeader.layout";

export interface DetailsTitleHeaderProps {
    title: string;
    subTitle?: string;
    "data-testid"?: string;

    [props: string]: any;
}

export const DetailsTitleHeader: React.FC<DetailsTitleHeaderProps> = ({title, subTitle, ...props}) => {
    return (<Container {...props}>
        <Text size={'md'} weight={'semiBold'}>{title}</Text>
        {
            subTitle && <Text size={'sm'} color={'fade'} data-testid={'details-title-subtitle'}>({subTitle})</Text>
        }
    </Container>)
};
;
