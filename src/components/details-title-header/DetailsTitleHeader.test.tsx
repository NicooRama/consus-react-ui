import {DetailsTitleHeader} from "./DetailsTitleHeader";
import {render} from "../../../test/test.utils";

describe('<DetailsTitleHeader />', () => {
    it('renders <DetailsTitleHeader /> successfully', () => {
        const {getByTestId} = render(<DetailsTitleHeader title={'Foo'} data-testid={'details-title-header'}/>)
        expect(getByTestId('details-title-header')).toBeInTheDocument();
    });

    it('renders <DetailsTitleHeader /> with title', () => {
        const {getByText} = render(<DetailsTitleHeader title={'title'} data-testid={'details-title-header'}/>)
        expect(getByText('title')).toBeInTheDocument();
    });

    it('renders <DetailsTitleHeader /> with title and subtitle', () => {
        const {getByText} = render(<DetailsTitleHeader title={'title'} subTitle={'subTitle'}
                                                       data-testid={'details-title-header'}/>)
        expect(getByText('title')).toBeInTheDocument();
        expect(getByText('(subTitle)')).toBeInTheDocument();
    });

    it('renders <DetailsTitleHeader /> with title and  without subtitle', () => {
        const {getByText, queryByTestId} = render(<DetailsTitleHeader title={'title'} data-testid={'details-title-header'}/>)
        expect(getByText('title')).toBeInTheDocument();
        expect(queryByTestId('details-title-subtitle')).not.toBeInTheDocument();
    });
});
