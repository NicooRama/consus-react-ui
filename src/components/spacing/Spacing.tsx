import styled from "styled-components";
import {Spacing as SpacingType} from "../../utils/types";

export interface SpacingProps {
    spacing?: SpacingType,
}

export const Spacing = styled.div<SpacingProps>`
  margin-bottom: ${({theme, spacing = 'sm'}) => theme.spacing[spacing]}px;
`
