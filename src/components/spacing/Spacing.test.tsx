import {Spacing} from "./Spacing";
import {render} from "../../../test/test.utils";

describe('<Spacing />', () => {
    it('renders <Spacing /> successfully', () => {
        const {getByTestId} = render(<Spacing data-testid={'spacing'}/>)
        expect(getByTestId('spacing')).toBeInTheDocument();
    });
});
