import type {Meta, StoryObj} from '@storybook/react';
import {Spacing} from './Spacing';

const meta: any = {
    title: 'Layout/Spacing',
    component: Spacing,
    tags: ['autodocs'],
    decorators: [
        (Story: any) => (
            <div style={{border: '1px solid transparent', backgroundColor: 'orange'}}>
                <Story/>
            </div>
        ),
    ]
} satisfies Meta<typeof Spacing>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
};
