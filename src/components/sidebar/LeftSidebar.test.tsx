import { LeftSidebar } from './LeftSidebar'
import { renderWithRouter } from '../../../test/test.utils'
import {icons} from "../icon/icons";

describe('<LeftSidebar />', () => {
  it('renders <LeftSidebar /> successfully', () => {
    const { getByTestId } = renderWithRouter(
      <LeftSidebar
        sections={[
          {
            title: 'Section 1',
            buttons: [
              {
                id: 'button-1',
                label: 'Button 1',
                to: '/button-1',
                icon: icons.user,
              },
            ],
          },
        ]}
        data-testid={'left-sidebar'}
      />,
    )
    expect(getByTestId('left-sidebar')).toBeInTheDocument()
  })
})
