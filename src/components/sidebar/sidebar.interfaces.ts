import { IconProp } from "@fortawesome/fontawesome-svg-core";
import {IconInterface} from "../icon/Icon";

export type SidebarButtonType = 'link' | 'button' | 'custom';

export interface SidebarButton {
    id: string;
    icon: IconInterface | IconProp,
    to: string;
    label: string;
    type?: SidebarButtonType;
    render?: JSX.Element;
}
