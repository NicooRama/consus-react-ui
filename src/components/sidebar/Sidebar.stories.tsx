import type {Meta, StoryObj} from '@storybook/react';
import {Sidebar} from './Sidebar';
import {routerDecorator} from "../../storybook.utils";
import {icons} from "../icon/icons";
import {faCoffee} from "@fortawesome/free-solid-svg-icons";
import {SidebarProvider} from "./SidebarContext";
import {Select} from "../select/Select";

const decorators = [
    routerDecorator,
    (Story: any) => (
        <SidebarProvider>
            <Story/>
            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
                scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
                into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                software like Aldus PageMaker including versions of Lorem Ipsum.
            </p>
        </SidebarProvider>
    ),
]

const meta: any = {
    title: 'Commons/Sidebar',
    component: Sidebar,
    tags: ['autodocs'],
} satisfies Meta<typeof Sidebar>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    decorators,
    args: {
        sections: [
            {
                title: '',
                buttons: [
                    {
                        id: 'dashboard',
                        icon: icons.check,
                        label: 'Dashboard',
                        to: ''
                    },
                    {
                        id: 'search',
                        icon: icons.check,
                        label: 'Search',
                        to: ''
                    },
                    {
                        id: 'insights',
                        icon: icons.check,
                        label: 'Insights',
                        to: ''
                    },
                    {
                        id: 'docs',
                        icon: icons.check,
                        label: 'Docs',
                        to: ''
                    },
                ]
            },
            {
                title: 'PRODUCTOS',
                buttons: [
                    {
                        id: 'products',
                        icon: icons.check,
                        label: 'Products',
                        to: ''
                    },
                ]
            },
            {
                title: '',
                buttons: [
                    {
                        id: 'dashboard',
                        icon: icons.check,
                        label: 'Dashboard',
                        to: ''
                    },
                    {
                        id: 'search',
                        icon: icons.check,
                        label: 'Search',
                        to: ''
                    },
                    {
                        id: 'insights',
                        icon: icons.check,
                        label: 'Insights',
                        to: ''
                    },
                    {
                        id: 'docs',
                        icon: icons.check,
                        label: 'Docs',
                        to: ''
                    },
                ]
            },
        ],
    },
};

export const WithExternalButton: Story = {
    decorators,
    args: {
        ...Primary.args,
        topButtons: [
            {
                id: 'custom',
                render: <Select items={['ES', 'EN', 'LATAM']} selected={undefined} onChange={(value: string) => console.log(value)}/>,
                type: 'custom'
            },
            {
                id: 'donation',
                label: 'Ayudame con un cafecito',
                to: 'https://cafecito.app/poke-collection',
                icon: faCoffee,
                type: 'button'
            }
        ]
    }
};

export const WithCustomButton: Story = {
    decorators,
    args: {
        ...Primary.args,
        topButtons: [
            {
                id: 'custom',
                render: <Select items={['ES', 'EN', 'LATAM']} selected={undefined} onChange={(value: string) => console.log(value)}/>,
                type: 'custom'
            },
            {
                id: 'signIn',
                label: 'Sign in',
                to: '/ingresar',
                icon: icons.user,
                type: 'link'
            },
            {
                id: 'signUp',
                label: 'Sign up',
                to: '/registrarse',
                icon: icons.signUp,
                type: 'link'
            },
        ]
    }
};
