import { Sidebar } from './Sidebar'
import { renderWithRouter } from '../../../test/test.utils'
import {icons} from "../icon/icons";

describe('<Sidebar />', () => {
  it('renders <Sidebar /> successfully', () => {
    const { getByTestId } = renderWithRouter(<Sidebar data-testid={'sidebar'}
                                                      sections={[
                                                          {
                                                              title: 'Section 1',
                                                              buttons: [
                                                                  {
                                                                      id: 'button-1',
                                                                      label: 'Button 1',
                                                                      to: '/button-1',
                                                                      icon: icons.user,
                                                                  },
                                                              ],
                                                          },
                                                      ]}
    />)
    expect(getByTestId('sidebar')).toBeInTheDocument()
  })
})
