import {render} from "../../../test/test.utils";
import {HamburgerButton} from "./HamburgerButton";

describe('<HamburgerButton />', () => {
    it('renders <HamburgerButton /> successfully', () => {
        const {getByTestId} = render(<HamburgerButton data-testid={'hamburger-button'} onClick={() => {}}/>)
        expect(getByTestId('hamburger-button')).toBeInTheDocument();
    });
});
