import React from "react"

import {TopSidebar, TopSidebarProps} from "./TopSidebar/TopSidebar";
import {LeftSidebar, LeftSidebarProps} from "./LeftSidebar";

export const Sidebar: React.FC<TopSidebarProps & LeftSidebarProps> = ({
                            sections,
                            topButtons,
                            active,
                            isLoggedIn,
                            signInText,
                            signInLink,
                            signUpLink,
                            signUpText,
                            logoutText,
                            logoutLink,
                            loading,
                            ...props
                        }) => {
    return (<div {...props}>
        <TopSidebar isLoggedIn={isLoggedIn}
                    topButtons={topButtons}
                    signInLink={signInLink}
                    signInText={signInText}
                    signUpLink={signUpLink}
                    signUpText={signUpText}
                    logoutText={logoutText}
                    logoutLink={logoutLink}
                    loading={loading}
        />
        <LeftSidebar sections={sections} active={active}/>
    </div>)
};
