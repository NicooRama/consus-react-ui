import { createContext, useContext, useState } from "react";

const SidebarContext = createContext({
        opened: false,
        close: () => {},
        open: () => {},
    });

export const SidebarProvider = ({children}: any) => {
    const [opened, setOpened] = useState(false);

    const close = () => {
        setOpened(false);
    };

    const open = () => {
        setOpened(true)
    }

    return <SidebarContext.Provider value={{opened, close, open}}>
        {children}
    </SidebarContext.Provider>
};

export const useSidebar = () => useContext(SidebarContext);
