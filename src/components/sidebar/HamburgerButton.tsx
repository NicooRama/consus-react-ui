import React from 'react'

import styled from 'styled-components'

interface HamburgerButtonProps {
  onClick: () => void
}

const Line = styled.div`
  height: 3px;
  border-radius: ${({ theme }: any) => theme.borderRadius}px;
  width: 25px;
  background-color: ${({ theme }: any) => theme.colors.sidebar.text.normal};
`

const Container = styled.div`
  display: flex;
  gap: 6px;
  padding: ${({ theme }: any) => theme.spacing.sm}px;
  flex-direction: column;
  width: fit-content;
  cursor: pointer;

  &:hover {
    transition-duration: 0.2s;

    * {
      background-color: ${({ theme }: any) => theme.colors.sidebar.text.hover};
    }
  }

  &:active {
    transition-duration: 0.2s;
    background-color: ${({ theme }: any) => theme.colors.sidebar.background.hover};
    border-radius: 50px;

    div {
      background-color: ${({ theme }: any) => theme.colors.sidebar.text.active};
    }
  }
`

export const HamburgerButton: React.FC<HamburgerButtonProps> = ({ onClick, ...props }) => {
  return (
    <Container onClick={onClick} role={'button'} {...props}>
      <Line />
      <Line />
      <Line />
    </Container>
  )
}
