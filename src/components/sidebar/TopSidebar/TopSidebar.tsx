import React from 'react'
import styled from 'styled-components'
import {HamburgerButton} from '../HamburgerButton'
import {useSidebar} from '../SidebarContext'
import {SidebarButton, SidebarButtonType} from '../sidebar.interfaces'
import {Link} from 'react-router-dom'
import {ConsusTheme} from '../../../styles/theme.interface'
import {Text} from '../../text/Text'
import {Icon, IconInterface} from '../../icon/Icon'
import {icons} from '../../icon/icons'
import {IconButton} from '../../icon-button/IconButton'
import {mobileMediaQuery} from "../../../styles/common";
import {TopSidebarLoading} from "./TopSidebarLoading";

const Container = styled.div`
    display: flex;
    align-items: center;
    gap: ${({theme}: any) => theme.spacing.sm}px;
    background-color: ${({theme}: any) => theme.colors.sidebar.background.normal};
    padding: ${({theme}: any) => theme.spacing.xs}px;
    padding-right: ${({theme}: any) => theme.spacing.xs + theme.spacing.sm}px;
    position: fixed;
    width: -moz-available;
    width: -webkit-fill-available;
    z-index: 20;

    button {
        flex-shrink: 0;
    }
`

const Dummy = styled.div`
    height: 60px;
`

export interface TopSidebarProps {
    isLoggedIn?: boolean
    topButtons?: SidebarButton[]
    signInText?: string
    signInLink?: string
    signUpText?: string
    signUpLink?: string
    logoutText?: string
    logoutLink?: string
    loading?: boolean;
}

const textColor = (theme: ConsusTheme) => theme.colors.bar.text.normal
const textHover = (theme: ConsusTheme) => theme.colors.bar.text.hover
const backgroundHover = (theme: ConsusTheme) => theme.colors.bar.background.hover
const backgroundPressed = (theme: ConsusTheme) => theme.colors.bar.background.pressed

const BarLink = styled(Link)`
    display: flex;
    gap: ${({theme}: any) => theme.spacing.xs}px;
    text-decoration: none;
    flex-shrink: 0;
    padding: ${({theme}: any) => theme.spacing.sm}px;
    border: 1px solid ${({theme}: any) => textColor(theme)};
    border-radius: ${({theme}: any) => theme.borderRadius}px;

    .link-label {
        color: ${({theme}: any) => textColor(theme)};

        ${mobileMediaQuery} {
            display: none;
        }
    }

    .link-icon {
        path {
            fill: ${({theme}: any) => textColor(theme)};
        }
    }

    &:active {
        background-color: ${({theme}: any) => backgroundPressed(theme)};
    }

    &:hover {
        background-color: ${({theme}: any) => backgroundHover(theme)};
        border-color: ${({theme}: any) => backgroundHover(theme)};

        .link-label {
            color: ${({theme}: any) => textHover(theme)};
        }

        .link-icon {
            path {
                fill: ${({theme}: any) => textHover(theme)};
            }
        }
    }
`

const ButtonsContainer = styled.div`
    margin-left: auto;
    display: flex;
    gap: ${({theme}: any) => theme.spacing.sm}px;
    align-items: center;

    button {
        &.with-children {
            ${mobileMediaQuery} {
                padding: ${({theme}: any) => `${theme.spacing.sm - 2}px`};
                width: auto;

                .children {
                    display: none;
                }
            }
        }
    }
`

export const TopSidebar: React.FC<TopSidebarProps> = ({
                                                          topButtons,
                                                          isLoggedIn = false,
                                                          signInText = 'Acceder',
                                                          signUpText = 'Registrarse',
                                                          logoutText = 'Salir',
                                                          signInLink = '/sign-in',
                                                          signUpLink = '/sign-up',
                                                          logoutLink = '/logout',
                                                          loading = false,
                                                          ...props
                                                      }) => {
    const {open} = useSidebar()

    topButtons =
        topButtons ||
        (!isLoggedIn
            ? [
                {
                    id: 'signIn',
                    label: signInText || 'Acceder',
                    to: signInLink,
                    icon: icons.user,
                },
                {
                    id: 'signUp',
                    label: signUpText || 'Registrarse',
                    to: signUpLink,
                    icon: icons.signUp,
                },
            ]
            : [
                {
                    id: 'logout',
                    label: logoutText || 'Salir',
                    to: logoutLink,
                    icon: icons.logout,
                },
            ])

    const openLink = (link: string) => {
        window.open(link, '_blank')
    }

    const onMiddleClick = (link: string) => (event: any) => {
        if (event.nativeEvent.button === 1) {
            openLink(link)
        }
    }

    const renderMap = new Map<string, any>([
        [
            'button', (button: SidebarButton) => (
            <IconButton
                icon={button.icon}
                onClick={() => openLink(button.to)}
                onMouseDown={onMiddleClick(button.to)}
            >
                {button.label}
            </IconButton>
        ),], [
            'link', (button: SidebarButton) => (
                <BarLink to={button.to} key={button.id} title={button.label}>
                    <Icon
                        icon={button.icon as IconInterface}
                        width={16}
                        height={16}
                        className={'link-icon'}
                    />
                    <Text size={'xs'} className={'link-label'}>
                        {button.label}
                    </Text>
                </BarLink>
            )
        ],
        [
            'custom', (button: SidebarButton) => button.render
        ]
    ])


    return (
        <div {...props}>
            <Container>
                <HamburgerButton onClick={open}/>
                <ButtonsContainer>
                    {loading ? <TopSidebarLoading quantity={topButtons.length || 0}/> : topButtons.map((button) => {
                        const render = renderMap.get(button.type || '');
                        return render ? render(button) : undefined;
                    })}
                </ButtonsContainer>
            </Container>
            <Dummy/>
        </div>
    )
}
