import { useTheme } from "styled-components";
import {SkeletonButton} from "../../skeletons";
import {useOnResize} from "../../../hooks/window.hooks";
import {useCallback, useState } from "react";
import {ConsusTheme} from "../../../styles/theme.interface";

interface TopSidebarLoadingProps {
    quantity: number;
}

const range = (count: number) => Array.from({length: count}, (_, i) => i);

export const TopSidebarLoading = ({
                                      quantity,
                                  }: TopSidebarLoadingProps) => {
    const [width,setWidth] = useState(95);
    const height = 42;
    const theme = useTheme() as ConsusTheme;

    const changeWidth = useCallback((windowWidth: number) => {
        if(windowWidth < theme.breakpoints.mobile.max) {
            setWidth(42);
        } else {
            setWidth(95);
        }
    }, [width])

    useOnResize(changeWidth, {fireOnInit: true});

    return (<>
        {
            range(quantity).map((i) => (
                <SkeletonButton
                    key={`loadingButton-${i}`}
                    width={width}
                    height={height}
                />
            ))
        }
    </>)
}
