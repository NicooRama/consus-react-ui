import { TopSidebar } from './TopSidebar'
import { renderWithRouter } from '../../../../test/test.utils'
import { icons } from '../../icon/icons'

describe('<TopSidebar />', () => {
  it('renders <TopSidebar /> successfully', () => {
    const { getByTestId } = renderWithRouter(
      <TopSidebar
        data-testid={'top-sidebar'}
        topButtons={[
          {
            id: 'button-1',
            label: 'Button 1',
            to: '/button-1',
            icon: icons.user,
          },
        ]}
      />,
    )
    expect(getByTestId('top-sidebar')).toBeInTheDocument()
  })
})
