import React from "react"
import {css} from "styled-components";
import styled from "styled-components";
import {Link} from "react-router-dom";
import {Text} from "../text/Text";
import {Icon} from "../icon/Icon";
import {Divider} from "../divider/Divider";
import {ConsusTheme} from "../../styles/theme.interface";
import {HamburgerButton} from "./HamburgerButton";
import {useSidebar} from "./SidebarContext";
import {SidebarButton} from "./sidebar.interfaces";

export interface LeftSidebarProps {
    sections: SidebarSection[];
    active?: string;
}

interface SidebarSection {
    title?: string;
    buttons: SidebarButton[];
}

const backgroundColor = (theme: ConsusTheme) => theme.colors.sidebar.background.normal
const activeBackgroundColor = (theme: ConsusTheme) => theme.colors.sidebar.background.active
const activeTextColor = (theme: ConsusTheme) => theme.colors.sidebar.text.active
const textColor = (theme: ConsusTheme) => theme.colors.sidebar.text.normal
const textHover = (theme: ConsusTheme) => theme.colors.sidebar.text.hover
const backgroundHover = (theme: ConsusTheme) => theme.colors.sidebar.background.hover
const backgroundPressed = (theme: ConsusTheme) => theme.colors.sidebar.background.pressed

const SectionDivider = styled(Divider)`
    &.section-divider {
        border-color: ${({theme}: any) => backgroundHover(theme)};
    }
`

const Section = styled.div`
    display: flex;
    gap: ${({theme}: any) => theme.spacing.xxs}px;
    flex-direction: column;
    padding-top: ${({theme}: any) => theme.spacing.xs}px;
    padding-bottom: ${({theme}: any) => theme.spacing.xs}px;
`

const SectionTitle = styled(Text)`
    &.section-title {
        color: ${({theme}: any) => textColor(theme)}
    }

    padding-left: ${({theme}: any) => theme.spacing.sm}px;
    padding-right: ${({theme}: any) => theme.spacing.sm}px;
    padding-top: ${({theme}: any) => theme.spacing.xs}px;
    padding-bottom: ${({theme}: any) => theme.spacing.xs}px;

`

const SidebarLink = styled(Link)<{ active: boolean }>`
    display: flex;
    text-decoration: none;
    color: ${({theme}: any) => textColor(theme)};
    align-items: center;
    gap: ${({theme}: any) => theme.spacing.xs}px;
    padding: ${({theme}: any) => theme.spacing.sm}px;
    border-radius: ${({theme}: any) => theme.borderRadius}px;
    min-width: 120px;

    ${({theme, active}: any) => active && css`
        background-color: ${activeBackgroundColor(theme)};

        .button-label {
            color: ${activeTextColor(theme)} !important;
        }

        .sidebar-icon {
            path {
                fill: ${activeTextColor(theme)};
            }
        }
    `}
    &:active {
        background-color: ${({theme}: any) => backgroundPressed(theme)};
    }

    &:hover {
        transition-duration: 0.2s;
        background-color: ${({theme}: any) => backgroundHover(theme)};

        .sidebar-icon {
            path {
                fill: ${({theme}: any) => textHover(theme)};
            }
        }

        .button-label {
            color: ${({theme}: any) => textHover(theme)};
        }
    }
`

const ButtonLabel = styled(Text)`
    &.button-label {
        color: ${({theme}: any) => textColor(theme)};
    }
`

const StyledIcon = styled(Icon as any)`
    &.sidebar-icon {
        path {
            fill: ${({theme}: any) => textColor(theme)}
        }
    }
`

const Nav = styled.nav<{ open: boolean }>`
    width: ${({open}: any) => open ? '100%' : 'auto'};
    opacity: 1;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
`

const Overlay = styled.div<{ open: boolean }>`
    background-color: rgba(0, 0, 0, 0.4);
    width: 100%;
    height: 100%;
    position: ${({open}: any) => open ? 'fixed' : 'absolute'};
    top: 0;
    bottom: 0;
    left: 0;
    z-index: 21;
`

const Content = styled.div`
    position: fixed;
    z-index: 22;
    background-color: ${({theme}: any) => backgroundColor(theme)};
    overflow-y: auto;
    height: 100%;
    width: 0;
    transition-duration: 0.25s;

    &.open {
        width: 200px;
        padding-top: ${({theme}: any) => theme.spacing.xs}px;
        padding-left: ${({theme}: any) => theme.spacing.xs}px;
        padding-right: ${({theme}: any) => theme.spacing.xs}px;
        border-right: 1px solid ${({theme}: any) => backgroundHover(theme)};
    }

    ::-webkit-scrollbar {
        width: 4px;
        height: 4px
    }

    ::-webkit-scrollbar-track {
        box-shadow: none;
        border-radius: ${({theme}: any) => theme.borderRadius}px;
    }

    ::-webkit-scrollbar-thumb {
        background: ${({theme}: any) => textColor(theme)};
        border-radius: ${({theme}: any) => theme.borderRadius}px;
    }

    ::-webkit-scrollbar-thumb:hover {
        background: ${({theme}: any) => activeTextColor(theme)};
    }
`

export const LeftSidebar: React.FC<LeftSidebarProps> = ({
                                                            sections,
                                                            active,
                                                            ...props
                                                        }) => {
    const {opened, close} = useSidebar();

    return (<Nav open={opened} {...props}>
        <Content className={opened ? 'open' : ''}>
            <HamburgerButton onClick={close}/>
            {
                sections.filter(section => section.buttons.length > 0)
                    .map((section, i) => (
                        <div key={section.title || `section-${i}`}>
                            <Section>
                                {
                                    section.title &&
                                    <SectionTitle className={'section-title'} size={'xxs'}
                                                  weight={'semiBold'}>{section.title}</SectionTitle>
                                }
                                {
                                    section.buttons.map((button) => (
                                        <SidebarLink to={button.to} active={button.id === active}
                                                     key={button.id}>
                                            <StyledIcon icon={button.icon} width={16} height={16}
                                                        className={'sidebar-icon'}/>
                                            <ButtonLabel size={'xs'} className={'button-label'}>
                                                {button.label}
                                            </ButtonLabel>
                                        </SidebarLink>
                                    ))
                                }
                            </Section>
                            {i !== sections.length - 1 && <SectionDivider className={'section-divider'}/>}
                        </div>
                    ))
            }
        </Content>
        <Overlay onClick={close} open={opened}/>
    </Nav>)
};
