import {SidebarButton} from "./sidebar.interfaces";
import { useLocation } from 'react-router-dom'

export const useActiveSidebarButton = (buttons: SidebarButton[]) => {
    const {pathname} = useLocation();
    const sortedButtons = [...buttons];
    sortedButtons.sort((a, b) => (
        b.to.length - a.to.length
    ));
    const button = buttons.find((button) => pathname.startsWith(button.to));
    return button?.id;
}
