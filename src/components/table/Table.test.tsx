import {Table} from "./Table";
import {render} from "../../../test/test.utils";

describe('<Table />', () => {
    it('renders <Table /> successfully', () => {
        const {getByTestId} = render(<Table data-testid={'table'} headers={[
            {title: 'Title 1'},
            {title: 'Title 2'},
            {title: 'Title 3'},
        ]}>
            <tbody>
                <tr>
                    <td>Data 1</td>
                    <td>Data 2</td>
                    <td>Data 3</td>
                </tr>
            </tbody>
        </Table>)
        expect(getByTestId('table')).toBeInTheDocument();
    });
});
