import { ReactElement } from 'react'
import styled from 'styled-components'
import { Text } from '../text/Text'

export const TableContainer = styled.div`
  width: 100%;
  overflow: auto;

  table {
    border: 1px solid ${({ theme }: any) => theme.colors.border};
    border-radius: ${({ theme }: any) => theme.borderRadius}px;
    border-collapse: separate;
    border-spacing: 0;
    width: 100%;

    th,
    td {
      border-bottom: ${({ theme }: any) => theme.border};
      border-right: ${({ theme }: any) => theme.border};

      &:last-of-type {
        border-right: 0;
      }
    }

    td.borderless {
      border: 0;
    }

    td.td-left {
      text-align: left;
    }

    td.border-left {
      border-left: ${({ theme }: any) => theme.border};
    }

    thead {
      th {
        padding: ${({ theme }: any) => theme.spacing.md}px;
        text-align: center;

        p {
          font-weight: 600;
        }
      }
    }

    tbody {
      tr {
        td {
          text-align: center;
          padding: ${({ theme }: any) => theme.spacing.sm}px
            ${({ theme }: any) => theme.spacing.md}px;

          &:first-of-type {
            text-align: left;
          }
        }

        &:last-of-type {
          td {
            border-bottom: 0;
          }
        }
      }
    }
  }
`

interface Props {
  headers: {
    title: string
  }[]
  children: ReactElement
  className?: string
}

export const Table = ({ headers, children, className = '', ...props }: Props) => {
  return (
    <TableContainer className={className} {...props}>
      <table>
        <thead>
          {headers.map((header, i) => (
            <th key={i}>
              <Text size={'xs'} className={'th-title'}>
                {header.title}
              </Text>
            </th>
          ))}
        </thead>
        {children || <tbody></tbody>}
      </table>
    </TableContainer>
  )
}
