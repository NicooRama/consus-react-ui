import type { Meta, StoryObj } from '@storybook/react'
import { Table } from './Table'

const meta: any = {
  title: 'Commons/Table',
  component: Table,
  tags: ['autodocs'],
} satisfies Meta<typeof Table>

export default meta
type Story = StoryObj<typeof meta>

export const Primary: Story = {
  args: {
    headers: [{ title: 'Title 1' }, { title: 'Title 2' }, { title: 'Title 3' }],
    children: (
      <tbody>
        <tr>
          <td>Data 1</td>
          <td>Data 2</td>
          <td>Data 3</td>
        </tr>
        <tr>
          <td>Data 4</td>
          <td>Data 5</td>
          <td>Data 6</td>
        </tr>
        <tr>
          <td>Data 7</td>
          <td>Data 8</td>
          <td>Data 9</td>
        </tr>
      </tbody>
    ),
  },
}
