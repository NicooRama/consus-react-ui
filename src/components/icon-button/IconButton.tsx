import { ThemeColorIndex } from '../../styles/theme.interface'
import { Button } from '../button/Button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'

import { mobileMediaQuery } from '../../styles/common'
import styled from 'styled-components'

const StyledButton = styled(Button)`
  display: flex;
  gap: ${({ theme }: any) => theme.spacing.sm}px;
  align-items: center;
  flex-wrap: wrap;

  .children {
    display: inline-flex
  }

  &.circle {
    border-radius: 50px;
    position: relative;
    padding: ${({ theme }: any) => `${theme.spacing.sm / 2 + 1}px ${theme.spacing.sm}px`};

    ${mobileMediaQuery} {
      width: auto;
    }
  }

  &.circle:not(&.with-children):not(&.mini) {
    padding-top: ${({ theme }: any) => theme.spacing.sm - 1}px;
    padding-bottom: ${({ theme }: any) => theme.spacing.sm - 1}px;
  }

  &.mini {
    font-size: ${({ theme }: any) => theme.font.size.xxs}px;
    padding ${({ theme }: any) => `${theme.spacing.xxs}px ${theme.spacing.xxs + 0.75}px`};
    height: auto;

    ${mobileMediaQuery} {
      width: auto;
    }
  }

  &.with-children {
    ${mobileMediaQuery} {
      width: 100%;
    }
  }
`

export interface IconButtonProps {
  onClick?: (e?: any) => void
  icon: any
  color?: ThemeColorIndex
  variant?: 'primary' | 'secondary' | 'tertiary'
  mini?: boolean
  circle?: boolean
  children?: any
  className?: string
  iconClass?: string

  [props: string]: any
}

export interface CustomIconButtonProps extends Omit<IconButtonProps, 'icon'> {}

export const IconButton: React.FC<IconButtonProps> = ({
  className = '',
  icon,
  iconClass = '',
  mini = false,
  circle = false,
  onClick = () => {},
  children,
  color = 'primary.normal',
  variant = 'primary',
  ...props
}) => {
  const generateClass = () => {
    let generatedClass = ''
    // if (variant === 'reversed') generatedClass += "reversed ";
    if (circle) generatedClass += 'circle '
    if (mini) generatedClass += 'mini '
    if (children) generatedClass += 'with-children '
    generatedClass += className
    return generatedClass
  }

  return (
    <StyledButton
      className={generateClass()}
      onClick={onClick}
      color={color}
      variant={variant}
      {...props}
    >
      <FontAwesomeIcon
        icon={icon}
        className={`icon ${iconClass} ${!!children && 'with-brother'}`}
      />
      {(!!children && <div className={'children'}>{children}</div>) as any}
    </StyledButton>
  )
}
