import React from 'react'
import PropTypes from 'prop-types'
import { faMinus } from '@fortawesome/free-solid-svg-icons'
import { CustomIconButtonProps, IconButton } from './IconButton'

export const SubstractButton: React.FC<CustomIconButtonProps> = ({
                                                                   children,
                                                                   onClick,
                                                                   className = '',
                                                                   ...props
                                                                 }) => {
  return (
      <IconButton
          icon={faMinus}
          color={'error.normal'}
          onClick={onClick}
          className={className}
          {...props}
      >
        {children}
      </IconButton>
  )
}

SubstractButton.propTypes = {
  className: PropTypes.string,
};
