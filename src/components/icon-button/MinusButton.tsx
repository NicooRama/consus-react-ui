import React from 'react'
import PropTypes from 'prop-types'
import { faMinus } from '@fortawesome/free-solid-svg-icons'
import { CustomIconButtonProps, IconButton } from './IconButton'

export const MinusButton: React.FC<CustomIconButtonProps> = ({
                                                                 children,
                                                                 onClick,
                                                                 className = '',
                                                                 ...props
                                                             }) => {
    return (
        <IconButton icon={faMinus} onClick={onClick} className={className} {...props}>
            {children}
        </IconButton>
    )
}

MinusButton.propTypes = {
    className: PropTypes.string,
};
