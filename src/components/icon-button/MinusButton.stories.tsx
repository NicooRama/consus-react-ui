import type {Meta, StoryObj} from '@storybook/react';
import {MinusButton} from './MinusButton';

const meta: any = {
    title: 'Button/MinusButton',
    component: MinusButton,
    tags: ['autodocs'],
} satisfies Meta<typeof MinusButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: 'Button'
    },
};
