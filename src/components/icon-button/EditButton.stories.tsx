import type {Meta, StoryObj} from '@storybook/react';
import {EditButton} from './EditButton';

const meta: any = {
    title: 'Button/EditButton',
    component: EditButton,
    tags: ['autodocs'],
} satisfies Meta<typeof EditButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: 'Button'
    },
};
