import type {Meta, StoryObj} from '@storybook/react';
import {SubstractButton} from './SubstractButton';

const meta: any = {
    title: 'Button/SubstractButton',
    component: SubstractButton,
    tags: ['autodocs'],
} satisfies Meta<typeof SubstractButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
};
