import {IconButton} from "./IconButton";
import {render} from "../../../test/test.utils";
import {faCoffee} from "@fortawesome/free-solid-svg-icons";
import {screen} from "@testing-library/react";

describe('<IconButton />', () => {
    it('renders <IconButton /> successfully', () => {
        const {getByTestId} = render(<IconButton data-testid={'icon-button'} icon={faCoffee} onClick={() => {}}/>)
        expect(getByTestId('icon-button')).toBeInTheDocument();
    });

    it('onClick fire event successfully', () => {
        const handleClick = jest.fn();
        render(<IconButton onClick={handleClick} icon={faCoffee} />)
        const button = screen.getByRole('button');
        button.click();
        expect(handleClick).toHaveBeenCalled();
    });

    it('set mini class when mini flag is setted true', () => {
        render(<IconButton onClick={jest.fn()} mini icon={faCoffee} />)
        const button = screen.getByRole('button')
        expect(button).toHaveClass('mini')
    })

    it('set circle class when circle flag is setted true', () => {
        render(<IconButton onClick={jest.fn()} circle icon={faCoffee} />)
        const button = screen.getByRole('button')
        expect(button).toHaveClass('circle')
    })

    it('set with-children class when the button has children', () => {
        render(<IconButton onClick={jest.fn()} circle icon={faCoffee}>Cafe</IconButton>)
        const button = screen.getByRole('button')
        expect(button).toHaveClass('with-children')
    })

    it('set reversed class when the button has variant "secondary"', () => {
        render(<IconButton onClick={jest.fn()} variant={'secondary'} icon={faCoffee}>Cafe</IconButton>)
        const button = screen.getByRole('button')
        expect(button).toHaveClass('secondary')
    })

    it('doesn\'t have with-children class because doen\'t have a children', () => {
        render(<IconButton onClick={jest.fn()} circle icon={faCoffee} />)
        const button = screen.getByRole('button')
        expect(button).not.toHaveClass('with-children')
    })
});
