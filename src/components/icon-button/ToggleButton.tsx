import React from "react"
import {IconButton} from "./IconButton";

export interface ToggleButtonProps {
    enabled: boolean,
    enabledIcon: any,
    disabledIcon: any,
    onClick: () => void,
    className?: string,
    children?: any,
    }

export const ToggleButton: React.FC<ToggleButtonProps> = ({
                                 enabled,
                                 enabledIcon,
                                 disabledIcon,
                                 onClick,
                                 className = "",
                                 children,
                                 ...props
                             }) => {
    return (<IconButton icon={enabled ? enabledIcon : disabledIcon}
                        onClick={onClick}
                        className={className}
                        {...props}>{children}</IconButton>)
};
