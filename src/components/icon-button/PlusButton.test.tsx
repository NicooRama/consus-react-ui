import {PlusButton} from "./PlusButton";
import {render} from "../../../test/test.utils";

describe('<PlusButton />', () => {
    it('renders <PlusButton /> successfully', () => {
        const {getByTestId} = render(<PlusButton data-testid={'plus-button'}/>)
        expect(getByTestId('plus-button')).toBeInTheDocument();
    });
});
