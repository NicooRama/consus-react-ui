import React from 'react'
import PropTypes from 'prop-types'
import { CustomIconButtonProps, IconButton } from './IconButton'
import styled from 'styled-components'
import { faEdit } from '@fortawesome/free-regular-svg-icons'

const StyledIconButton = styled(IconButton)`
  &.circle:not(&.with-children):not(&.mini) {
    padding-left: ${({ theme }: any) => theme.spacing.sm - 2}px;
    padding-right: ${({ theme }: any) => theme.spacing.sm - 2}px;
  }

  &.mini {
    padding: ${({ theme }: any) => `${theme.spacing.xxs}px ${theme.spacing.xxs - 0.75}px`};
  }
`

export const EditButton: React.FC<CustomIconButtonProps> = ({
  children,
  onClick,
  className = '',
  ...props
}) => {
  return (
    <StyledIconButton
      icon={faEdit}
      color={'warn.normal'}
      onClick={onClick}
      className={className}
      {...props}
    >
      {children}
    </StyledIconButton>
  )
}

EditButton.propTypes = {
  className: PropTypes.string,
}
