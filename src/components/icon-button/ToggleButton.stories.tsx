import type {Meta, StoryObj} from '@storybook/react';
import {ToggleButton} from './ToggleButton';
import {makeControlForObjectArray} from "../../storybook.utils";
import {faEye, faEyeSlash} from "@fortawesome/free-solid-svg-icons";
import {icons} from "../icon/icons";

const meta: any = {
    title: 'Button/ToggleButton',
    component: ToggleButton,
    tags: ['autodocs'],
} satisfies Meta<typeof ToggleButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        enabled: true,
        onClick: () => console.log('click!'),
        children: 'Toggle'
    },
    argTypes: {
        enabledIcon: makeControlForObjectArray(Object.values(icons), '3', 'select', faEye),
        disabledIcon: makeControlForObjectArray(Object.values(icons), '3', 'select', faEyeSlash),
    }
};
