import React from 'react'
import { CustomIconButtonProps, IconButton } from './IconButton'
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons'

export const TrashButton: React.FC<CustomIconButtonProps> = ({
  children,
  onClick,
  className = '',
  ...props
}) => {
  return (
    <IconButton
      circle
      icon={faTrashAlt}
      variant={'secondary'}
      color={'error.normal'}
      onClick={onClick}
      className={className}
      {...props}
    >
      {children}
    </IconButton>
  )
}
