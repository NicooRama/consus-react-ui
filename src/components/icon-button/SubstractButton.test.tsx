import {SubstractButton} from "./SubstractButton";
import {render} from "../../../test/test.utils";

describe('<SubstractButton />', () => {
    it('renders <SubstractButton /> successfully', () => {
        const {getByTestId} = render(<SubstractButton data-testid={'substract-button'}/>)
        expect(getByTestId('substract-button')).toBeInTheDocument();
    });
});
