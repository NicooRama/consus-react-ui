import type {Meta, StoryObj} from '@storybook/react';
import {PlusButton} from './PlusButton';

const meta: any = {
    title: 'Button/PlusButton',
    component: PlusButton,
    tags: ['autodocs'],
} satisfies Meta<typeof PlusButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: 'Button'
    },
};
