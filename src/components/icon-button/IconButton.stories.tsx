import type {Meta, StoryObj} from '@storybook/react';
import {IconButton} from './IconButton';
import {faCoffee} from "@fortawesome/free-solid-svg-icons";

const meta: any = {
    title: 'Button/IconButton',
    component: IconButton,
    tags: ['autodocs'],
} satisfies Meta<typeof IconButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        icon: faCoffee,
        children: 'Coffe',
    },
};
