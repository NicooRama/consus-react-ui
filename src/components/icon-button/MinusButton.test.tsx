import {MinusButton} from "./MinusButton";
import {render} from "../../../test/test.utils";

describe('<MinusButton />', () => {
    it('renders <MinusButton /> successfully', () => {
        const {getByTestId} = render(<MinusButton data-testid={'minus-button'}/>)
        expect(getByTestId('minus-button')).toBeInTheDocument();
    });
});
