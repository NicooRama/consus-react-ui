import { ToggleButton } from './ToggleButton'
import { render } from '../../../test/test.utils'
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons'

describe('<ToggleButton />', () => {
  it('renders <ToggleButton /> successfully', () => {
    const { getByTestId } = render(
      <ToggleButton
        data-testid={'toggle-button'}
        disabledIcon={faEyeSlash}
        enabledIcon={faEye}
        enabled={true}
       onClick={jest.fn}/>,
    )
    expect(getByTestId('toggle-button')).toBeInTheDocument()
  })
})
