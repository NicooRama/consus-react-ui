import type {Meta, StoryObj} from '@storybook/react';
import {TrashButton} from './TrashButton';

const meta: any = {
    title: 'Button/TrashButton',
    component: TrashButton,
    tags: ['autodocs'],
} satisfies Meta<typeof TrashButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
};
