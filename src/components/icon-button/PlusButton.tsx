import React from 'react'
import PropTypes from 'prop-types'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { CustomIconButtonProps, IconButton } from './IconButton'

export const PlusButton: React.FC<CustomIconButtonProps> = ({
                                                                children,
                                                                onClick,
                                                                className = '',
                                                                ...props
                                                            }) => {
    return (
        <IconButton icon={faPlus} onClick={onClick} className={className} {...props}>
            {children}
        </IconButton>
    )
}

PlusButton.propTypes = {
    className: PropTypes.string,
};
