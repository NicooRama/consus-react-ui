import {TrashButton} from "./TrashButton";
import {render} from "../../../test/test.utils";

describe('<TrashButton />', () => {
    it('renders <TrashButton /> successfully', () => {
        const {getByTestId} = render(<TrashButton data-testid={'trash-button'}/>)
        expect(getByTestId('trash-button')).toBeInTheDocument();
    });
});
