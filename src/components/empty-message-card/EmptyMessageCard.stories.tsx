import type {Meta, StoryObj} from '@storybook/react';
import {EmptyMessageCard} from './EmptyMessageCard';

const meta: any = {
    title: 'Commons/EmptyMessageCard',
    component: EmptyMessageCard,
    tags: ['autodocs'],
} satisfies Meta<typeof EmptyMessageCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        message: 'Aun no tienes elementos'
    },
};
