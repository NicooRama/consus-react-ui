import {EmptyMessageCard} from "./EmptyMessageCard";
import {render} from "../../../test/test.utils";

describe('<EmptyMessage />', () => {
    it('renders <EmptyMessage /> successfully', () => {
        const {getByTestId} = render(<EmptyMessageCard message={'Aun no tienes elementos'} data-testid={'empty-message'}/>)
        expect(getByTestId('empty-message')).toBeInTheDocument();
    });
});
