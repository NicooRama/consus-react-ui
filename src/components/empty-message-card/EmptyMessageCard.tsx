import styled from 'styled-components';
import {mobileMediaQuery} from "../../styles/common";
import {Card} from "../card/Card";
import {Text} from '../text/Text';

const StyledCard = styled(Card)`
  width: fill-available;
  min-height: 310px;
  display: flex;
  place-items: center;
  place-content: center;

  p {
    text-align: center;
  }

  ${mobileMediaQuery} {
    min-height: initial;
  }
`

interface Props {
    message: string;
}

export const EmptyMessageCard = ({message, ...props}: Props) => {
    return (<StyledCard {...props}>
        <Text size={'md'} weight={'semiBold'} color={'fade'}>
            {message}
        </Text>
    </StyledCard>)
};
