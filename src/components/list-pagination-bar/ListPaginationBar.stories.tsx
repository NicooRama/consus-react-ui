import type {Meta, StoryObj} from '@storybook/react';
import {ListPaginationBar} from './ListPaginationBar';

const meta: any = {
    title: 'Pagination/ListPaginationBar',
    component: ListPaginationBar,
    tags: ['autodocs'],
} satisfies Meta<typeof ListPaginationBar>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        items:  Array.from(new Array(100),(_,index)=> `Item ${index}` ),
        pageSize: 5,
        onPageChange: (items: any) => { console.log(items) }
    },
};
