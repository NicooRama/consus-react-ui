import React from 'react'
import PropTypes from 'prop-types'
import { useEffect, useState } from 'react'
import { PaginationUtils } from '@consus/js-utils'
import { PaginationBar } from '../pagination-bar/PaginationBar'

export interface ListPaginationBarProps {
  items: any[]
  onPageChange: (items: any[], pageNumber: number) => void
  initialPage?: number
  pageSize?: number
  className?: string
  hideNavigators?: boolean
  [props: string]: any;
}

export const ListPaginationBar: React.FC<ListPaginationBarProps> = ({
  items,
  pageSize = 5,
  initialPage = 0,
  onPageChange,
  className = '',
  hideNavigators = false,
  ...props
}) => {
  const [actualPageNumber, setActualPageNumber] = useState(initialPage)
  useEffect(() => {
    changePage(actualPageNumber)
  }, [items, pageSize])

  const pageCount = PaginationUtils.pageCount(items.length, pageSize)
  const navigationClass = (className: string) =>
    PaginationUtils.areNavigationNecesary(pageCount) || hideNavigators ? 'd-none' : className
  const changePage = (pageNumber: number) => {
    const [itemsOfPage, pageSelected] = PaginationUtils.pageState(
      pageNumber,
      pageSize,
      items,
    ) as any
    setActualPageNumber(pageSelected)
    onPageChange(itemsOfPage, pageNumber)
  }

  const changePageNormalized = (selectedPageNumber: { selected: number }) =>
    changePage(selectedPageNumber.selected)

  return (
    <PaginationBar
      previousClassName={navigationClass('previous')}
      nextClassName={navigationClass('next')}
      forcePage={actualPageNumber}
      pageCount={pageCount}
      onChange={changePageNormalized}
      {...props}
    />
  )
}

ListPaginationBar.propTypes = {
  className: PropTypes.string,
}
