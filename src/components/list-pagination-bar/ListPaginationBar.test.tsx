import {ListPaginationBar} from "./ListPaginationBar";
import {render} from "../../../test/test.utils";

describe('<ListPaginationBar />', () => {
    it('renders <ListPaginationBar /> successfully', () => {
        const {getByTestId} = render(<ListPaginationBar data-testid={'list-pagination-bar'}
                                                        items={[
                                                            'Item 1',
                                                            'Item 2',
                                                            'Item 3',
                                                            'Item 4',
                                                            'Item 5',
                                                            'Item 6',
                                                            'Item 7',
                                                            'Item 8',
                                                            'Item 9',
                                                            'Item 10',
                                                            'Item 11',
                                                            'Item 12',
                                                            'Item 13',
                                                            'Item 14',
                                                            'Item 15',
                                                        ]} onPageChange={() => {}}
        />)
        expect(getByTestId('list-pagination-bar')).toBeInTheDocument();
    });
});
