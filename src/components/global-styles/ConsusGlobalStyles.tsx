import {DatePickerGlobalStyles} from "../date-picker/DatePickerGlobalStyles";
import {TypeaheadGlobalStyles} from "../input/typeahead/typeahead.styles";
import {ScrollbarStyles} from "../scrollbar/scrollbar.styles";
import {ToastGlobalStyles} from "../toast/ToastGlobalStyles";
import {GlobalStyles} from "../../styles/globals.styles";
import {ModalGlobalStyles} from "../modal/modal.styles";

export const ConsusGlobalStyles = () => {
    return (<>
        <ModalGlobalStyles />
        <GlobalStyles />
        <DatePickerGlobalStyles/>
        <TypeaheadGlobalStyles />
        <ScrollbarStyles />
        <ToastGlobalStyles />
    </>)
};
