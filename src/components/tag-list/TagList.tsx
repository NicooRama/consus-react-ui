import React, { useEffect, useState } from 'react'
import { ConsusRender } from '../../utils/types'
import { ThemeColorIndex } from '../../styles/theme.interface'
import { normalizeDisplay } from '@consus/js-utils'
import { Tag } from '../tag/Tag'
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: ${({theme}) => theme.spacing.xs}px;
`;

export interface TagListProps {
    color?: ThemeColorIndex;
    items: any[];
    render?: ConsusRender;
    className?: string;
    onChange?: (item: any) => void;
    conditionalClassName?: (item: any, index?: number) => string;
    selected?: any;
    }

export const TagList: React.FC<TagListProps> = ({
                            color = 'tag',
                            items,
                            render,
                            onChange,
                            selected,
                            //@ts-ignore
                            conditionalClassName = (item: any) => '',
                            className = "",
                            ...props
                        }) => {
    const [itemSelected, setItemSelected] = useState(selected);

    useEffect(() => {
        //FIXME: con objetos no esta funcionando el seleccionado por afuera.
        setItemSelected(selected);
    }, [selected]);

    const normalizedRender = normalizeDisplay(render);
    const isOptionSelected = (option: any) => option === itemSelected;
    const isClickeable = !!onChange;

    const onClick = (option: any) => {
        if (!isClickeable) return;
        return () => {
            const newOption: any = isOptionSelected(option) ? null : option;
            setItemSelected(newOption);
            onChange && onChange(newOption);
        }
    };

    // @ts-ignore
    return (<Container className={`tag-list ${className}`} {...props}>
        {
            items.map((item, key) => (
                <Tag key={key}
                     color={color}
                     className={conditionalClassName(item, key)}
                     onClick={onClick(item) as any}
                     active={isOptionSelected(item)}>
                    {normalizedRender(item)}
                </Tag>
            ))
        }
    </Container>)
};
