import type {Meta, StoryObj} from '@storybook/react';
import {TagList} from './TagList';
import styled from "styled-components";

const Container = styled.div`
  .tag-error {
    background-color: red;
  }

`

const meta: any = {
    title: 'Tag/TagList',
    component: TagList,
    tags: ['autodocs'],
    decorators: [
        (Story: any) => (
            <Container>
                <Story/>
            </Container>
        )]
} satisfies Meta<typeof TagList>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        items: ["Item 1", "Item 2", "Item 3", "Item 4"],
    },
};

export const ConditionalClassName: Story = {
    args: {
        items: ["Item 1", "Item 2", "Item 3", "Item 4"],
        conditionalClassName: (item: any) => item === 'Item 2' ? 'tag-error' : '',
    }
}
