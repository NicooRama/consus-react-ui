import {TagList} from "./TagList";
import {render} from "../../../test/test.utils";

describe('<TagList />', () => {
    it('renders <TagList /> successfully', () => {
        const {getByTestId} = render(<TagList data-testid={'tag-list'} items={[]}/>)
        expect(getByTestId('tag-list')).toBeInTheDocument();
    });
});
