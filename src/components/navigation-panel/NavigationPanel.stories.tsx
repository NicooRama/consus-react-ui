import type {Meta, StoryObj} from '@storybook/react';
import {NavigationPanel} from './NavigationPanel';

const meta: any = {
    title: 'Navigation/NavigationPanel',
    component: NavigationPanel,
    tags: ['autodocs'],
} satisfies Meta<typeof NavigationPanel>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        actualStep: 1,
        totalSteps: 3,
    },
};
