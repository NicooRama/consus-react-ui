import type {Meta, StoryObj} from '@storybook/react';

import {NavigationPanelSkeleton} from './NavigationPanelSkeleton';

const meta: any = {
    title: 'Navigation/NavigationPanelSkeleton',
    component: NavigationPanelSkeleton,
    tags: ['autodocs'],
} satisfies Meta<typeof NavigationPanelSkeleton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {},
};
