import {mobileMediaQuery} from "../../styles/common";
import styled from "styled-components";

export const NavigationButtonsContainer = styled.div`
  display: grid;
  grid-template-columns: 200px auto 200px;
  align-items: center;

  ${mobileMediaQuery} {
    grid-template-columns: 1fr;
    grid-gap: ${({theme}: any) => theme.spacing.xs}px;
  }
`
