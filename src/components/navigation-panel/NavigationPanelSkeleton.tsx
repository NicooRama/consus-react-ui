import {NavigationButtonsContainer} from "./NavigationButtonsContainer";
import {SkeletonButton, SkeletonText} from "../skeletons";
import styled from "styled-components";

const StyledSkeletonText = styled(SkeletonText)`
  margin: 0 auto;
  width: 100px;
`

const ButtonNextSkeleton = styled(SkeletonButton)`
    margin-left: auto;
`

export const NavigationPanelSkeleton = () => {
    return (<NavigationButtonsContainer>
        <SkeletonButton height={'44px'} width={'84px'}/>
        <StyledSkeletonText />
        <ButtonNextSkeleton height={'44px'} width={'94px'}/>
    </NavigationButtonsContainer>)
};
