import {NavigationPanel} from "./NavigationPanel";
import {render} from "../../../test/test.utils";

describe('<NavigationPanel />', () => {
    it('renders <NavigationPanel /> successfully', () => {
        const {getByTestId} = render(<NavigationPanel data-testid={'navigation-panel'} actualStep={1} isSubmitting={false} totalSteps={4}/>)
        expect(getByTestId('navigation-panel')).toBeInTheDocument();
    });
});
