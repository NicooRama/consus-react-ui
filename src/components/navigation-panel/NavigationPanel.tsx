import React from "react"
import styled from "styled-components";
import {NavigationButtonsContainer} from "./NavigationButtonsContainer";
import {Button} from "../button/Button";
import {mobileMediaQuery} from "../../styles/common";
import {LoaderWrapper} from "../loader-wrapper/LoaderWrapper";
import {Text} from "../text/Text";

const Container = styled.div`
  margin-bottom: ${({theme}: any) => theme.spacing.lg}px;
`

const PrevButton = styled(Button as any)`
  width: fit-content;

  ${mobileMediaQuery} {
    width: 100%;
  }
`

const StepsText = styled(Text as any)`
  width: 100%;
  text-align: center;
`

const NextButton = styled(Button as any)`
  width: fit-content;
  justify-self: end;

  ${mobileMediaQuery} {
    width: 100%;
  }
`

const StyledLoaderWrapper = styled(LoaderWrapper as any)`
  width: fit-content;
  margin-left: auto;

  ${mobileMediaQuery} {
    width: 100%;
  }
`

export interface NavigationPanelProps {
    showPrev?: boolean;
    prevText?: string;
    nextText?: string;
    actualStep: number;
    totalSteps: number;
    isSubmitting: boolean;
    onBack?: () => void;
    onNext?: () => void;
    }

export const NavigationPanel: React.FC<NavigationPanelProps> = ({
                                    showPrev = true,
                                    prevText = 'Anterior',
                                    nextText = 'Siguiente',
                                    actualStep,
                                    totalSteps,
                                    isSubmitting,
                                    onBack = () => {
                                    },
                                    onNext,
                                    ...props
                                }) => {
        return (<Container  {...props}>
        <NavigationButtonsContainer >
            <PrevButton onClick={onBack} disabled={!showPrev} >{prevText}</PrevButton>
            <StepsText size={'xs'} >Paso {actualStep} de {totalSteps}</StepsText>
            <StyledLoaderWrapper loading={isSubmitting} size={32}>
                <NextButton onClick={onNext} type={'submit'} >
                    {nextText}
                </NextButton>
            </StyledLoaderWrapper>
        </NavigationButtonsContainer>
    </Container>)
};
