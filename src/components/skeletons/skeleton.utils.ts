export const toDimension = (value: string | number) => {
    if(typeof value === 'number' || !isNaN(value as any)) return `${value}px`;
    return value;
}
