import styled from "styled-components";
import {SkeletonText} from "../skeleton-text/SkeletonText";
import {toDimension} from "../skeleton.utils";

export const SkeletonLabel = styled(SkeletonText as any)<{width: string}>`
  width: ${({width}) => toDimension(width)};
  margin-bottom: ${({theme}: any) => theme.spacing.xxs}px;
`
