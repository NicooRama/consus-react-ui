import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonLabel} from './SkeletonLabel';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonLabel',
    component: SkeletonLabel,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonLabel>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
