import {SkeletonLabel} from "./SkeletonLabel";
import {render} from "../../../../test/test.utils";

describe('<SkeletonLabel />', () => {
    it('renders <SkeletonLabel /> successfully', () => {
        const {getByTestId} = render(<SkeletonLabel data-testid={'skeleton-label'}/>)
        expect(getByTestId('skeleton-label')).toBeInTheDocument();
    });
});
