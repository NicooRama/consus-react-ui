import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonCardList} from './SkeletonCardList';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonCardList',
    component: SkeletonCardList,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonCardList>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
