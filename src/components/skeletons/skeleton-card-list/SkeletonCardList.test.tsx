import {SkeletonCardList} from "./SkeletonCardList";
import {render} from "../../../../test/test.utils";

describe('<SkeletonCardList />', () => {
    it('renders <SkeletonCardList /> successfully', () => {
        const {getByTestId} = render(<SkeletonCardList data-testid={'card-list-skeleton'}/>)
        expect(getByTestId('card-list-skeleton')).toBeInTheDocument();
    });
});
