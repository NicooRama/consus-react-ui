import React from "react"
import styled from "styled-components";
import {SkeletonPagination} from '../skeleton-pagination/SkeletonPagination';
import {SkeletonCard} from "../skeleton-card/SkeletonCard";

const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: ${({theme}: any) => theme.spacing.sm}px;
`

export interface CardListSkeletonProps {
    quantity?: number;
    cardHeight?: string;
    }

export const SkeletonCardList: React.FC<CardListSkeletonProps> = ({
                                     quantity = 7,
                                     cardHeight = '100px',
                                     ...props
                                 }) => {
        return (<Container  {...props}>
        {
            Array.from(Array(quantity).keys()).map((index: number) => <SkeletonCard
                                height={cardHeight}
                key={`card${index}`}/>)
        }
        <SkeletonPagination/>
    </Container>)
};
