import styled from "styled-components";
import {skeletonBackgroundAnimation} from "../skeleton.animations";
import {SkeletonLabel} from "../skeleton-label/SkeletonLabel";

export const SkeletonInput = styled.div`
    width: 100%;
  ${skeletonBackgroundAnimation};
  border-radius: ${({theme}) => theme.borderRadius}px;
  height: 42px;
`

export const SkeletonInputWithLabel = (props: any) => (
    <div {...props}>
      <SkeletonLabel width={props.labelWidth || '100%'}/>
      <SkeletonInput />
    </div>
)
