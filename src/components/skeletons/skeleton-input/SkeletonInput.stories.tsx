import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonInput} from './SkeletonInput';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonInput',
    component: SkeletonInput,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonInput>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
