import {SkeletonInput} from "./SkeletonInput";
import {render} from "../../../../test/test.utils";

describe('<SkeletonInput />', () => {
    it('renders <SkeletonInput /> successfully', () => {
        const {getByTestId} = render(<SkeletonInput data-testid={'skeleton-input'}/>)
        expect(getByTestId('skeleton-input')).toBeInTheDocument();
    });
});
