import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonCard} from './SkeletonCard';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonCard',
    component: SkeletonCard,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
