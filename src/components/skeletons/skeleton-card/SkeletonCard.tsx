import styled from "styled-components";
import {skeletonBorderAnimation} from "../skeleton.animations";
import {toDimension} from "../skeleton.utils";

export interface SkeletonCardProps {
  theme?: any,
  height?: string
}

export const SkeletonCard = styled.div<SkeletonCardProps>`
  ${skeletonBorderAnimation};
  padding: ${({theme}) => theme.spacing.md}px;
  border-radius: ${({theme}) => theme.borderRadius}px;
  border: 1px solid;
  box-shadow: 0 0 3px 0 ${({theme}) => theme.colors.boxShadow};
  height: ${({height = 'auto'}) => toDimension(height)}; 
`
