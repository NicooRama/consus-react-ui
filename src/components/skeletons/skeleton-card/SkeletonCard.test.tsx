import {SkeletonCard} from "./SkeletonCard";
import {render} from "../../../../test/test.utils";

describe('<SkeletonCard />', () => {
    it('renders <SkeletonCard /> successfully', () => {
        const {getByTestId} = render(<SkeletonCard data-testid={'skeleton-card'}/>)
        expect(getByTestId('skeleton-card')).toBeInTheDocument();
    });
});
