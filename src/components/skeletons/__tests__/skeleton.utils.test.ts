import { toDimension } from '../skeleton.utils'

describe('skeleton.utils', () => {
  describe('toDimension', () => {
    it('should return a string with px', () => {
      expect(toDimension(10)).toEqual('10px')
    })
    it('should return the same percentage string', () => {
      expect(toDimension('10%')).toEqual('10%')
    });
    it('should return the same px string', () => {
      expect(toDimension('10px')).toEqual('10px')
    });
  })
})
