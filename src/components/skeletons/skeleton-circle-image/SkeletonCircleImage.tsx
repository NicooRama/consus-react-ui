import styled from "styled-components";
import {skeletonBackgroundAnimation} from "../skeleton.animations";

export interface SkeletonCircleImageProps {
  size: number
}

export const SkeletonCircleImage = styled.div<SkeletonCircleImageProps>`
  ${skeletonBackgroundAnimation};
  width: ${({size}) => size}px;
  height: ${({size}) => size}px;
  min-width: ${({size}) => size}px;
  min-height: ${({size}) => size}px;
  border-radius: ${({size}) => size*2}px;
`
