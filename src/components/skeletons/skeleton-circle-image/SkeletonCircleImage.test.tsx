import {SkeletonCircleImage} from "./SkeletonCircleImage";
import {render} from "../../../../test/test.utils";

describe('<SkeletonCircleImage />', () => {
    it('renders <SkeletonCircleImage /> successfully', () => {
        const {getByTestId} = render(<SkeletonCircleImage data-testid={'skeleton-circle-image'} size={50}/>)
        expect(getByTestId('skeleton-circle-image')).toBeInTheDocument();
    });
});
