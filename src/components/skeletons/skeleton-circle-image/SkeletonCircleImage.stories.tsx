import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonCircleImage} from './SkeletonCircleImage';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonCircleImage',
    component: SkeletonCircleImage,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonCircleImage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        size: 50,
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
