import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonRadioButtonGroup} from './SkeletonRadioButtonGroup';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonRadioButtonGroup',
    component: SkeletonRadioButtonGroup,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonRadioButtonGroup>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
