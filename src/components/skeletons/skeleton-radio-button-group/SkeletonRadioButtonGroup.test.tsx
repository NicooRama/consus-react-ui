import {SkeletonRadioButtonGroup} from "./SkeletonRadioButtonGroup";
import {render} from "../../../../test/test.utils";

describe('<SkeletonRadioButtonGroup />', () => {
    it('renders <SkeletonRadioButtonGroup /> successfully', () => {
        const {getByTestId} = render(<SkeletonRadioButtonGroup data-testid={'skeleton-radio-button-group'}/>)
        expect(getByTestId('skeleton-radio-button-group')).toBeInTheDocument();
    });
});
