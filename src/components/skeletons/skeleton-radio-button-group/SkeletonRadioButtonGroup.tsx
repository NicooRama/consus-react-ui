import React from 'react'
import styled from 'styled-components'
import { mobileMediaQuery } from '../../../styles/common'
import { SkeletonButton } from '../skeleton-button/SkeletonButton'

const Container = styled.div`
  display: flex;
  gap: ${({ theme }: any) => theme.spacing.xs}px;
  flex-wrap: wrap;

  ${mobileMediaQuery} {
    div {
      width: 100%;
    }
  }
`

export interface SkeletonRadioButtonGroupProps {
  buttonWidth?: string
  quantity?: number
}

export const SkeletonRadioButtonGroup: React.FC<SkeletonRadioButtonGroupProps> = ({
  buttonWidth = '125px',
  quantity = 5,
  ...props
}) => {
  return (
    <Container {...props}>
      {Array.from(Array(quantity).keys()).map((index: number) => (
        <SkeletonButton width={buttonWidth} key={`button${index}`} />
      ))}
    </Container>
  )
}
