import {SkeletonLoader} from "./SkeletonLoader";
import {render} from "../../../../test/test.utils";

describe('<SkeletonLoader />', () => {
    it('renders <SkeletonLoader /> successfully', () => {
        const {getByTestId} = render(<SkeletonLoader data-testid={'skeleton-loader'} loading={true} skeleton={<div data-testid={'skeleton'}>skeleton</div>}>Content</SkeletonLoader>)
        expect(getByTestId('skeleton')).toBeInTheDocument();
    });
});
