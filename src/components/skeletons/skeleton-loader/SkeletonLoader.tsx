import React, { ReactElement } from 'react';

interface SkeletonLoaderProps {
    loading: boolean;
    skeleton: ReactElement;
    children: any;
}

export const SkeletonLoader: React.FC<SkeletonLoaderProps> = ({loading, skeleton, children}) => (
    loading ? skeleton : children
)
