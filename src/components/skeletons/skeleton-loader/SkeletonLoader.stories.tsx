import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonLoader} from './SkeletonLoader';
import {SkeletonText} from "../skeleton-text/SkeletonText";
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonLoader',
    component: SkeletonLoader,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonLoader>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        loading: true,
        skeleton: <SkeletonText />,
        children: 'Contenido'
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
