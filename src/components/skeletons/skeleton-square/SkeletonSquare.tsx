import styled from "styled-components";
import {skeletonBackgroundAnimation} from "../skeleton.animations";
import {toDimension} from "../skeleton.utils";

export interface SkeletonSquareProps {
  width?: string | number;
  height?: string | number;
}

export const SkeletonSquare = styled.div<SkeletonSquareProps>`
  ${skeletonBackgroundAnimation};
  border-radius: ${({theme}) => theme.borderRadius}px;
  width: ${({width = '100%'}) => toDimension(width)};
  height: ${({height = '100%'}) => toDimension(height)};
`
