import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonSquare} from './SkeletonSquare';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonSquare',
    component: SkeletonSquare,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonSquare>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        width: '150px',
        height: '150px',
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
