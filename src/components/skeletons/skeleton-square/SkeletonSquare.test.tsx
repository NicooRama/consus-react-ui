import {SkeletonSquare} from "./SkeletonSquare";
import {render} from "../../../../test/test.utils";

describe('<SkeletonSquare />', () => {
    it('renders <SkeletonSquare /> successfully', () => {
        const {getByTestId} = render(<SkeletonSquare data-testid={'skeleton-square'} width={'140px'} height={'140px'}/>)
        expect(getByTestId('skeleton-square')).toBeInTheDocument();
    });
});
