import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonText} from './SkeletonText';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonText',
    component: SkeletonText,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonText>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
