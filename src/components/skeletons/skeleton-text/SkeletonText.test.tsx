import {SkeletonText} from "./SkeletonText";
import {render} from "../../../../test/test.utils";

describe('<SkeletonText />', () => {
    it('renders <SkeletonText /> successfully', () => {
        const {getByTestId} = render(<SkeletonText data-testid={'skeleton-text'}/>)
        expect(getByTestId('skeleton-text')).toBeInTheDocument();
    });
});
