import styled from "styled-components";
import {skeletonBackgroundAnimation} from "../skeleton.animations";
import {FontSize} from "../../../styles/theme.interface";
import {toDimension} from "../skeleton.utils";

export interface SkeletonTextProps {
  width?: string | number;
  size?: FontSize;
}

export const SkeletonText = styled.div<SkeletonTextProps>`
  ${skeletonBackgroundAnimation};
  width: ${({width = '100%'}: any) => toDimension(width)};
  height: ${({theme, size = 'sm'}: any) => theme.font.size[size] + 3}px;
  border-radius: ${({theme}: any) => theme.borderRadius}px;
`
