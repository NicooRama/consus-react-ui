import {css, keyframes} from "styled-components";

export const skeletonBackgroundLoading = keyframes`
  0% {
    background-color: hsl(200, 20%, 70%);
  }
  100% {
    background-color: hsl(200, 20%, 95%);
  }
`

export const borderSkeletonLoading = keyframes`
  0% {
    border-color: hsl(200, 20%, 70%);
  }
  100% {
    border-color: hsl(200, 20%, 95%);
  }
`

export const skeletonBackgroundAnimation = css`
  opacity: 0.7;
  animation: ${skeletonBackgroundLoading} 1s linear infinite alternate;
`

export const skeletonBorderAnimation = css`
  opacity: 0.7;
  animation: ${borderSkeletonLoading} 1s linear infinite alternate;
`
