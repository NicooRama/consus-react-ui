import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonButton} from './SkeletonButton';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonButton',
    component: SkeletonButton,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        width: '150px'
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
