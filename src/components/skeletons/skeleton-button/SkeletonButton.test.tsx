import {SkeletonButton} from "./SkeletonButton";
import {render} from "../../../../test/test.utils";

describe('<SkeletonButton />', () => {
    it('renders <SkeletonButton /> successfully', () => {
        const {getByTestId} = render(<SkeletonButton data-testid={'skeleton-button'} width={'150px'}/>)
        expect(getByTestId('skeleton-button')).toBeInTheDocument();
    });
});
