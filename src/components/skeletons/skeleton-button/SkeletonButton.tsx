import styled from "styled-components";
import {skeletonBackgroundAnimation} from "../skeleton.animations";
import {toDimension} from "../skeleton.utils";

export interface SkeletonButtonProps {
  width?: string | number;
  height?: string | number;
}

export const SkeletonButton = styled.div<SkeletonButtonProps>`
  width: ${({width = '100%'}) => toDimension(width)};
  ${skeletonBackgroundAnimation};
  border-radius: ${({theme}: any) => theme.borderRadius}px;
  height: ${({height = '40px'}) => toDimension(height)};
`
