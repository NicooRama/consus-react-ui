import styled from "styled-components";
import {skeletonBackgroundAnimation} from "../skeleton.animations";

export const SkeletonPagination = styled.div`
  ${skeletonBackgroundAnimation};
  width: 350px;
  height: 30px;
  border-radius: ${({theme}) => theme.borderRadius}px;
`
