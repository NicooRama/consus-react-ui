import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonPagination} from './SkeletonPagination';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonPagination',
    component: SkeletonPagination,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonPagination>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
