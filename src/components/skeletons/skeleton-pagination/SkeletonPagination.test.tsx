import {SkeletonPagination} from "./SkeletonPagination";
import {render} from "../../../../test/test.utils";

describe('<SkeletonPagination />', () => {
    it('renders <SkeletonPagination /> successfully', () => {
        const {getByTestId} = render(<SkeletonPagination data-testid={'skeleton-pagination'}/>)
        expect(getByTestId('skeleton-pagination')).toBeInTheDocument();
    });
});
