import {SkeletonSquareCard} from "./SkeletonSquareCard";
import {render} from "../../../../test/test.utils";

describe('<SkeletonSquareCard />', () => {
    it('renders <SkeletonSquareCard /> successfully', () => {
        const {getByTestId} = render(<SkeletonSquareCard data-testid={'skeleton-square-card'} height={'100px'}/>)
        expect(getByTestId('skeleton-square-card')).toBeInTheDocument();
    });
});
