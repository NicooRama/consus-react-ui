import React from "react"
import styled from "styled-components";
import {SkeletonSquare} from "../skeleton-square/SkeletonSquare";
import {SkeletonCard} from "../skeleton-card/SkeletonCard";


const StyledSkeletonCard = styled(SkeletonCard)`
    padding: 0;
`

export interface SkeletonSquareCardProps {
    height: string;
    }

export const SkeletonSquareCard: React.FC<SkeletonSquareCardProps> = ({height, ...props}) => {

    return (<StyledSkeletonCard height={height}  {...props}>
        <SkeletonSquare />
    </StyledSkeletonCard>)
};
