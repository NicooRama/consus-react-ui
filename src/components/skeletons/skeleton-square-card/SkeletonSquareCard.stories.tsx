import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonSquareCard} from './SkeletonSquareCard';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonSquareCard',
    component: SkeletonSquareCard,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonSquareCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        height: '100px'
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
