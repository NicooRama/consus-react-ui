import {SkeletonList} from "./SkeletonList";
import {render} from "../../../../test/test.utils";

describe('<SkeletonList />', () => {
    it('renders <SkeletonList /> successfully', () => {
        const {getByTestId} = render(<SkeletonList data-testid={'skeleton-list'} cardWidth={'85px'}/>)
        expect(getByTestId('skeleton-list')).toBeInTheDocument();
    });
});
