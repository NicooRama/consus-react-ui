import React from "react"
import styled from "styled-components";
import {SkeletonPagination} from '../skeleton-pagination/SkeletonPagination';
import {SkeletonCardList} from '../skeleton-card-list/SkeletonCardList';
import {SkeletonInput} from '../skeleton-input/SkeletonInput';

const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: ${({theme}: any) => theme.spacing.sm}px;
  overflow: auto;
  max-width: 100%;
`

export interface SkeletonListProps {
    cardQuantity?: number;
    cardWidth: string;
    showPagination?: boolean;
    showInput?: boolean;

    }

export const SkeletonList: React.FC<SkeletonListProps> = ({
                                 cardQuantity = 8,
                                 cardWidth,
                                 showPagination = true,
                                 showInput = false,
                                 ...props
                             }) => {

    return (<Container  {...props}>
        {
            showInput && <SkeletonInput />
        }
        <SkeletonCardList quantity={cardQuantity} cardHeight={cardWidth} />
        {
            showPagination && <SkeletonPagination />
        }
    </Container>)
};
