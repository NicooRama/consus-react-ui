import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonList} from './SkeletonList';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonList',
    component: SkeletonList,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonList>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        cardWidth: '85px'
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
