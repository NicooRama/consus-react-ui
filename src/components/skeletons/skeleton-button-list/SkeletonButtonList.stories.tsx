import type {Meta, StoryObj} from '@storybook/react';
import {SkeletonButtonList} from './SkeletonButtonList';
import {disableDummyArgTypes} from "../../../storybook.utils";

const meta: any = {
    title: 'Skeletons/SkeletonButtonList',
    component: SkeletonButtonList,
    tags: ['autodocs'],
} satisfies Meta<typeof SkeletonButtonList>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        quantity: 4,
        width: '50px',
        height: '50px',
    },
    argTypes: {
        ...disableDummyArgTypes
    }
};
