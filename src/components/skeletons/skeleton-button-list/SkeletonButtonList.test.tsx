import {SkeletonButtonList} from "./SkeletonButtonList";
import {render} from "../../../../test/test.utils";

describe('<SkeletonButtonList />', () => {
    it('renders <SkeletonButtonList /> successfully', () => {
        const {getByTestId} = render(<SkeletonButtonList data-testid={'skeleton-button-list'} quantity={4} height={'150px'} width={'150px'}/>)
        expect(getByTestId('skeleton-button-list')).toBeInTheDocument();
    });
});
