import React from "react"
import styled from "styled-components";
import {SkeletonButton} from "../skeleton-button/SkeletonButton";


const Container = styled.div`
  display: flex;
  gap: ${({theme}) => theme.spacing.xs}px;
  flex-wrap: wrap;

`

export interface SkeletonButtonListProps {
    quantity: number,
    className?: string,
    width?: string,
    height?: string,
    }

export const SkeletonButtonList: React.FC<SkeletonButtonListProps> = ({
                                       quantity,
                                       width = '100%',
                                       height='40px',
                                       className = "",
                                       ...props}) => {
    return (<Container className={className} {...props}>
        {
            Array.from(Array(quantity).keys()).map((index: number) => <SkeletonButton key={`skeletonButtonList-${index}`} width={width} height={height} />)
        }
    </Container>)
};
