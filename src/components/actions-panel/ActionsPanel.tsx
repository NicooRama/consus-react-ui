import React from 'react';
import styled from 'styled-components';
import {Link} from "react-router-dom";
import {faEdit, faEye, faTrashAlt} from "@fortawesome/free-regular-svg-icons";
import {IconButton} from "../icon-button/IconButton";
import {ConfirmationPopoverButton} from "../popover/ConfirmationPopoverButton";

const Container = styled.span`
  display: flex;
  place-content: center;
  gap: ${({theme}: any) => theme.spacing.xs}px;

  * {
    padding: 0 !important;
  }
`

export interface ActionsPanelProps {
    detailsUrl: string;
    editUrl: string;
    confirmationMessage: string;
    onRemove: () => Promise<void>;
    [props: string]: any;
}

export const ActionsPanel: React.FC<ActionsPanelProps> = ({
                                                              detailsUrl,
                                                              editUrl,
                                                              confirmationMessage,
                                                              onRemove,
                                                              ...props
                                                          }) => {
    return (<Container {...props}>
            <IconButton tag={Link}
                        to={detailsUrl}
                        title={'Ver detalle'}
                        color={'gray.black'}
                        icon={faEye}
                        onClick={() => {
                        }}
                        variant={'tertiary'}
            />
            <IconButton tag={Link}
                        to={editUrl}
                        title={'Editar'}
                        color={'warn.dark'}
                        icon={faEdit}
                        onClick={() => {
                        }}
                        variant={'tertiary'}
            />
            <ConfirmationPopoverButton message={confirmationMessage}
                                       onAccept={onRemove}
            >
                {
                    ({onClick}: any) => <IconButton icon={faTrashAlt}
                                                    onClick={onClick}
                                                    variant={'tertiary'}
                                                    color={'error.normal'}
                    />
                }
            </ConfirmationPopoverButton>
    </Container>)
};
;
