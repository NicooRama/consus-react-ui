import type {Meta, StoryObj} from '@storybook/react';
import {ActionsPanel} from './ActionsPanel';
import {routerDecorator} from "../../storybook.utils";

const meta: any = {
    title: 'Commons/ActionsPanel',
    component: ActionsPanel,
    tags: ['autodocs'],
} satisfies Meta<typeof ActionsPanel>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        detailsUrl: '/details',
        editUrl: '/edit',
        confirmationMessage: '¿Está seguro que desea eliminar esto?',
        onRemove: async () => {},
    },
    decorators: [routerDecorator]
};
