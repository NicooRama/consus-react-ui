import {ActionsPanel} from "./ActionsPanel";
import {renderWithRouter as render} from "../../../test/test.utils";

describe('<ActionsPanel />', () => {
    it('renders <ActionsPanel /> successfully', () => {
        const {getByTestId} = render(<ActionsPanel
            confirmationMessage={'¿Estas seguro que queres eliminar el producto?'}
            onRemove={async () => {
            }}
            detailsUrl={'/details'}
            editUrl={'/edit'}
            data-testid={'actions-panel'}
        />)
        expect(getByTestId('actions-panel')).toBeInTheDocument();
    });
});
