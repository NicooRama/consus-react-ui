import type {Meta, StoryObj} from '@storybook/react';
import {RadioOptionsSlider} from './RadioOptionsSlider';
import {OptionsSliderRender} from "../optionsSlider.types";

const meta: any = {
    title: 'Commons/RadioOptionsSlider',
    component: RadioOptionsSlider,
    tags: ['autodocs'],
} satisfies Meta<typeof RadioOptionsSlider>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        options: ['Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5'],
        onChange: (option: any) => console.log(option),
        render: ({option, active, onClick}: OptionsSliderRender) => (
            <div onClick={onClick}>{option} {active ? 'ACTIVE' : ''}</div>),
    },
};
