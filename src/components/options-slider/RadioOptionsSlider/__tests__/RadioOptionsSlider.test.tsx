import { RadioOptionsSlider } from "../RadioOptionsSlider";
import {render} from "@test/test.utils";
import {OptionsSliderRender} from "../../optionsSlider.types";

describe('<RadioOptionsSlider />', () => {
    it('renders <RadioOptionsSlider /> successfully', () => {
        const {getByTestId} = render(<RadioOptionsSlider
            options={['Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5']}
            onChange={jest.fn()}
            render={({option, active, onClick}: OptionsSliderRender) => (<div onClick={onClick}>{option} {active ? 'ACTIVE' : ''}</div>)}
            data-testid={'radio-options-slider'}
        />)
        expect(getByTestId('radio-options-slider')).toBeInTheDocument();
    });
});
