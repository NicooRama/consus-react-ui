import React, {ReactElement} from 'react';
import {OptionsSliderContainer} from "../OptionsSliderContainer";
import {useSelectOption} from "../../../hooks/useSelectOption";
import {OptionsSliderRender} from "../optionsSlider.types";

export interface RadioOptionsSliderProps {
    options: any[];
    selected?: any;
    comparator?: (option: any) => ((selected: any) => boolean);
    onChange: (option: any) => void;
    withScroll?: boolean;
    "data-testid"?: string;
    render: ({option, active, onClick}: OptionsSliderRender) => ReactElement;
}

export const RadioOptionsSlider = ({
                                       options,
                                       comparator,
                                       selected,
                                       onChange,
                                       withScroll = true,
                                       render,
                                       ...props
                                   }: RadioOptionsSliderProps) => {
    const {handleChange, isSelected} = useSelectOption(options, selected, onChange, comparator);
    return (<OptionsSliderContainer data-testid={"radio-options-selector"}
                                    withScroll={withScroll}
                                    {...props}>
        {
            options.map(((option, i) => render({option, active: isSelected(option), onClick: () => handleChange(i)})))
        }
    </OptionsSliderContainer>)
};
