import styled, {css} from "styled-components";
import {FlexContainer, FlexContainerProps} from "../CssComponents/FlexContainer/FlexContainer";
import {swiperScrollBar} from "../../styles/css.utils";

export interface OptionsSliderContainerProps extends FlexContainerProps {
    withScroll: boolean
}

export const OptionsSliderContainer = styled(FlexContainer)<OptionsSliderContainerProps>`
    ${({theme, withScroll}: any) =>
            withScroll &&
            css`
                ${swiperScrollBar(theme)};
            `}
`
