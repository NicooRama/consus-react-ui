export type OptionsSliderRender = {
    option: any,
    active: boolean,
    onClick: () => void;
}
