import type {Meta, StoryObj} from '@storybook/react';
import { CheckOptionsSlider } from './CheckOptionsSlider';
import {OptionsSliderRender} from "../optionsSlider.types";

const meta: any = {
    title: 'Commons/CheckOptionsSlider',
    component: CheckOptionsSlider,
    tags: ['autodocs'],
} satisfies Meta<typeof CheckOptionsSlider>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        options: ['Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5'],
        onChange: (options: any[]) => console.log(options),
        render: ({option, active, onClick}: OptionsSliderRender) => (
            <div onClick={onClick}>{option} {active ? 'ACTIVE' : ''}</div>),
    },
};

