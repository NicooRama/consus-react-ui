import { CheckOptionsSlider } from "../CheckOptionsSlider";
import {render} from "@test/test.utils";
import {OptionsSliderRender} from "../../optionsSlider.types";

describe('<CheckOptionsSlider />', () => {
    it('renders <CheckOptionsSlider /> successfully', () => {
        const {getByTestId} = render(<CheckOptionsSlider
            options={['Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5']}
            onChange={jest.fn()}
            render={({option, active, onClick}: OptionsSliderRender) => (<div onClick={onClick}>{option} {active ? 'ACTIVE' : ''}</div>)}
            data-testid={'check-options-slider'}/>)
        expect(getByTestId('check-options-slider')).toBeInTheDocument();
    });
});
