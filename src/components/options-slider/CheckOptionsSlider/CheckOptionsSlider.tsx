import React, {ReactElement} from 'react';
import {OptionsSliderContainer} from "../OptionsSliderContainer";
import {useSelectOptions} from "../../../hooks/useSelectOptions";

type Render = {
    option: any,
    active: boolean,
    onClick: () => void;
}

export interface CheckOptionsSliderProps {
    options: any[];
    selecteds?: any[];
    comparator?: (option: any) => ((selected: any) => boolean);
    onChange: (options: any[]) => void;
    withScroll?: boolean;
    "data-testid"?: string;
    render: ({option, active, onClick}: Render) => ReactElement;
}

export const CheckOptionsSlider = ({
                                       options,
                                       comparator,
                                       selecteds = [],
                                       onChange,
                                       withScroll = true,
                                       render,
                                       ...props
                                   }: CheckOptionsSliderProps) => {
    const {handleChange, isSelected} = useSelectOptions(options, selecteds, onChange, comparator);

    return (<OptionsSliderContainer data-testid={"shop-check-options"}
                                    withScroll={withScroll}
                                    {...props}>
        {
            options.map(((option, i) => render({option, active: isSelected(option), onClick: () => handleChange(i)})))
        }
    </OptionsSliderContainer>)
};
