import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components";

const Component = styled.div`
    height: auto;
    background-color: transparent;
    width: 4px;
    margin-left: 4px;
    margin-bottom: 4px;
    margin-top: 4px;
    border-radius: 50px;
    &.active {
        background-color: ${({theme}: any) => theme.colors.primary.normal}
    }
`

export interface ActiveIndicatorProps {
    active?: boolean;
    className?: string;
}

export const ActiveIndicator: React.FC<ActiveIndicatorProps> = ({active = false, className = "", ...props}) => {
    return (<Component className={`${active ? 'active' : ''} ${className}`}
                 data-testid={'active-indicator-test'} {...props}>
    </Component>)
};

ActiveIndicator.propTypes = {
    className: PropTypes.string,
};
