import type {Meta, StoryObj} from '@storybook/react';
import {ActiveIndicator} from './ActiveIndicator';

const meta: any = {
    title: 'Commons/ActiveIndicator',
    component: ActiveIndicator,
    tags: ['autodocs'],
} satisfies Meta<typeof ActiveIndicator>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        active: true,
    },
    decorators: [
        (Story: any) => <div style={{height: 50, width: 25, display: 'flex'}}>
            <Story />
        </div>
    ]
};
