import {ActiveIndicator} from "./ActiveIndicator";
import {render} from "../../../test/test.utils";

describe('<ActiveIndicator />', () => {
    it('renders <ActiveIndicator /> successfully', () => {
        const {getByTestId} = render(<ActiveIndicator data-testid={'active-indicator'}/>)
        expect(getByTestId('active-indicator')).toBeInTheDocument();
    });
});

