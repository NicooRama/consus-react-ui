import type {Meta, StoryObj} from '@storybook/react';
import {Loader} from './Loader';

const meta: any = {
    title: 'Loader/Loader',
    component: Loader,
    tags: ['autodocs'],
} satisfies Meta<typeof Loader>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
};

export const Small: Story = {
    args: {
        size: 16
    },
};

export const Medium: Story = {
    args: {
        size: 32
    },
};

export const Large: Story = {
    args: {
        size: 40
    },
};
