import {Loader} from "./Loader";
import {render} from "../../../test/test.utils";

describe('<Loader />', () => {
    it('renders <Loader /> successfully', () => {
        const {getByTestId} = render(<Loader data-testid={'loader'}/>)
        expect(getByTestId('loader')).toBeInTheDocument();
    });
});
