import React from 'react'
import styled from "styled-components";

const Component = styled.div`
  .loader {
    border: 4px solid rgba(0, 0, 0, 0.1);
    border-radius: 50%;
    border-left-color: ${({theme}) => theme.colors.primary.normal};
    animation: spin 1s ease infinite;
    @keyframes spin {
      0% {
        transform: rotate(0deg);
      }
      100% {
        transform: rotate(360deg);
      }
    }
  }
  &.center {
    width: 100%;
    display: flex;
    justify-content: center;
  }
`

export interface LoaderProps {
  size?: number
  center?: boolean
  className?: string
}

export const Loader: React.FC<LoaderProps> = ({
  size = 36,
  center = true,
  className = '',
  ...props
}) => {
  return (
    <Component className={`${className} ${center ? 'center' : ''}`} {...props}>
      <div className={'loader'} style={{ width: size, height: size }} />
    </Component>
  )
}
