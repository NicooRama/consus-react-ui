import React from "react"
import PropTypes from 'prop-types';

import {faTrashAlt} from "@fortawesome/free-regular-svg-icons";
import { mobileMediaQuery } from '../../styles/common';
import { ConfirmationPopoverButton } from '../popover/ConfirmationPopoverButton';
import { IconButton } from '../icon-button/IconButton';
import {ListCard} from "../list-card/ListCard";
import styled from "styled-components";

const Container = styled(ListCard as any)`
  cursor: pointer;
  
  &:hover {
    &  > div {
      .confirmation-popover {
        .entity-delete-button {
          visibility: visible;
          opacity: 1
        }
      }
    }
  }
`

const Content = styled.div`
  min-height: 38px;
  display: flex;
  align-items: center;
  gap ${({theme}: any) => theme.spacing.sm}px;
  
  .confirmation-popover {
    margin-left: auto;
    .entity-delete-button {
      height: fit-content;
      visibility: hidden;
      opacity: 0;
      transition: visibility 0s, opacity 0.4s linear;
      &.circle {
        padding-top: ${({theme}: any) => theme.spacing.sm -1}px;
        padding-bottom: ${({theme}: any) => theme.spacing.sm -1}px;
        padding-left: ${({theme}: any) => theme.spacing.sm}px;
        padding-right: ${({theme}: any) => theme.spacing.sm}px;
      }
      &:focus {
        visibility: visible;
        opacity: 1;
      }
      ${mobileMediaQuery} {
        visibility:  hidden;
        opacity: 1;
      }
    }
  }
`

export interface EntityListCardProps {
    children: any;
    confirmationRemoveMessage?: string;
    onClickRemove?: (event: any) => void;
    className?: string;
    [props: string]: any;
    }

export const EntityListCard: React.FC<EntityListCardProps> = ({
                                   children,
                                   confirmationRemoveMessage = '¿Estas seguro que queres eliminar?',
                                   onClickRemove,
                                   className = "",
                                   ...props
                               }) => {
        const handleRemove = async (event: any) => {
        !!onClickRemove && await onClickRemove(event);
    }

    // @ts-ignore
    return (<Container  className={className} {...props}>
        <Content >
            <div>
                {children}
            </div>
            {
                !!onClickRemove && <ConfirmationPopoverButton message={confirmationRemoveMessage}
                                                              onAccept={handleRemove as any}
                                                              className={'confirmation-popover'}
                >
                    {
                        ({onClick}) => <IconButton
                            circle
                            color="error.normal"
                            icon={faTrashAlt}
                            onClick={onClick}
                            variant="secondary"
                            className={'entity-delete-button'}
                        />
                    }
                </ConfirmationPopoverButton>
            }
        </Content>
    </Container>)
};

EntityListCard.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    onClickRemove: PropTypes.func,
};
