import {EntityListCard} from "./EntityListCard";
import {render} from "../../../test/test.utils";

describe('<EntityListCard />', () => {
    it('renders <EntityListCard /> successfully', () => {
        const {getByTestId} = render(<EntityListCard data-testid={'entity-list-card'}>Card</EntityListCard>)
        expect(getByTestId('entity-list-card')).toBeInTheDocument();
    });
});
