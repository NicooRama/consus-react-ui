import type {Meta, StoryObj} from '@storybook/react';
import {EntityListCard} from './EntityListCard';

const meta: any = {
    title: 'Card/EntityListCard',
    component: EntityListCard,
    tags: ['autodocs'],
} satisfies Meta<typeof EntityListCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: 'Entity',
        active: false
    },
    argTypes: {
        confirmationRemoveMessage: {
            control: {
                type: 'text'
            }
        },
        active: {
            control: {
                type: 'boolean'
            }
        },
        theme: {
            table: {
                disable: true,
            },
        },
    }
};
