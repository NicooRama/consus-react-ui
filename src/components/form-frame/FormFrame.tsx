import React from 'react'
import styled from 'styled-components'
import { ThemeColorIndex } from '../../styles/theme.interface'
import { Card } from '../card/Card'
import { Icon } from '../icon/Icon'
import { getIn } from '../../utils/utils'

const Container = styled.div`
  position: relative;
  padding-top: 40px;
`

const StyledCard = styled(Card)`
  &.form-frame-card {
    width: auto;
    max-width: 425px;
    display: block;
    margin: 0 auto;
    padding-top: calc(30px + ${({ theme }: any) => theme.spacing.md}px);
  }
`

const CircleIconContainer = styled.div<{ color: ThemeColorIndex }>`
  display: flex;
  z-index: 1;
  justify-content: center;
  border: ${({ theme }: any) => theme.border};
  border-radius: 80px;
  position: absolute;
  top: 0;
  left: calc(50% - 40px);
  background-color: white;
  height: 80px;
  width: 80px;
  min-width: 80px;

  svg {
    align-self: center;

    path {
      fill: ${({ theme, color }: any) => getIn(theme.colors, color)};
    }
  }
`

export interface FormFrameProps {
  icon: { viewBox: string; path: string }
  children: any
  className?: string
  color?: ThemeColorIndex
}

export const FormFrame: React.FC<FormFrameProps> = ({
  icon,
  color = 'primary.normal',
  children,
  className = '',
  ...props
}) => {
  return (
    <Container className={className} {...props}>
      <CircleIconContainer className={'circle-icon'} color={color}>
        <Icon data-testid={'icon'} icon={icon} height={40} width={40} />
      </CircleIconContainer>
      <StyledCard className={'form-frame-card'}>{children}</StyledCard>
    </Container>
  )
}
