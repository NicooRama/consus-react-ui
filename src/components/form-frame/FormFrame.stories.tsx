import type {Meta, StoryObj} from '@storybook/react';
import {FormFrame} from './FormFrame';
import {fontAwesomeToIcon} from "../icon/icon.utils";
import {faUser} from "@fortawesome/free-solid-svg-icons";

const meta: any = {
    title: 'Commons/FormFrame',
    component: FormFrame,
    tags: ['autodocs'],
} satisfies Meta<typeof FormFrame>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: <div>
            <div>I'm</div>
            <div>a</div>
            <div>content</div>
        </div>,
        icon: fontAwesomeToIcon(faUser)
    },
};
