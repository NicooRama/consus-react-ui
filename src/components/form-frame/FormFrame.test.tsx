import {FormFrame} from "./FormFrame";
import {render} from "../../../test/test.utils";
import {screen} from "@testing-library/react";
import {fontAwesomeToIcon} from "../icon/icon.utils";
import {faUser} from "@fortawesome/free-solid-svg-icons";

describe('<FormFrame />', () => {
    it('renders FormFrame successfully', () => {
        render(<FormFrame icon={fontAwesomeToIcon(faUser)}>I'm a form frame</FormFrame>)
    })

    it('renders children successfully', () => {
        render(<FormFrame icon={fontAwesomeToIcon(faUser)}>I'm a form frame</FormFrame>)
        expect(screen.getByText('I\'m a form frame')).toBeInTheDocument();
    })

    it('renders icon successfully', () => {
        render(<FormFrame icon={fontAwesomeToIcon(faUser)}>I'm a form frame</FormFrame>)
        expect(screen.getByTestId('icon')).toBeInTheDocument();
    })
});
