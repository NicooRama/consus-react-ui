import type {Meta, StoryObj} from '@storybook/react';
import { BackofficeTagList } from './BackofficeTagList';

const meta: any = {
    title: 'Backoffice/BackofficeTagList',
    component: BackofficeTagList,
    tags: ['autodocs'],
} satisfies Meta<typeof BackofficeTagList>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        items: ['Item 1', 'Item 2', 'Item 3']
    },
};
