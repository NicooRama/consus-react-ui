import styled from "styled-components";

export const BackofficeFilterPanel = styled.div`
  display: grid;
  grid-gap: ${({theme}: any) => theme.spacing.sm}px;
  grid-template-columns: 1fr;
`

export const BackofficeContainer = styled.div`
  display: grid;
  grid-gap: ${({theme}: any) => theme.spacing.sm}px;
  grid-template-columns: 1fr;
`

export const BackofficeFooter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`
