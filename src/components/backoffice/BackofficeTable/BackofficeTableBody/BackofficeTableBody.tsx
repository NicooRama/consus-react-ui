import React from 'react';


import styled from 'styled-components';

const Container = styled.div`
`

export interface BackofficeTableBodyProps {
    rows: any[];
    prepareRow: any;
    tableBodyProps: any;
    "data-testid"?: string;
}

export const BackofficeTableBody = ({rows, prepareRow, tableBodyProps, ...props}: BackofficeTableBodyProps) => {
    return (<tbody {...tableBodyProps} data-testid={'backoffice-table-body'} {...props}>
    {rows.map((row: any, i: number) => {
        prepareRow(row)
        return (
            <tr {...row.getRowProps()} key={`row-${i}`}>
                {row.cells.map((cell: any) => {
                    return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                })}
            </tr>
        )
    })}
    </tbody>)
};
