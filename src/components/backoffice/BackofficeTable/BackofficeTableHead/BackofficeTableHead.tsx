import React from 'react';


import styled from 'styled-components';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSortDown, faSortUp} from "@fortawesome/free-solid-svg-icons";

const Th = styled.th<{ columnAlign: string, columnWidth: string }>`
  span + span {
    margin-left: ${({theme}: any) => theme.spacing.xxs}px;
  }
;

  &.backoffice-th {
    width: ${({columnWidth}) => columnWidth || 'auto'};
    text-align: ${({columnAlign}) => columnAlign || 'center'};
  }
`

export interface BackofficeTableHeadProps {
    headerGroups: any[];
    "data-testid"?: string;
}

export const BackofficeTableHead = ({headerGroups, ...props}: BackofficeTableHeadProps) => {
    return (<thead data-testid={"backoffice-table-head"} {...props}>
    <tr>
        {headerGroups[0].headers.map((column: any) => {
            return (
                <Th className={'backoffice-th'} key={column.accessor} columnAlign={column.align}
                    columnWidth={column.width}>
                                <span>
                                {column.render('Header')}
                                    </span>
                    <span>
                                    {column.isSorted
                                        ? column.isSortedDesc
                                            ? <FontAwesomeIcon icon={faSortDown}/>
                                            : <FontAwesomeIcon icon={faSortUp}/>
                                        : ''}
                                  </span>
                </Th>
            )
        })}
    </tr>
    </thead>)
};
