import { BackofficeTable } from "../BackofficeTable";
import {render} from "@test/test.utils";

describe('<BackofficeTable />', () => {
    const data = [
        {
            name: 'Fulano',
            lastname: 'De Tal',
            age: 30,
        },
        {
            name: 'Mengano',
            lastname: 'De Tal',
            age: 30,
        },
        {
            name: 'Sutano',
            lastname: 'De Tal',
            age: 30,
        },
    ];

    const columns = [
        {
            Header: 'Nombre',
            accessor: 'name',
        },
        {
            Header: 'Apellido',
            accessor: 'lastname',
        },
        {
            Header: 'Edad',
            accessor: 'age',
        }
    ]

    it('renders <BackofficeTable /> successfully', () => {
        const {getByTestId} = render(<BackofficeTable data={data} columns={columns} data-testid={'backoffice-table'}/>)
        expect(getByTestId('backoffice-table')).toBeInTheDocument();
    });
});
