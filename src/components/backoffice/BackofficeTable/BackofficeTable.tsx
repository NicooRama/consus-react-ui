import React from 'react';
import {useTable} from "react-table";
import styled from 'styled-components';
import {BackofficeTableHead} from "./BackofficeTableHead/BackofficeTableHead";
import {BackofficeTableBody} from "./BackofficeTableBody/BackofficeTableBody";
import {TableContainer} from "../../table/Table";

const StyledTableContainer = styled(TableContainer)`
  background-color: white;
`

export interface BackofficeTableProps {
    data: any[];
    columns: any[];
    "data-testid"?: string;
}

export const BackofficeTable = ({data, columns, ...props}:BackofficeTableProps) => {
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        rows,
    } = useTable(
        {
            columns,
            data,
        },
    )

    return (<StyledTableContainer data-testid={'backoffice-table'} {...props}>
        <table {...getTableProps()}>
            <BackofficeTableHead headerGroups={headerGroups}/>
            <BackofficeTableBody rows={rows} prepareRow={prepareRow} tableBodyProps={getTableBodyProps()}/>
        </table>
    </StyledTableContainer>)
};
