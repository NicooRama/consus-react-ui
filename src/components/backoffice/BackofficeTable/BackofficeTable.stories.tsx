import type {Meta, StoryObj} from '@storybook/react';
import { BackofficeTable } from './BackofficeTable';
import {backofficeTableColumns, backofficeTableData} from "../backoffice.examples";

const meta: any = {
    title: 'Backoffice/BackofficeTable',
    component: BackofficeTable,
    tags: ['autodocs'],
} satisfies Meta<typeof BackofficeTable>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        data: backofficeTableData,
        columns: backofficeTableColumns
    },
};
