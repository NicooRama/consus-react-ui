export const backofficeTableData = [
    {
        name: 'Fulano',
        lastname: 'De Tal',
        age: 30,
    },
    {
        name: 'Mengano',
        lastname: 'De Tal',
        age: 28,
    },
    {
        name: 'Sutano',
        lastname: 'De Tal',
        age: 20,
    },
];

export const backofficeTableColumns = [
    {
        Header: 'Nombre',
        accessor: 'name',
        align: 'left'
    },
    {
        Header: 'Apellido',
        accessor: 'lastname',
    },
    {
        Header: 'Edad',
        accessor: 'age',
    }
]
