import {HeaderTitleCard} from "./HeaderTitleCard";
import {render} from "../../../test/test.utils";

describe('<HeaderTitleCard />', () => {
    it('renders <HeaderTitleCard /> successfully', () => {
        const {getByTestId} = render(<HeaderTitleCard data-testid={'header-title-card'} active={true} onEditClick={() => {}} text={'Foo'}/>)
        expect(getByTestId('header-title-card')).toBeInTheDocument();
    });
});
