import type {Meta, StoryObj} from '@storybook/react';
import {HeaderTitleCard} from './HeaderTitleCard';

const meta: any = {
    title: 'Card/HeaderTitleCard',
    component: HeaderTitleCard,
    tags: ['autodocs'],
} satisfies Meta<typeof HeaderTitleCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        text: 'Lorem',
        active: true,
        onEditClick: () => {}
    },
};
