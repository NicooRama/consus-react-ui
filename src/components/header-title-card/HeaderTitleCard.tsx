import React from "react"
import {Badge} from "../badge/Badge";
import {EditButton} from '../icon-button/EditButton';
import {Text} from '../text/Text';
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  align-items: center;
  gap: ${({theme}: any) => theme.spacing.xs}px;
  margin-bottom: ${({theme}: any) => theme.spacing.xxs}px;
  
  .active-badge {
    margin-left: auto;
  }
`

const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  gap: ${({theme}: any) => theme.spacing.xs}px;
`

export interface HeaderTitleCardProps {
    text: string;
    active: boolean;
    showEditButton?: boolean;
    showBadge?: boolean;
    onEditClick: () => void;
    className?: string;
    }

export const HeaderTitleCard: React.FC<HeaderTitleCardProps> = ({
                                    text,
                                    active,
                                    showEditButton = true,
                                    showBadge = true,
                                    onEditClick,
                                    className = "",
                                    ...props
                                }) => {

    // @ts-ignore
    return (<Container  className={className} {...props}>
        <TitleContainer >
            <Text size={'lg'} tag={'h1'} weight="semiBold">
                {text}
            </Text>
            {
                showEditButton && <EditButton
                    circle
                    mini
                    onClick={onEditClick}
                    variant="secondary"
                />
            }
        </TitleContainer>
        {
            showBadge && <Badge className={'active-badge'} color={active ? 'success.dark' : 'error.normal'}>
                {active ? 'ACTIVO' : 'INACTIVO'}
            </Badge>
        }
    </Container>)
};
