import type {Meta, StoryObj} from '@storybook/react';
import {RadioButtonGroup} from './RadioButtonGroup';

const meta: any = {
    title: 'RadioButton/RadioButtonGroup',
    component: RadioButtonGroup,
    tags: ['autodocs'],
} satisfies Meta<typeof RadioButtonGroup>;

export default meta;
type Story = StoryObj<typeof meta>;

const options = ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5"];

export const Primary: Story = {
    args: {
        options,
        onCheck: (option: any) => console.log(`Checked: ${option}`)
    },
    argTypes: {
        selected: {
            options,
            control: { type: 'select' },
        }
    }
};
