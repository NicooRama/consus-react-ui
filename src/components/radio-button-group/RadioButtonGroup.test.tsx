import {RadioButtonGroup} from "./RadioButtonGroup";
import {render} from "../../../test/test.utils";

describe('<RadioButtonGroup />', () => {
    it('renders <RadioButtonGroup /> successfully', () => {
        const {getByTestId} = render(<RadioButtonGroup data-testid={'radio-button-group'} onCheck={() => {}} options={['Item 1', 'Item 2', 'Item 3']}/>)
        expect(getByTestId('radio-button-group')).toBeInTheDocument();
    });
});
