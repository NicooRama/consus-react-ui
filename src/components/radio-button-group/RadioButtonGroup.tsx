import React from 'react'
import { ConsusRender } from '../../utils/types'
import { RadioButton } from '../radio-button/RadioButton'
import { normalizeDisplay } from '@consus/js-utils'
import { mobileMediaQuery } from '../../styles/common'
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  gap: ${({ theme }) => theme.spacing.xs}px;
  flex-wrap: wrap;

  .radio-container {
    color: black;
    border: ${({ theme }) => theme.border};
    border-radius: ${({ theme }) => theme.borderRadius}px;
    padding-top: ${({ theme }) => theme.spacing.sm - 1.5}px;
    padding-bottom: ${({ theme }) => theme.spacing.sm - 1.5}px;
    padding-left: ${({ theme }) => theme.spacing.sm}px;
    padding-right: ${({ theme }) => theme.spacing.lg}px;

    &:focus:not(.disabled) {
      border-color: ${({ theme }) => theme.colors.primary.normal};
    }

    &.disabled {
      background-color: ${({ theme }) => theme.colors.inputs.disabled};
      cursor: default;
    }

    &.disabled:not(.checked) {
      .box {
        background-color: ${({ theme }) => theme.colors.inputs.disabled};
      }
    }

    ${mobileMediaQuery} {
      width: 100%;
    }
  }
`

export interface RadioButtonGroupProps {
  options: any[]
  render?: ConsusRender
  selected?: any
  className?: string
  comparator?: (option: any) => boolean
  onCheck: (option: any) => void
  disabled?: boolean
}

export const RadioButtonGroup: React.FC<RadioButtonGroupProps> = ({
  options,
  render,
  onCheck,
  selected,
  comparator,
  className = '',
  disabled,
  ...props
}) => {
  render = normalizeDisplay(render)

  comparator = comparator || ((option) => option === selected)

  return (
    <Container className={className} {...props}>
      {options.map((option: any, index: number) => (
        <RadioButton
          onCheck={() => onCheck(option)}
          checked={(comparator as any)(option)}
          className={'radio-container'}
          key={index}
          disabled={disabled}
        >
          {(render as (item: any) => string)(option)}
        </RadioButton>
      ))}
    </Container>
  )
}
