import type {Meta, StoryObj} from '@storybook/react';
import {NewEntityCard} from './NewEntityCard';

const meta: any = {
    title: 'Card/NewEntityCard',
    component: NewEntityCard,
    tags: ['autodocs'],
} satisfies Meta<typeof NewEntityCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        description: 'New entity',
    },
};
