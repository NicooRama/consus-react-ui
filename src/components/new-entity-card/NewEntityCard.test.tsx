import {NewEntityCard} from "./NewEntityCard";
import {render} from "../../../test/test.utils";

describe('<NewEntityCard />', () => {
    it('renders <NewEntityCard /> successfully', () => {
        const {getByTestId} = render(<NewEntityCard data-testid={'new-entity-card'} description={'Description'} onClick={() => {}}/>)
        expect(getByTestId('new-entity-card')).toBeInTheDocument();
    });
});
