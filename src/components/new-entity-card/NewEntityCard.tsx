import React from "react"
import styled from "styled-components";
import {Card} from "../card/Card";
import {PlusButton} from "../icon-button/PlusButton";
import {Text} from '../text/Text';


const Content = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: ${({theme}: any) => theme.spacing.xs}px;
  text-align: center;
  justify-items: center;

  p {
    color: ${({theme}: any) => theme.colors.typographic.fade};
  }

  button {
    width: fit-content;
  }
`

export interface NewEntityCardProps {
    description: string;
    onClick: () => void;
    className?: string;

    }

export const NewEntityCard: React.FC<NewEntityCardProps> = ({description, onClick, className = "", ...props}) => {
    return (<Card className={className} {...props}>
        <Content>
            <Text size={'sm'}> {description} </Text>
            <PlusButton circle type="button" variant={'secondary'} onClick={onClick}/>
        </Content>
    </Card>)
};
