import {Checkbox} from "./Checkbox";
import {render} from "../../../test/test.utils";
import {screen} from '@testing-library/react';

describe('<Checkbox />', () => {
    it('renders check icon because checked property setted', () => {
        render(<Checkbox checked={true}/>)
        const icon = screen.getByTestId('check-icon');
        expect(icon).toBeInTheDocument();
    })

    it('not renders check icon because checked property is not setted', () => {
        render(<Checkbox checked={false}/>)
        const icon = screen.queryByTestId('check-icon');
        expect(icon).not.toBeInTheDocument();
    })

    it('not renders check icon because props change to checked false', () => {
        // rerender sirve para renderizar de nuevo un componente pero con distintas props
        const {rerender} = render(<Checkbox checked={true}/>)
        rerender(<Checkbox checked={false}/>)
        const icon = screen.queryByTestId('check-icon');
        expect(icon).not.toBeInTheDocument();
    })

    it('set class name disabled because disabled flag is setted', () => {
        render(<Checkbox checked={true} disabled={true}/>)
        const checkbox = screen.getByTestId('checkbox');
        expect(checkbox).toHaveClass('disabled')
    })

    it('set class name checked because checked is setted', () => {
        render(<Checkbox checked={true}/>)
        const checkbox = screen.getByTestId('checkbox');
        expect(checkbox).toHaveClass('checked')
    })

    it('set class name have-children because checkbox have-children', () => {
        render(<Checkbox checked={true}>Check me</Checkbox>)
        const checkbox = screen.getByTestId('checkbox');
        expect(checkbox).toHaveClass('checked')
    })

    it('call onChange function when checkbox state change to checked', () => {
        const onChange = jest.fn();
        render(<Checkbox checked={false} onChange={onChange}>Check me</Checkbox>)
        const button = screen.getByRole('button');
        button.click();
        expect(onChange).toBeCalledWith(true);
    })

    it('call onChange function when checkbox state change to not checked', () => {
        const onChange = jest.fn();
        render(<Checkbox checked={true} onChange={onChange}>Check me</Checkbox>)
        const button = screen.getByRole('button');
        button.click();
        expect(onChange).toBeCalledWith(false);
    })

    it('not call onChange function because check is disabled', () => {
        const onChange = jest.fn();
        render(<Checkbox checked={true} disabled={true} onChange={onChange}>Check me</Checkbox>)
        const button = screen.getByRole('button');
        button.click();
        expect(onChange).not.toBeCalled();
    })

    it('click on label doesn\'t have effect because handleLabelClick prop is not setted', () => {
        const onChange = jest.fn();
        render(<Checkbox checked={true} handleLabelClick={false} onChange={onChange}>Check me</Checkbox>)
        const label = screen.getByTestId('check-label');
        label.click();
        expect(onChange).not.toBeCalled();
    })

    it('click on label have effect because handleLabelClick prop is setted', () => {
        const onChange = jest.fn();
        render(<Checkbox checked={true} handleLabelClick={true} onChange={onChange}>Check me</Checkbox>)
        const label = screen.getByTestId('check-label');
        label.click();
        expect(onChange).toBeCalled();
    })
});
