import type {Meta, StoryObj} from '@storybook/react';
import {Checkbox} from './Checkbox';

const meta: any = {
    title: 'Commons/Checkbox',
    component: Checkbox,
    tags: ['autodocs'],
} satisfies Meta<typeof Checkbox>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
    },
    argTypes: {
        children: {
            control: { type: 'text' },
        },
    },
};


export const WithText: Story = {
    args: {
        children: 'Checkbox',
    },
};
