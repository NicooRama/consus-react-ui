import React from 'react'
import { icons } from '../icon/icons'
import { Icon } from '../icon/Icon'
import { ButtonWrapper } from '../button-wrapper/ButtonWrapper'
import { ReactElement, useEffect, useState } from 'react'
import styled from "styled-components";

const StyledButtonWrapper = styled(ButtonWrapper)`
  font-size: 16px;
  color: ${props => props.theme.colors.primary.normal};
  display: flex;
  gap: ${props => props.theme.spacing.xs}px;
  align-items: center;
  position: relative;

  &.disabled .square {
    background-color: ${props => props.theme.colors.disabled};
  }

  .icon {
    background-color: white;
    border-radius: 4px;
    border: 1px solid ${props => props.theme.colors.inputs.border};
    width: 17px;
    height: 17px;
    cursor: pointer;

    path {
      fill: ${props => props.theme.colors.primary.normal};
    }

    &:focus {
      border-color: ${props => props.theme.colors.primary.normal};
    }
  }

  .check-label {
    color: ${props => props.theme.colors.text};
    text-align: left;
  }

  &.disabled .icon {
    cursor: default;

    path {
      fill: ${props => props.theme.colors.disabled};
    }
  }
`
export interface CheckboxProps {
  checked?: boolean
  disabled?: boolean
  children?: ReactElement | string
  onChange?: (checked: boolean) => void
  className?: string
  handleLabelClick?: boolean
  iconProps?: any
  [props: string]: any
}

export const Checkbox: React.FC<CheckboxProps> = ({
  checked = false,
  disabled = false,
  children,
  onChange = () => {},
  className = '',
  handleLabelClick = true,
  iconProps,
  ...props
}) => {
  const [checkedState, setChecked] = useState(checked)

  useEffect(() => {
    setChecked(checked)
  }, [checked])

  const toggleCheck = (e: any) => {
    e.stopPropagation()
    e.preventDefault()
    if (!disabled) {
      onChange(!checkedState)
      setChecked(!checkedState)
    }
  }

  const toggleLabel = handleLabelClick ? toggleCheck : () => {}

  return (
    <StyledButtonWrapper
      data-testid={'checkbox'}
      className={`${className}  ${disabled ? 'disabled' : ''} ${checkedState ? 'checked' : ''} ${
        !!children ? 'have-children' : ''
      }`}
      onClick={toggleLabel}
      {...props}
    >
      <div className="icon" onClick={toggleCheck}>
        {checkedState && (
          <Icon data-testid="check-icon" icon={icons.check} width={18} height={18} {...iconProps} />
        )}
      </div>
      <div data-testid={'check-label'} className={'check-label'} onClick={toggleLabel}>
        {children}
      </div>
    </StyledButtonWrapper>
  )
}

