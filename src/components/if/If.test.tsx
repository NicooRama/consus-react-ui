import {If} from "./If";
import {render} from "../../../test/test.utils";

describe('<If />', () => {
    it('renders <If /> successfully', () => {
        const {getByTestId} = render(<If condition={true}>
            <div data-testid={'content'}>Content</div>
        </If>)
        expect(getByTestId('content')).toBeInTheDocument();
    });
});
