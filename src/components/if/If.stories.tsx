import type {Meta, StoryObj} from '@storybook/react';
import {If} from './If';

const meta: any = {
    title: 'Commons/If',
    component: If,
    tags: ['autodocs'],
} satisfies Meta<typeof If>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        condition: true,
        children: 'Content'
    },
};
