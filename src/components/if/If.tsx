import React from "react"
interface IfProps {
    condition: any;
    children: any;
}

export const If: React.FC<IfProps> = ({condition, children}) => (
    condition ? children : null
)
