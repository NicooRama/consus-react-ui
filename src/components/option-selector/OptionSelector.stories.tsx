import type {Meta, StoryObj} from '@storybook/react';
import {OptionSelector} from './OptionSelector';

const meta: any = {
    title: 'Commons/OptionSelector',
    component: OptionSelector,
    tags: ['autodocs'],
} satisfies Meta<typeof OptionSelector>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        options: ['Opción 1', 'Opción 2', 'Opción 3'],
        onChange: () => {}
    },
};
