import React, {useEffect, useState} from 'react';
import styled from "styled-components";
import {normalizeDisplay} from "@consus/js-utils";
import {Button} from '../button/Button';
import {ConsusRender} from "../../utils/types";

const Container = styled.div`
  display: flex;
  gap: ${({theme}: any) => theme.spacing.xs}px;
  height: fit-content;
  flex-direction: row;
`

export interface OptionSelectorProps {
    options: any[];
    selected?: any;
    onChange: (option: any) => void;
    comparator?: (option: any) => boolean;
    render?: ConsusRender,
    [props: string]: any;
}

/**
 * @param options
 * @param selected to Controlled
 * @param onChange
 * @param className
 * @param props
 * @constructor
 */
export const OptionSelector: React.FC<OptionSelectorProps> = ({
                                                                  options = [],
                                                                  selected,
                                                                  onChange = () => {
                                                                  },
                                                                  comparator,
                                                                  render,
                                                                  ...props
                                                              }) => {
    const [innerSelected, setInnerSelected] = useState(selected);

    comparator = comparator || ((option: any) => innerSelected === option);

    useEffect(() => {
        setInnerSelected(selected);
    }, [selected]);

    render = normalizeDisplay(render);

    const handleChange = (optionSelected: any) => {
        let newOptionSelected;
        if (optionSelected === innerSelected) {
            newOptionSelected = null;
        } else {
            newOptionSelected = optionSelected;
        }
        setInnerSelected(newOptionSelected);
        onChange(newOptionSelected);
    }

    return (<Container {...props}>
        {
            options.map((option, index) => (
                <Button onClick={() => handleChange(option)}
                        active={(comparator as any)(option)}
                        variant={'secondary'}
                        key={index}
                >
                    {(render as (option: any) => string)(option)}
                </Button>
            ))
        }
    </Container>)
};
