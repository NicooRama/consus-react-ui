import {OptionSelector} from "./OptionSelector";
import {render} from "../../../test/test.utils";

describe('<OptionSelector />', () => {
    it('renders <OptionSelector /> successfully', () => {
        const {getByTestId} = render(<OptionSelector data-testid={'option-selector'} onChange={() => {}} options={['Item 1', 'Item 2']}/>)
        expect(getByTestId('option-selector')).toBeInTheDocument();
    });
});
