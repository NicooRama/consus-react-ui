import React from "react"
import { ThemeColorIndex} from '../../styles/theme.interface';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {ButtonWrapper} from "../button-wrapper/ButtonWrapper";
import styled from "styled-components";
import {getIn} from "../../utils/utils";

const StyledButtonWrapper = styled(ButtonWrapper)<{color: ThemeColorIndex}>`
    z-index: 1;
    background: white;
    border-radius: 50px;
    font-size: 22px;
    cursor: pointer;
    transition-duration: 0.15s;

    svg {
        display: flex;
    }

    path {
        transition: 0.3s;
        fill: #0000004f;
    }

    &:hover path {
        ${({color, theme}) => `fill: ${getIn(theme.colors, color)};`}
    }

    &.active path {
        ${({color, theme}) => `fill: ${getIn(theme.colors, color)};`}
    }

    &.on-corner {
        position: absolute;
        right: -10px;
        top: -10px;
    }
`;

export interface InteractiveButtonProps {
    icon: any,
    active?: boolean,
    onClick: () => void,
    color?: ThemeColorIndex,
    onCorner?: boolean,
    className?: string,
    }

export const InteractiveButton: React.FC<InteractiveButtonProps> = ({
                                      icon,
                                      onClick,
                                      color = 'primary.normal',
                                      onCorner = false,
                                      active = false,
                                      className = "",
                                      ...props
                                  }) => {
    return (
        <StyledButtonWrapper className={`${!!onCorner ? 'on-corner' : ''} ${active ? 'active' : ''} ${className}`} onClick={onClick} color={color}>
            <FontAwesomeIcon icon={icon} {...props}/>
        </StyledButtonWrapper>
    )
};
