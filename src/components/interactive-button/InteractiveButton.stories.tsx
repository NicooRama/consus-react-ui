import type {Meta, StoryObj} from '@storybook/react';
import {InteractiveButton} from './InteractiveButton';
import {makeControlForObjectArray} from "../../storybook.utils";
import {icons} from "../icon/icons";
import {faCheckCircle} from "@fortawesome/free-regular-svg-icons";

const meta: any = {
    title: 'Button/InteractiveButton',
    component: InteractiveButton,
    tags: ['autodocs'],
} satisfies Meta<typeof InteractiveButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        onCorner: false,
        icon: faCheckCircle,
    },
};
