import {InteractiveButton} from "./InteractiveButton";
import {render} from "../../../test/test.utils";
import {faCoffee} from "@fortawesome/free-solid-svg-icons";

describe('<InteractiveButton />', () => {
    it('renders <InteractiveButton /> successfully', () => {
        const {getByTestId} = render(<InteractiveButton data-testid={'interactive-button'} icon={faCoffee} onClick={() => {}}/>)
        expect(getByTestId('interactive-button')).toBeInTheDocument();
    });
});
