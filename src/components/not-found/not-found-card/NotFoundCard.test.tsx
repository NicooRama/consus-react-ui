import {NotFoundCard} from "./NotFoundCard";
import {render} from "../../../../test/test.utils";

describe('<NotFoundCard />', () => {
    it('renders <NotFoundCard /> successfully', () => {
        const {getByTestId} = render(<NotFoundCard data-testid={'not-found-card'} message={'Card'} onClick={() => {}}/>)
        expect(getByTestId('not-found-card')).toBeInTheDocument();
    });
});
