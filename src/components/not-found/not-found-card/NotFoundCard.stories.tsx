import type {Meta, StoryObj} from '@storybook/react';
import {NotFoundCard} from './NotFoundCard';

const meta: any = {
    title: 'Layout/NotFoundCard',
    component: NotFoundCard,
    tags: ['autodocs'],
} satisfies Meta<typeof NotFoundCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        message: "Not Found",
        onClick: () => {}
    },
};
