import React from "react"
import styled from "styled-components";
import {Card} from "../../card/Card";
import {NotFound, NotFoundProps} from "../NotFound";

const StyledCard = styled(Card)`
  & > div {
    height: calc(400px - ${({theme}: any) => theme.spacing.md * 2}px);
  }
`

interface NotFoundCardProps extends NotFoundProps {
    className?: string;
}

export const NotFoundCard: React.FC<NotFoundCardProps> = ({className, message, ...props}) => {
    return (<StyledCard className={className}>
        <NotFound message={message} {...props} />
    </StyledCard>)
};
