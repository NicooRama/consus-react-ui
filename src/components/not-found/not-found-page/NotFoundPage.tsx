import React from "react"
import styled from "styled-components";
import {Container} from "../../container/Container";
import {mobileMediaQuery} from "../../../styles/common";
import {NotFound, NotFoundProps} from "../NotFound";

const StyledContainer = styled(Container)`
  margin-top: ${({theme}: any) => theme.spacing.xlg * 4}px;

  ${mobileMediaQuery} {
    margin-top: 0;
  }
  
  p {
    margin-bottom: ${({theme}: any) => theme.spacing.lg}px;
  }
`

export const NotFoundPage: React.FC<NotFoundProps> = (props) => {
    return (<StyledContainer>
        <NotFound {...props} />
    </StyledContainer>)
};
