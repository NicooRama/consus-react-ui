import {NotFoundPage} from "./NotFoundPage";
import {render} from "../../../../test/test.utils";

describe('<NotFoundPage />', () => {
    it('renders <NotFoundPage /> successfully', () => {
        const {getByTestId} = render(<NotFoundPage data-testid={'not-found-page'} message={'Not found'} onClick={() => {}}/>)
        expect(getByTestId('not-found-page')).toBeInTheDocument();
    });
});
