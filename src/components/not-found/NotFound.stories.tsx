import type {Meta, StoryObj} from '@storybook/react';
import {NotFound} from './NotFound';

const meta: any = {
    title: 'Layout/NotFound',
    component: NotFound,
    tags: ['autodocs'],
} satisfies Meta<typeof NotFound>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        message: "Not Found",
        onClick: () => {}
    },
};
