import React from "react"
import styled from "styled-components";
import notFoundImg from './not-found.jpg';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import {IconButton} from "../icon-button/IconButton";
import {Text} from "../text/Text";

const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  place-items: center;

  p {
    text-align: center;
  }

  img {
    mix-blend-mode: darken;
  }
`

export interface NotFoundProps {
    message: string;
    illustration?: any;
    showButton?: boolean;
    onClick: () => void;
    buttonText?: string;

    }

export const NotFound: React.FC<NotFoundProps> = ({
                             message,
                             illustration = notFoundImg,
                             buttonText = 'Volver atras',
                             showButton = true,
                             onClick,
                             ...props
                         }) => {
    return (<Container {...props}>
        <img src={illustration} alt={'No encontrado'}/>
        <Text size={'xlg'}>{message}</Text>
        {
            showButton && <IconButton onClick={onClick} icon={faArrowLeft}>{buttonText}</IconButton>
        }
    </Container>)
};
