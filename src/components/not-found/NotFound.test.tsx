import {NotFound} from "./NotFound";
import {render} from "../../../test/test.utils";

describe('<NotFound />', () => {
    it('renders <NotFound /> successfully', () => {
        const {getByTestId} = render(<NotFound data-testid={'not-found'} message={'Not Found'} onClick={() => {}}/>)
        expect(getByTestId('not-found')).toBeInTheDocument();
    });
});
