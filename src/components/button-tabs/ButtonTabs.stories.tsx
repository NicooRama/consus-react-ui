import type {Meta, StoryObj} from '@storybook/react';
import {ButtonTabs} from './ButtonTabs';
import {makeControlForObjectArray, makeControlForRender} from "../../storybook.utils";

const meta: any = {
    title: 'Button/ButtonTabs',
    component: ButtonTabs,
    tags: ['autodocs'],
} satisfies Meta<typeof ButtonTabs>;

export default meta;
type Story = StoryObj<typeof meta>;

const stringTabs = ['Tab 1', 'Tab 2', 'Tab 3', 'Tab 4', 'Tab 5', 'Tab 6'];

export const Strings: Story = {
    args: {
        items: stringTabs,
        onClick: (item: any) => { console.log(item) },
    },
    argTypes: {
        selected: {
            options: stringTabs,
            control: { type: 'select' },
        },
    }
};

const objectTabs = [
    {
        value: 'tab1',
        description: 'Tab 1',
    },
    {
        value: 'tab2',
        description: 'Tab 2',
    },
    {
        value: 'tab3',
        description: 'Tab 3',
    },
    {
        value: 'tab4',
        description: 'Tab 4',
    },
    {
        value: 'tab5',
        description: 'Tab 5',
    },
    {
        value: 'tab6',
        description: 'Tab 6',
    },
];

export const Objects: Story = {
    args: {
        items: objectTabs,
        onClick: (item: any) => { console.log(item) },
        render: 'description',
    },
    argTypes: {
        selected: makeControlForObjectArray(objectTabs, 'description'),
        render: makeControlForRender(objectTabs[0], 'description'),
    }
}
