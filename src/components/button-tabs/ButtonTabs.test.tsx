import {ButtonTabs} from "./ButtonTabs";
import {render} from "../../../test/test.utils";
import {screen} from "@testing-library/react";

describe('<ButtonTabs />', () => {
    it('renders <ButtonTabs /> successfully', () => {
        const {getByTestId} = render(<ButtonTabs data-testid={'button-tabs'} items={['Button 1', 'Button 2']} onClick={() => {}}/>)
        expect(getByTestId('button-tabs')).toBeInTheDocument();
    });

    it('renders <ButtonTabs /> with object items successfully', () => {
        const items = [
            {
                value: "tab1",
                description: "Tab 1"
            },
            {
                value: "tab2",
                description: "Tab 2"
            },
        ]
        render(<ButtonTabs items={items} onClick={jest.fn()} render={'description'}/>)
    })

    it('handle onClick successfully', () => {
        const onClick = jest.fn();
        render(<ButtonTabs items={["Tab 1", "Tab 2"]} onClick={onClick}/>)
        const buttons = screen.getAllByRole('button');
        buttons[1].click();
        expect(onClick).toBeCalledWith("Tab 2");
    })

    it('set active class on string selected item', () => {
        render(<ButtonTabs items={["Tab 1", "Tab 2"]} selected={"Tab 2"} onClick={jest.fn()}/>)
        const buttons = screen.getAllByRole('button');
        expect(buttons[1]).toHaveClass("active");
    })

    it('set active class on object selected item', () => {
        const items = [
            {
                value: "tab1",
                description: "Tab 1"
            },
            {
                value: "tab2",
                description: "Tab 2"
            },
        ]
        render(<ButtonTabs items={items} selected={items[1]} onClick={jest.fn()} render={'description'}/>)
        const buttons = screen.getAllByRole('button');
        expect(buttons[1]).toHaveClass("active");
    })
});
