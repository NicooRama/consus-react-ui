import React from 'react'
import { normalizeDisplay } from '@consus/js-utils'
import { Button } from '../button/Button'
import styled from "styled-components";

const Container = styled.div<{minWidth: number}>`
  display: grid;
    grid-template-columns: repeat(auto-fit, minmax(${({minWidth}) => minWidth}px, 1fr));
    grid-gap: ${({theme}) => theme.spacing.xxs}px;
    button {
        margin-right: 0;
        margin-bottom: 0;
    }
`

export interface ButtonTabsProps {
  minWidth?: number
  items: any[]
  selected?: any
  render?: any
  className?: string
  buttonProps?: object
  onClick: (item: any) => void
}

export const ButtonTabs: React.FC<ButtonTabsProps> = ({
  minWidth = 140,
  items,
  selected,
  render,
  className = '',
  onClick,
  buttonProps = {},
  ...props
}) => {
  render = normalizeDisplay(render) as (item: any) => string
  const isActive = (item: any) => item === selected

  return (
    <Container minWidth={minWidth} className={className} {...props}>
      {items.map((item, key) => (
        <Button key={key} onClick={() => onClick(item)} active={isActive(item)} {...buttonProps}>
          {render(item)}
        </Button>
      ))}
    </Container>
  )
}
