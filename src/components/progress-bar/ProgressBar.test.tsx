import {ProgressBar} from "./ProgressBar";
import {render} from "../../../test/test.utils";

describe('<ProgressBar />', () => {
    it('renders <ProgressBar /> successfully', () => {
        const {getByTestId} = render(<ProgressBar data-testid={'progress-bar'} color={'primary.normal'} completed={50}/>)
        expect(getByTestId('progress-bar')).toBeInTheDocument();
    });
});
