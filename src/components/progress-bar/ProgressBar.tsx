import styled from "styled-components";
import React from 'react';
import {ThemeColorIndex} from "../../styles/theme.interface";
import {getIn} from "../../utils/utils";

const Container = styled.div`
  height: 8px;
  background-color: ${({theme}: any) => theme.colors.gray.lightest};
  border-radius: 16px;
`

const Filler = styled.div<{ completed: number, color: ThemeColorIndex }>`
  height: 100%;
  width: ${({completed}: any) => completed}%;
  background-color: ${({theme, color}: any) => getIn(theme.colors, color)};
  border-radius: inherit;
  text-align: right;
`

export interface ProgressBarProps {
    completed: number;
    color: ThemeColorIndex;
    }

export const ProgressBar: React.FC<ProgressBarProps> = ({color, completed, ...props}) => {
    return <Container {...props}>
        <Filler completed={completed <= 100 ? completed : 100} color={color} />
    </Container>
}
