import type {Meta, StoryObj} from '@storybook/react';
import {ProgressBar} from './ProgressBar';

const meta: any = {
    title: 'Commons/ProgressBar',
    component: ProgressBar,
    tags: ['autodocs'],
} satisfies Meta<typeof ProgressBar>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        color: 'primary.normal',
        completed: 50,
    },
    argTypes: {
        completed: {
            control: {
                type: 'range',
            }
        }
    }
};
