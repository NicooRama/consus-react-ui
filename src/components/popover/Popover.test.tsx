import {Popover} from "./Popover";
import {render} from "../../../test/test.utils";
import {screen} from "@testing-library/react";

describe('<Popover />', () => {
    it('renders <Popover /> successfully', () => {
        render(<Popover data-testid={'popover'} content={'Content'} header={'Header'} isOpen={false} onClose={() => {}}>
            <div data-testid={'popover-firer'}>Content</div>
        </Popover>)
        expect(screen.getByTestId('popover-firer')).toBeInTheDocument();
    });
});
