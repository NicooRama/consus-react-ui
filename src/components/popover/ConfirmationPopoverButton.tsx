import React from 'react';
import {Popover} from "./Popover";
import {useState} from "react";
import {Button} from "../button/Button";

import {Text} from "../text/Text";
import {faTrashAlt} from "@fortawesome/free-regular-svg-icons";
import {IconButton} from '../icon-button/IconButton';
import styled from "styled-components";
import {LoaderWrapper} from "../loader-wrapper/LoaderWrapper";

const PopoverContentContainer = styled.div`
  background-color: white;
  display: grid;
  grid-gap: ${({ theme }) => theme.spacing.sm}px;

  .buttons-panel {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: ${({ theme }) => theme.spacing.xs}px;

    button {
      width: 100%;
    }
  }
`;

export interface ConfirmationPopoverButtonProps {
    header?: string,
    message: string,
    onAccept: (event: Event) => void | Promise<void>,
    onCancel?: (event: Event) => void | Promise<void>,
    positions?: string[],
    initialIsOpen?: boolean,
    children?: ({onClick}: { onClick: (event?: Event) => void }) => any,
    className?: string,

    }

export const ConfirmationPopoverButton: React.FC<ConfirmationPopoverButtonProps> = ({
                                              header = 'Confirmación',
                                              message,
                                              initialIsOpen = false,
                                              className = "",
                                              onAccept,
                                              onCancel = () => {
                                              },
                                              positions = ['top', 'bottom', 'left', 'right'],
                                              children,
                                              ...props
                                          }) => {
    const [isOpen, setIsOpen] = useState(initialIsOpen);
    const [isAccepting, setIsAccepting] = useState(false);
    const [isCanceling, setIsCanceling] = useState(false);

    const closeModal = () => setIsOpen(false);

    const handleAccept = async (event: Event) => {
        setIsAccepting(true);
        await onAccept(event);
        setIsAccepting(false);
        closeModal();
    }

    const handleCancel = async (event: Event) => {
        setIsCanceling(true);
        await onCancel(event);
        closeModal();
        setIsCanceling(false);
    }

    const popoverContent = (
        <PopoverContentContainer className={className}>
            <Text size={'xs'}>{message}</Text>
            <div className={'buttons-panel'}>
                <LoaderWrapper loading={isCanceling} size={28} center>
                    <Button color={'gray.darken'} variant={'tertiary'} onClick={handleCancel}>Cancelar</Button>
                </LoaderWrapper>
                <LoaderWrapper loading={isAccepting} size={28} center>
                    <Button color={'error.normal'} onClick={handleAccept}>Eliminar</Button>
                </LoaderWrapper>
            </div>
        </PopoverContentContainer>
    )

    const onClick = () => setIsOpen(!isOpen);

    return (<Popover isOpen={isOpen}
                           header={header}
                           content={popoverContent}
                           positions={positions}
                           onClose={closeModal}
                           contentStyles={{borderTop: 0}}
                           {...props}>
        <span className={className}>
            {
                !!children ? children({onClick}) :
                    <IconButton
                        circle
                        color="error.normal"
                        icon={faTrashAlt}
                        onClick={onClick}
                        variant="secondary"
                    />
            }
        </span>
    </Popover>)
};
