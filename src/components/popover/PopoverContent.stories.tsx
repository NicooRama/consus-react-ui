import type {Meta, StoryObj} from '@storybook/react';
import {PopoverContent} from './PopoverContent';

const meta: any = {
    title: 'Popover/PopoverContent',
    component: PopoverContent,
    tags: ['autodocs'],
} satisfies Meta<typeof PopoverContent>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: 'Content'
    },
};
