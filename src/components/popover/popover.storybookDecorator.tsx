import styled from "styled-components";

const PopoverStoriesStyles = styled.div`
  padding: 40px;
  text-align: center;
  height: 300px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
export const PopoverStorybookDecorator = (Story: any) => (
  <PopoverStoriesStyles>
    <Story />
  </PopoverStoriesStyles>
)
