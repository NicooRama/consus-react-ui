import React from 'react'
import styled from "styled-components";

const Container = styled.div`
  border: 1px solid ${({ theme }) => theme.colors.border};
  border-bottom-right-radius: ${({ theme }) => theme.borderRadius}px;
  border-bottom-left-radius: ${({ theme }) => theme.borderRadius}px;
  border-top: 0;
  padding: ${({ theme }) => theme.spacing.sm}px;
  font-size: ${({ theme }) => theme.font.size.xxs}px;
  background-color: white;
`;
export const PopoverContent: React.FC<PopoverContentProps> = ({ children, ...props }) => {
  return <Container {...props}>{children}</Container>
}

export interface PopoverContentProps {
  children: any
}
