import type {Meta, StoryObj} from '@storybook/react';
import {Popover} from './Popover';
import {PopoverStorybookDecorator} from "./popover.storybookDecorator";

const meta: any = {
    title: 'Popover/Popover',
    component: Popover,
    tags: ['autodocs'],
    decorators: [PopoverStorybookDecorator]
} satisfies Meta<typeof Popover>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        header: 'Title',
        content: "I'm a popover",
        isOpen: true,
        positions: ['top', 'bottom', 'left', 'right'],
        onClose: () => {console.log("Closed")},
        children: <span>hover me</span>
    },
};
