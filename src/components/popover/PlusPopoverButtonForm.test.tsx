import { PlusPopoverButtonForm } from './PlusPopoverButtonForm'
import { render } from '../../../test/test.utils'
import {screen} from "@testing-library/react";

describe('<PlusPopoverButtonForm />', () => {
  it('renders <PlusPopoverButtonForm /> successfully', async () => {
    render(
      <PlusPopoverButtonForm
        data-testid={'plus-popover-button-form'}
        header={'Header'}
        onClick={() => {}}
        onClose={() => {}}
      >
        {
          // @ts-ignore
          ({closeModal}) => (
              <div data-testid={'popover-content'}>Content</div>
          )
        }
      </PlusPopoverButtonForm>,
    )
    await screen.getByRole('button').click();
    expect(screen.getByTestId('popover-content')).toBeInTheDocument()
  })
})
