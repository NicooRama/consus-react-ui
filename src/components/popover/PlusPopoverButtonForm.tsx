import React from 'react'
import { Popover } from './Popover'
import { PlusButton } from '../icon-button/PlusButton'
import { useState } from 'react'

export interface PlusPopoverButtonFormProps {
  header: string
  children: ({ closeModal }: { closeModal: any }) => any
  positions?: string[]
  initialOpen?: boolean
  onClick: () => void
  onClose: () => void
  className?: string
}

export const PlusPopoverButtonForm: React.FC<PlusPopoverButtonFormProps> = ({
  header,
  children,
  initialOpen = false,
  className = '',
  positions = ['top', 'bottom', 'left', 'right'],
  onClose,
  ...props
}) => {
  const [isOpen, setIsOpen] = useState(initialOpen)

  const closeModal = () => setIsOpen(false)

  return (
    <Popover
      isOpen={isOpen}
      header={header}
      content={children({ closeModal })}
      positions={positions}
      onClose={onClose}
      {...props}
    >
      <div className={className}>
        <PlusButton onClick={() => setIsOpen(!isOpen)} mini circle />
      </div>
    </Popover>
  )
}
