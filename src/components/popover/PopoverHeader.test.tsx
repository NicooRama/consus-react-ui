import {PopoverHeader} from "./PopoverHeader";
import {render} from "../../../test/test.utils";

describe('<PopoverHeader />', () => {
    it('renders <PopoverHeader /> successfully', () => {
        const {getByTestId} = render(<PopoverHeader data-testid={'popover-header'} onClose={() => {}}>Header</PopoverHeader>)
        expect(getByTestId('popover-header')).toBeInTheDocument();
    });
});
