import {ConfirmationPopoverButton} from "./ConfirmationPopoverButton";
import {render} from "../../../test/test.utils";

describe('<ConfirmationPopoverButton />', () => {
    it('renders <ConfirmationPopoverButton /> successfully', () => {
        const {getByTestId} = render(<ConfirmationPopoverButton data-testid={'confirmation-popover-button'} message={'Message'} onAccept={() => {}}>
            {
                ({onClick}: any) => <button data-testid={'popover-button'} onClick={onClick}>Click</button>
            }
        </ConfirmationPopoverButton>)
        expect(getByTestId('popover-button')).toBeInTheDocument();
    });
});
