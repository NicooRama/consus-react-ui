import React from "react"
import PropTypes from 'prop-types';
import {ConsusTheme} from "../../styles/theme.interface";
import {ArrowContainer, Popover as ReactPopover} from 'react-tiny-popover'
import {PopoverHeader} from "./PopoverHeader";
import {PopoverContent} from "./PopoverContent";

import {StrUtils} from "@consus/js-utils";
import styled, {useTheme} from "styled-components";

const Container = styled.div`
    background-color: white;
`

export interface PopoverProps {
    header: any,
    isOpen: boolean,
    children: any,
    content: any,
    className?: string,
    onClose: () => void,
    contentStyles?: any,

    [props: string]: any,
}

export const Popover: React.FC<PopoverProps> = ({
                            header,
                            isOpen,
                            children,
                            content,
                            className = "",
                            onClose,
                            contentStyles = {},
                            ...props
                        }) => {
    const theme = useTheme() as ConsusTheme;

    const parsedContent = ({position, childRect, popoverRect}: any) => {
        const color = theme.colors.border;
        const style = position === 'bottom' ? {top: '1px'} : {};

        return <ArrowContainer
            position={position}
            childRect={childRect}
            popoverRect={popoverRect}
            arrowStyle={style}
            arrowColor={color}
            arrowSize={5}
            className='popover-arrow-container'
            arrowClassName='popover-arrow'
        >
            <Container>
                {
                    StrUtils.isString(header) ? <PopoverHeader onClose={onClose}>{header}</PopoverHeader> :
                        header({onClose})
                }
                {/**@ts-ignore**/}
                <PopoverContent className={'popover-content'} style={contentStyles}>{content}</PopoverContent>
            </Container>
        </ArrowContainer>
    };

    return (<ReactPopover content={parsedContent}
                          isOpen={isOpen}
                          reposition={true}
                          onClickOutside={onClose}
                          containerStyle={{zIndex: '20'}}
                          {...props}>
        {children}
    </ReactPopover>)
};

Popover.propTypes = {
    header: PropTypes.node.isRequired,
    isOpen: PropTypes.bool.isRequired,
    children: PropTypes.node.isRequired,
    content: PropTypes.node.isRequired,
    className: PropTypes.string,
};
