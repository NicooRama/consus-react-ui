import type {Meta, StoryObj} from '@storybook/react';
import {ConfirmationPopoverButton} from './ConfirmationPopoverButton';
import {PopoverStorybookDecorator} from "./popover.storybookDecorator";

const meta: any = {
    title: 'Popover/ConfirmationPopoverButton',
    component: ConfirmationPopoverButton,
    tags: ['autodocs'],
    decorators: [PopoverStorybookDecorator]
} satisfies Meta<typeof ConfirmationPopoverButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        message: '¿Estas seguro que queres eliminar la entidad?',
        onAccept: () => new Promise(resolve => setTimeout(resolve, 1000)),
        onCancel: () => new Promise(resolve => setTimeout(resolve, 1000)),
        positions: ['top', 'bottom', 'left', 'right']
    },
};
