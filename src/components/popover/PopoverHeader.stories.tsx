import type {Meta, StoryObj} from '@storybook/react';
import {PopoverHeader} from './PopoverHeader';

const meta: any = {
    title: 'Popover/PopoverHeader',
    component: PopoverHeader,
    tags: ['autodocs'],
} satisfies Meta<typeof PopoverHeader>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: 'Header'
    },
};
