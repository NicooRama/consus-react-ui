import type {Meta, StoryObj} from '@storybook/react';
import {PlusPopoverButtonForm} from './PlusPopoverButtonForm';
import {PopoverStorybookDecorator} from "./popover.storybookDecorator";
import {Text} from "../text/Text";
import {Input} from "../input/Input";
import {Button} from "../button/Button";

const meta: any = {
    title: 'Popover/PlusPopoverButtonForm',
    component: PlusPopoverButtonForm,
    tags: ['autodocs'],
    decorators: [PopoverStorybookDecorator]
} satisfies Meta<typeof PlusPopoverButtonForm>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    render: () => (
        <PlusPopoverButtonForm header={'New Element'} positions={['top', 'bottom', 'left', 'right']} onClick={() => {}} onClose={() => {}}>
            {
                ({closeModal}) => (
                    <div style={{display: 'grid', gridGap: 8}}>
                        <Text size={'xs'}>Valor</Text>
                        <Input size={'xs'}/>
                        <div style={{display: 'grid', gridTemplateColumns: '1fr 1fr', gridGap: 8}}>
                            <Button onClick={closeModal}>Ok</Button>
                            <Button onClick={closeModal}>Cancelar</Button>
                        </div>
                    </div>
                )
            }
        </PlusPopoverButtonForm>
    )
};
