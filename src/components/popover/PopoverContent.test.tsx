import {PopoverContent} from "./PopoverContent";
import {render} from "../../../test/test.utils";

describe('<PopoverContent />', () => {
    it('renders <PopoverContent /> successfully', () => {
        const {getByTestId} = render(<PopoverContent data-testid={'popover-content'}>Content</PopoverContent>)
        expect(getByTestId('popover-content')).toBeInTheDocument();
    });
});
