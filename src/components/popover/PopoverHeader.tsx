import React from "react"

import styled from "styled-components";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTimes} from '@fortawesome/free-solid-svg-icons';
import {ButtonWrapper} from "../button-wrapper/ButtonWrapper";
import {Text} from "../text/Text";

const Container = styled.div`
  background-color: white;
  display: flex;
  align-items: center;
  gap: ${({theme}: any) => theme.spacing.xs}px;
  border: 1px solid ${({theme}: any) => theme.colors.border};
  border-radius: ${({theme}: any) => theme.borderRadius}px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
  padding: ${({theme}: any) => theme.spacing.sm}px;
  padding-bottom: 0;
  border-bottom: 0;
`

const StyledButtonWrapper = styled(ButtonWrapper as any)`
  margin-left: auto;
`

export interface PopoverHeaderProps {
    children: any,
    onClose: () => void;
}

export const PopoverHeader: React.FC<PopoverHeaderProps> = ({children, onClose, ...props}) => {

    return <Container {...props}>
        <Text size={'xs'} weight={'semiBold'}>{children}</Text>
        <StyledButtonWrapper onClick={onClose}>
            <FontAwesomeIcon icon={faTimes}/>
        </StyledButtonWrapper>
    </Container>
};
