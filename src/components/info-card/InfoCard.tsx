import React from 'react';
import styled from 'styled-components';
import {Size} from "../../utils/types";
import {Card} from "../card/Card";
import {Icon} from "../icon/Icon";
import {Text} from "../text/Text";
import {fontAwesomeToIcon} from "../icon/icon.utils";
import {faInfoCircle} from "@fortawesome/free-solid-svg-icons";

const Container = styled(Card)`
  display: grid;
  grid-template-columns: auto 1fr;
  grid-gap: ${({theme}: any) => theme.spacing.sm}px;
  align-items: center;
  background-color: ${({theme}: any) => theme.colors.info.lightest};
`

export interface InfoCardProps {
    size?: Size,
    message: string;
    "data-testid"?: string;
    [props: string]: any;
}

export const InfoCard: React.FC<InfoCardProps> = ({
                                                      size = 'sm',
                                                      message,
                                                      ...props
                                                  }) => {
    return (<Container {...props}>
        <Icon icon={fontAwesomeToIcon(faInfoCircle)} color={'info.dark'}/>
        <Text size={size} color={'info.dark'}>{message}</Text>
    </Container>)
};
;
