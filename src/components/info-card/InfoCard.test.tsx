import {InfoCard} from "./InfoCard";
import {render} from "../../../test/test.utils";

describe('<InfoCard />', () => {
    it('renders <InfoCard /> successfully', () => {
        const {getByTestId} = render(<InfoCard data-testid={'info-card'} message={'Informative message'}/>)
        expect(getByTestId('info-card')).toBeInTheDocument();
    });

    it('renders message successfully', () => {
        const {getByText} = render(<InfoCard data-testid={'info-card'} message={'Informative message'}/>)
        expect(getByText('Informative message')).toBeInTheDocument();
    });
});
