import type {Meta, StoryObj} from '@storybook/react';
import {InfoCard} from './InfoCard';

const meta: any = {
    title: 'Card/InfoCard',
    component: InfoCard,
    tags: ['autodocs'],
} satisfies Meta<typeof InfoCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        message: 'Informative message'
    },
};
