import {InteractiveRemoveButton} from "./InteractiveRemoveButton";
import {render} from "../../../test/test.utils";

describe('<InteractiveRemoveButton />', () => {
    it('renders <InteractiveRemoveButton /> successfully', () => {
        const {getByTestId} = render(<InteractiveRemoveButton data-testid={'interactive-remove-button'} onClick={() => {}}/>)
        expect(getByTestId('interactive-remove-button')).toBeInTheDocument();
    });
});
