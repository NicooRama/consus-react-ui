import React from 'react'
import { InteractiveButton } from '../interactive-button/InteractiveButton'
import { faTimesCircle } from '@fortawesome/free-regular-svg-icons'

export interface InteractiveRemoveButtonProps {
  onClick: () => void
  className?: string

  [props: string]: any
}

export const InteractiveRemoveButton: React.FC<InteractiveRemoveButtonProps> = ({
  onClick,
  className = '',
  ...props
}) => {
  return (
    <InteractiveButton
      color={'error.normal'}
      icon={faTimesCircle}
      className={className}
      onClick={onClick}
      {...props}
    />
  )
}
