import type {Meta, StoryObj} from '@storybook/react';
import {InteractiveRemoveButton} from './InteractiveRemoveButton';

const meta: any = {
    title: 'Button/InteractiveRemoveButton',
    component: InteractiveRemoveButton,
    tags: ['autodocs'],
} satisfies Meta<typeof InteractiveRemoveButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        onCorner: false,
    },
    argTypes: {
        color: {
            table: {
                disable: true,
            },
        },
        icon: {
            table: {
                disable: true,
            },
        },
    }
};
