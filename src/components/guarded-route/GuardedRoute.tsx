import {Redirect, Route} from "react-router-dom";
import {ReactElement} from "react";
interface Props {
    isLoggedIn: boolean;
    component: ({props}: any) => ReactElement;
    componentProps?: object;
    [routeProps: string]: any;
}

export const GuardedRoute = ({isLoggedIn, component: Component, componentProps = {}, ...routeProps}: Props) => {
    return (<Route {...routeProps} render={(props) => (
        isLoggedIn ? <Component {...props} {...componentProps}/> : <Redirect to={{pathname: '/ingresar', state: { from: props.location }}} />
    )
    }>
    </Route>)
};
