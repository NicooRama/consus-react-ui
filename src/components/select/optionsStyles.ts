import {css} from "styled-components";
import {ConsusTheme} from "../../styles/theme.interface";

export const optionsStyles = (theme: ConsusTheme) => css`
  border: solid 1px ${theme.colors.inputs.border};
  border-radius: ${theme.inputs.borderRadius}px;
  cursor: pointer;
  width: inherit;
  background-color: white;
  color: ${theme.colors.typographic.solid};
  border-color: ${theme.colors.inputs.border};
  padding-top: ${theme.spacing.xxs}px;
  padding-bottom: ${theme.spacing.xxs}px;
  margin-top: -1px;
  z-index: 1;
  max-height: 152px;
  overflow: auto;
`;

export const optionStyles = (theme: ConsusTheme) => css`
  width: inherit;
  padding: ${theme.spacing.xs}px ${theme.spacing.sm}px;
  text-align: left;
  font-size: ${theme.font.size.sm}px;
  &:hover,
  &:focus {
    background-color: ${theme.colors.primary.normal};
    transition-duration: 0.2s;
  }
  &:first-of-type {
    border-top-left-radius: ${theme.inputs.borderRadius}px;
    border-top-right-radius: ${theme.inputs.borderRadius}px;
  }
  &:last-of-type {
    border-bottom-left-radius: ${theme.inputs.borderRadius}px;
    border-bottom-right-radius: ${theme.inputs.borderRadius}px;
  }
`;
