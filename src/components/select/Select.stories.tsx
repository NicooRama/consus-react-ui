import type {Meta, StoryObj} from '@storybook/react';
import {Select} from './Select';
import {makeControlForObjectArray} from "../../storybook.utils";

const meta: any = {
    title: 'Commons/Select',
    component: Select,
    tags: ['autodocs'],
} satisfies Meta<typeof Select>;

export default meta;
type Story = StoryObj<typeof meta>;

const stringItems = ["Item 1", "Item 2", "Item 3", "Un item demasiado largo largo largo largo demasiado largo largo"];

export const Strings: Story = {
    args: {
        items: stringItems
    },
    argTypes: {
        selected: {
            options: stringItems,
            control: { type: 'select' },
        },
    }
};

const items = [
    {
        value: 'item1',
        description: 'Item 1',
    },
    {
        value: 'item2',
        description: 'Item 2',
    },
    {
        value: 'item3',
        description: 'Item 3',
    },
    {
        value: 'item4',
        description: 'Item 4',
    },
    {
        value: 'item5',
        description: 'Item 5',
    },
];

export const Objects: Story = {
    args: {
        items,
        render: 'description',
        selected: items[0]
    },
    argTypes: {
        selected: makeControlForObjectArray(items, "description"),
    }
}

export const Function: Story = {
    args: {
        items,
        render: (item: any) => <div>{item?.description}</div>,
    },
    argTypes: {
        selected: makeControlForObjectArray(items, "description"),
    }
}
