import {Select} from "./Select";
import {screen} from '@testing-library/react';
import {render} from "../../../test/test.utils";

describe('<Select />', () => {
    it('renders select with strings successfully', async () => {
        render(<Select items={["Item 1", "Item 2", "Item 3"]} selected={undefined} onChange={() => {}}/>);
        const button = screen.getByRole('button');
        await button.click();
        const itemDiv = screen.getByTestId('option-0');
        expect(itemDiv).toBeInTheDocument();
    })

    it('passing a selected string item select that item successfully', () => {
        render(<Select items={["Item 1", "Item 2", "Item 3"]} selected={"Item 1"} onChange={() => {}}/>);
        const item = screen.getByText("Item 1");
        expect(item).toBeInTheDocument();
    })

    it('renders select with objects successfully', async () => {
        const item = {description: "Hello world"};
        render(<Select items={[item]} render={'description'} selected={undefined} onChange={() => {}}/>)
        const button = screen.getByRole('button');
        await button.click();
        const itemDiv = screen.getByTestId('option-0');
        expect(itemDiv).toBeInTheDocument();
    })

    it('passing a selected object item select that item successfully', async () => {
        const items = [{description: "Item 1"}, {description: "Item 2"}, {description: "Item 3"}];
        render(<Select items={items} render={'description'} selected={items[0]} onChange={() => {}} />)
        const itemOption = screen.getByText("Item 1");
        expect(itemOption).toBeInTheDocument();
    })

    it('renders select using component items successfully', async () => {
        const item = {description: "Hello world"};
        render(<Select items={[item]} render={(item) => <div>{item.description}</div>} selected={undefined} onChange={() => {}} />)
        const button = screen.getByRole('button');
        await button.click();
        const itemDiv = screen.getByTestId('option-0');
        expect(itemDiv).toBeInTheDocument();
    })

    it('passing a selected component item select that item successfully', async () => {
        const items = [{description: "Item 1"}, {description: "Item 2"}, {description: "Item 3"}];
        render(<Select items={items} render={(item) => <div>{item.description}</div>} selected={items[0]} onChange={() => {}} />)
        const itemOption = screen.getByText("Item 1");
        expect(itemOption).toBeInTheDocument();
    })

    it('renders placeholder successfully', async () => {
        render(<Select items={[]} placeholder={'Placeholder'} selected={undefined} onChange={() => {}} />);
        const placeholder = screen.getByText('Placeholder');
        expect(placeholder).toBeInTheDocument();
    })

    it('dosen\'t click in select not shows the options successfully', async () => {
        render(<Select items={["Item 1", "Item 2"]} selected={undefined} onChange={() => {}} />);
        const options = screen.queryByTestId('select-options');
        expect(options).not.toBeInTheDocument();
    })

    it('click in select shows the options successfully', async () => {
        render(<Select items={["Item 1", "Item 2"]} selected={undefined} onChange={() => {}} />);
        const button = screen.getByRole('button');
        await button.click();
        const options = screen.getByTestId('select-options');
        expect(options).toBeInTheDocument();
    })

    it('on item click returns the item and hide the options successfully', async () => {
        const onChange = jest.fn();
        render(<Select items={["Item 1", "Item 2"]} onChange={onChange} selected={undefined}  />);
        const button = screen.getByRole('button');
        await button.click();
        const buttonItem = screen.getByTestId('option-0');
        await buttonItem.click();
        const options = screen.queryByTestId('select-options');
        expect(onChange).toBeCalledWith("Item 1");
        expect(options).not.toBeInTheDocument();
    })

    it('on click returns if options are open', async () => {
        const onClick = jest.fn();
        render(<Select items={["Item 1", "Item 2"]} onClick={onClick} selected={undefined} onChange={() => {}} />);
        const button = screen.getByRole('button');
        await button.click();
        expect(onClick).toBeCalledWith(true);
    })

    it('renders pretext successfully', async () => {
        const items = [{description: "Item 1"}, {description: "Item 2"}, {description: "Item 3"}];
        render(<Select items={items} render={'description'} selected={items[0]} pretext={'Selected: '} onChange={() => {}}/>)
        const pretext = screen.getByText(/Selected*/);
        expect(pretext).toBeInTheDocument();
    })
})
