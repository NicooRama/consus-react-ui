import React from 'react'
import { useState, useEffect, useRef } from 'react'
import { normalizeDisplay } from '@consus/js-utils'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'
import { ConsusRender } from '../../utils/types'
import { optionsStyles, optionStyles } from './optionsStyles'
import { ButtonWrapper } from '../button-wrapper/ButtonWrapper'
import keycode from 'keycode'
import { isLastElement } from '@consus/js-utils'
import styled from 'styled-components'
// fix

const Container = styled.div<{ minHeight: number }>`
  display: flex;
  position: relative;
  width: -webkit-fill-available;
  width: -moz-available;

  &.fix-firefox {
    width: -moz-available;
  }

  .content {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    line-height: ${(props) => props.minHeight - 3}px;

    .placeholder {
      color: ${({ theme }) => theme.colors.inputs.placeholder};
      font-size: ${({ theme }) => theme.font.size.xs}px;
    }

    .selected-item {
      display: flex;
    }
  }

  .select {
    border: solid 1px ${({ theme }) => theme.colors.inputs.border};
    border-radius: ${({ theme }) => theme.inputs.borderRadius}px;
    cursor: pointer;
    width: inherit;
    background-color: white;
    position: relative;
    color: ${({ theme }) => theme.colors.typographic.solid};

    box-sizing: content-box;
    padding: ${({ theme }) => theme.spacing.sm}px;
    padding-right: 35px;
    font-size: ${({ theme }) => theme.font.size.xs}px;
    display: flex;
    align-items: center;
    min-height: ${({ minHeight }) => minHeight}px;

    &:focus {
      border-color: ${({ theme }) => theme.colors.primary.normal};

      & + .options {
        border-color: ${({ theme }) => theme.colors.primary.normal};
      }
    }

    svg {
      position: absolute;
      top: 30%;
      right: 12px;

      path {
        fill: ${({ theme }) => theme.colors.primary.normal};
      }
    }
  }

  .options {
    ${({ theme }: any) => optionsStyles(theme)}
    div {
      width: inherit;
    }

    .option {
      ${({ theme }: any) => optionStyles(theme)}
    }

    position: absolute;
    z-index: 5;
  }
`

export interface SelectProps {
  className?: string
  pretext?: string
  items: any[]
  selected: any
  placeholder?: string
  onChange: (item: any) => any
  onClick?: (showOptions: boolean) => any
  render?: ConsusRender
  id?: string
  minHeight?: number
}

export const Select: React.FC<SelectProps> = ({
  className,
  pretext = '',
  items,
  selected,
  placeholder,
  onChange,
  onClick = () => {},
  render,
  id,
  minHeight = 16,
  ...props
}) => {
  const [showOptions, setShowOptions] = useState(false)
  const [selectedItem, setSelectedItem] = useState(selected)
  const ref = useRef({})

  useEffect(() => {
    setSelectedItem(selected)
  }, [selected])

  const selectItem = (item: any) => {
    setSelectedItem(item)
    setShowOptions(false)
    onChange(item)
  }

  const onSelectClick = () => {
    onClick(!showOptions)
    setShowOptions(!showOptions)
  }

  const normalizedDisplay = normalizeDisplay(render)

  const selectHandleKey = (e: any) => {
    if (keycode.codes.down === e.keyCode) {
      if (!showOptions) {
        setShowOptions(true)
        return
      }
      if (items.length > 0) {
        ;((ref.current as any)[0] as any).focus()
      }
    }
  }

  const optionHandleKey = (index: number) => (e: any) => {
    //Flecha hacia abajo
    if (keycode.codes.down === e.keyCode) {
      if (isLastElement(items, index)) {
        setShowOptions(false)
        return
      }
      ;((ref.current as any)[index + 1] as any).focus()
      return
    }
    //Flecha hacia arriba
    if (keycode.codes.up === e.keyCode && index > 0) {
      ;((ref.current as any)[index - 1] as any).focus()
      return
    }

    if (isLastElement(items, index) && keycode.codes.tab === e.keyCode) {
      setShowOptions(false)
    }
  }

  return (
    <Container minHeight={minHeight} className={`${className} fix-firefox`} id={id} {...props}>
      <ButtonWrapper
        className={`select ${showOptions ? 'focus' : ''}`}
        onClick={onSelectClick}
        onKeyDown={selectHandleKey}
        type={'button'}
      >
        <div className="content">
          <div className={selectedItem === null || selectedItem === undefined ? 'placeholder' : ''}>
            {selectedItem === null || selectedItem === undefined ? (
              placeholder
            ) : (
              <div className={'selected-item'}>
                <div>{pretext}</div>
                {selectedItem ? normalizedDisplay(selectedItem) : ''}
              </div>
            )}
          </div>
          <FontAwesomeIcon icon={faChevronDown} />
        </div>
      </ButtonWrapper>
      {showOptions && (
        <div data-testid={'select-options'} className="options">
          {items.map((item, index) => (
            <ButtonWrapper
              data-testid={`option-${index}`}
              key={index}
              className="option"
              onClick={() => selectItem(item)}
              buttonRef={(buttonRef: any) => ((ref.current as any)[index] = buttonRef)}
              onKeyDown={optionHandleKey(index)}
              type={'button'}
            >
              {normalizedDisplay(item)}
            </ButtonWrapper>
          ))}
        </div>
      )}
    </Container>
  )
}
