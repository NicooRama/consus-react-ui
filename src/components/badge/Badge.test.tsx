import {Badge} from "./Badge";
import {render} from "../../../test/test.utils";

describe('<Badge />', () => {
    it('renders <Badge /> successfully', () => {
        const {getByTestId} = render(<Badge data-testid={'badge'}>Badge</Badge>)
        expect(getByTestId('badge')).toBeInTheDocument();
    });
});
