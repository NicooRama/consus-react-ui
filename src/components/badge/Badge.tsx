import React from 'react'
import { css } from 'styled-components'
import styled from 'styled-components'
import { ThemeColorIndex } from '../../styles/theme.interface'
import { Text } from '../text/Text'
import { getIn } from '../../utils/utils'

const Container = styled.div<{
  color: ThemeColorIndex
  border: ThemeColorIndex
  textColor?: ThemeColorIndex
  variant?: BadgeVariant
  clickeable?: 0 | 1
}>`
  width: fit-content;
  padding: ${({ theme }: any) => `${theme.spacing.xxs}px ${theme.spacing.xs}px`};
  border-radius: 12px;
  background-color: ${({ theme, color }: any) => getIn(theme.colors, color)};
  border: 1px solid ${({ theme, border }: any) => getIn(theme.colors, border)};
  display: flex;
  align-items: center;

  ${({ clickeable }: any) =>
    clickeable &&
    css`
      cursor: pointer;
    `}
  p {
    color: ${({ theme, textColor }: any) => (textColor ? getIn(theme.colors, textColor) : 'white')};
  }

  ${({ variant, theme }: any) =>
    variant &&
    variant !== 'custom' &&
    css`
      background-color: ${getIn(theme.colors, `${variant}.light`)};
      border: 1px solid ${getIn(theme.colors, `${variant}.darkest`)};

      p {
        color: ${getIn(theme.colors, `${variant}.darkest`)};
      }
    `}
`

export type BadgeVariant = 'custom' | 'success' | 'error' | 'warn' | 'info'

export interface BadgeProps {
  onClick?: (event?: any) => void
  color?: ThemeColorIndex
  border?: ThemeColorIndex
  textColor?: ThemeColorIndex
  className?: string
  variant?: BadgeVariant
  children: any
}

export const Badge: React.FC<BadgeProps> = ({
  variant = 'custom',
  children,
  onClick,
  color = 'primary.normal',
  border = color,
  textColor,
  className = '',
  ...props
}) => {
  const handleClick = onClick || (() => {})

  return (
    <Container
      variant={variant}
      role={'button'}
      color={color}
      border={border}
      textColor={textColor}
      onClick={handleClick}
      clickeable={!!onClick ? 1 : 0}
      className={className}
    >
      <Text {...props} size={'xxs'}>
        {children}
      </Text>
    </Container>
  )
}
