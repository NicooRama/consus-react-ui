import type {Meta, StoryObj} from '@storybook/react';
import {Badge} from './Badge';

const meta: any = {
    title: 'Commons/Badge',
    component: Badge,
    tags: ['autodocs'],
} satisfies Meta<typeof Badge>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        children: 'Badge'
    },
};

export const Success: Story = {
    args: {
        ...Primary.args,
        variant: 'success'
    },
};
export const Error: Story = {
    args: {
        ...Primary.args,
        variant: 'error'
    },
};

export const Info: Story = {
    args: {
        ...Primary.args,
        variant: 'info'
    },
};

export const Warn: Story = {
    args: {
        ...Primary.args,
        variant: 'warn'
    },
};
