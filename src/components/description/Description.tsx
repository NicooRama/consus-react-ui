import React from "react"
import styled from "styled-components";

const Container = styled.div`
    word-break: break-all;
  
    p:first-of-type {
      margin: 0;
    }
  
    p, ul, ol, pre, blockquote {
      margin-top: ${({theme}: any) => theme.spacing.xs}px;
      margin-bottom: ${({theme}: any) => theme.spacing.xs}px;
    }
    
    pre {
      background-color: ${({theme}: any) => theme.colors.gray.lightest};
      padding: ${({theme}: any) => theme.spacing.xs}px;
      border-radius: ${({theme}: any) => theme.borderRadius}px;
    }
  
    blockquote {
      border-left: 4px solid ${({theme}: any) => theme.colors.gray.lighten};
      color: ${({theme}: any) => theme.colors.gray.darken};
      font-family: 'Hoefler Text', 'Georgia', serif;
      font-style: italic;
      margin-left: 0;
      margin-right: 0;
      padding: ${({theme}: any) => theme.spacing.xs}px ${({theme}: any) => theme.spacing.sm}px;
    }
  
    ul, ol {
      padding-left: 24px;
      ul, ol {
        margin: 0;
      }
    }
`

export interface DescriptionProps {
    content: string;
    }

export const Description: React.FC<DescriptionProps> = ({
                                content,
                                ...props
                            }) => {
    return (<Container dangerouslySetInnerHTML={{ __html: content }}  {...props}>
    </Container>)
};
