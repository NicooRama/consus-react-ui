import {Description} from "./Description";
import {render} from "../../../test/test.utils";

describe('<Description />', () => {
    it('renders <Description /> successfully', () => {
        const {getByTestId} = render(<Description data-testid={'description'} content={'contenido'}/>)
        expect(getByTestId('description')).toBeInTheDocument();
    });
});
