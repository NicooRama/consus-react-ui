import type { Meta, StoryObj } from '@storybook/react'
import { Description } from './Description'

const meta: any = {
  title: 'Commons/Description',
  component: Description,
  tags: ['autodocs'],
} satisfies Meta<typeof Description>

export default meta
type Story = StoryObj<typeof meta>

export const Primary: Story = {
  args: {
    content: 'Write some html',
  },
}
