import type {Meta, StoryObj} from '@storybook/react';
import styled, {css} from 'styled-components';
import {Text} from "./components/text/Text";

import * as AllBadges from "./components/badge/Badge.stories";
import {Badge} from "./components/badge/Badge";

import * as AllActionsPanel from "./components/actions-panel/ActionsPanel.stories";
import {ActionsPanel} from "./components/actions-panel/ActionsPanel";

import * as AllActiveIndicator from "./components/active-indicator/ActiveIndicator.stories";
import {ActiveIndicator} from "./components/active-indicator/ActiveIndicator";

import * as AllBackofficeTable from "./components/backoffice/BackofficeTable/BackofficeTable.stories";
import {BackofficeTable} from "./components/backoffice/BackofficeTable/BackofficeTable";

import * as AllBackofficeTagList from "./components/backoffice/BackofficeTagList/BackofficeTagList.stories";
import {BackofficeTagList} from "./components/backoffice/BackofficeTagList/BackofficeTagList";

import * as AllButton from "./components/button/Button.stories";
import {Button} from "./components/button/Button";

import * as AllButtonTabs from "./components/button-tabs/ButtonTabs.stories";
import {ButtonTabs} from "./components/button-tabs/ButtonTabs";

import * as AllButtonWrapper from "./components/button-wrapper/ButtonWrapper.stories";
import {ButtonWrapper} from "./components/button-wrapper/ButtonWrapper";

import * as AllButtonsPanel from "./components/buttons-panel/ButtonsPanel.stories";
import {ButtonsPanel} from "./components/buttons-panel/ButtonsPanel";

import * as AllButtonsPanelSkeleton
    from "./components/buttons-panel/buttons-panel-skeleton/ButtonsPanelSkeleton.stories";
import {ButtonsPanelSkeleton} from "./components/buttons-panel/buttons-panel-skeleton/ButtonsPanelSkeleton";

import * as AllCard from "./components/card/Card.stories";
import {Card} from "./components/card/Card";

import * as AllCardFooter from "./components/card-footer/CardFooter.stories";
import {CardFooter} from "./components/card-footer/CardFooter";

import * as AllCheckOptionsSelector from "./components/check-options-selector/CheckOptionsSelector.stories";
import {CheckOptionsSelector} from "./components/check-options-selector/CheckOptionsSelector";

import * as AllCheckbox from "./components/checkbox/Checkbox.stories";
import {Checkbox} from "./components/checkbox/Checkbox";

import * as AllCircleImage from "./components/circle-image/CircleImage.stories";
import {CircleImage} from "./components/circle-image/CircleImage";

import * as AllCollapse from "./components/collapse/Collapse.stories";
import {Collapse} from "./components/collapse/Collapse";

import * as AllContainer from "./components/container/Container.stories";
import {Container} from "./components/container/Container";

import * as AllDatePicker from "./components/date-picker/DatePicker.stories";
import {DatePicker} from "./components/date-picker/DatePicker";

import * as AllRangeDatePicker from "./components/date-picker/RangeDatePicker.stories";
import {RangeDatePicker} from "./components/date-picker/RangeDatePicker";

import * as AllDescription from "./components/description/Description.stories";
import {Description} from "./components/description/Description";

import * as AllDetailsHeader from "./components/details-header/DetailsHeader.stories";
import {DetailsHeader} from "./components/details-header/DetailsHeader";

import * as AllDetailsHeaderSkeleton
    from "./components/details-header/details-header-skeleton/DetailsHeaderSkeleton.stories";
import {DetailsHeaderSkeleton} from "./components/details-header/details-header-skeleton/DetailsHeaderSkeleton";

import * as AllDetailsTitleHeader from "./components/details-title-header/DetailsTitleHeader.stories";
import {DetailsTitleHeader} from "./components/details-title-header/DetailsTitleHeader";

import * as AllDetailsTitleHeaderSkeleton
    from "./components/details-title-header/details-title-header-skeleton/DetailsTitleHeaderSkeleton.stories";
import {
    DetailsTitleHeaderSkeleton
} from "./components/details-title-header/details-title-header-skeleton/DetailsTitleHeaderSkeleton";

import * as AllDivider from "./components/divider/Divider.stories";
import {Divider} from "./components/divider/Divider";

import * as AllDraggable from "./components/draggable/DragAndDrop.stories";

import * as AllEmptyMessageCard from "./components/empty-message-card/EmptyMessageCard.stories";
import {EmptyMessageCard} from "./components/empty-message-card/EmptyMessageCard";

import * as AllEntityListCard from "./components/entity-list-card/EntityListCard.stories";
import {EntityListCard} from "./components/entity-list-card/EntityListCard";

import * as AllErrorImage from "./components/error-image/ErrorImage.stories";
import {ErrorImage} from "./components/error-image/ErrorImage";

import * as AllFormFrame from "./components/form-frame/FormFrame.stories";
import {FormFrame} from "./components/form-frame/FormFrame";

import * as AllHeader from "./components/header/Header.stories";
import {Header} from "./components/header/Header";

import * as AllHeaderTitleCard from "./components/header-title-card/HeaderTitleCard.stories";
import {HeaderTitleCard} from "./components/header-title-card/HeaderTitleCard";

import * as AllIcon from "./components/icon/Icon.stories";
import {Icon} from "./components/icon/Icon";

import * as AllIconButton from "./components/icon-button/IconButton.stories";
import {IconButton} from "./components/icon-button/IconButton";

import * as AllMinusButton from "./components/icon-button/MinusButton.stories";
import {MinusButton} from "./components/icon-button/MinusButton";

import * as AllPlusButton from "./components/icon-button/PlusButton.stories";
import {PlusButton} from "./components/icon-button/PlusButton";

import * as AllSubstractButton from "./components/icon-button/SubstractButton.stories";
import {SubstractButton} from "./components/icon-button/SubstractButton";

import * as AllToggleButton from "./components/icon-button/ToggleButton.stories";
import {ToggleButton} from "./components/icon-button/ToggleButton";

import * as AllTrashButton from "./components/icon-button/TrashButton.stories";
import {TrashButton} from "./components/icon-button/TrashButton";

import * as AllEditButton from "./components/icon-button/EditButton.stories";
import {EditButton} from "./components/icon-button/EditButton";

import * as AllImage from "./components/image/Image.stories";
import {Image} from "./components/image/Image";

import * as AllInfoCard from "./components/info-card/InfoCard.stories";
import {InfoCard} from "./components/info-card/InfoCard";

import * as AllInput from "./components/input/Input.stories";
import {Input} from "./components/input/Input";

import * as AllInteractiveButton from "./components/interactive-button/InteractiveButton.stories";
import {InteractiveButton} from "./components/interactive-button/InteractiveButton";

import * as AllInteractiveButtonsPanel from "./components/interactive-buttons-panel/InteractiveButtonsPanel.stories";
import {InteractiveButtonsPanel} from "./components/interactive-buttons-panel/InteractiveButtonsPanel";

import * as AllInteractiveRemoveButton from "./components/interactive-remove-button/InteractiveRemoveButton.stories";
import {InteractiveRemoveButton} from "./components/interactive-remove-button/InteractiveRemoveButton";

import * as AllInteractiveRemovePopover from "./components/interactive-remove-popover/InteractiveRemovePopover.stories";
import {InteractiveRemovePopover} from "./components/interactive-remove-popover/InteractiveRemovePopover";

import * as AllList from "./components/list/List.stories";
import {List} from "./components/list/List";

import * as AllListItems from "./components/list/list-items/ListItems.stories";
import {ListItems} from "./components/list/list-items/ListItems";

import * as AllListCard from "./components/list-card/ListCard.stories";
import {ListCard} from "./components/list-card/ListCard";

import * as AllListPaginationBar from "./components/list-pagination-bar/ListPaginationBar.stories";
import {ListPaginationBar} from "./components/list-pagination-bar/ListPaginationBar";

import * as AllLoader from "./components/loader/Loader.stories";
import {Loader} from "./components/loader/Loader";

import * as AllLoaderWrapper from "./components/loader-wrapper/LoaderWrapper.stories";
import {LoaderWrapper} from "./components/loader-wrapper/LoaderWrapper";

import * as AllModalHeader from "./components/modal/modal-header/ModalHeader.stories";
import {ModalHeader} from "./components/modal/modal-header/ModalHeader";

import * as AllNavigationPanel from "./components/navigation-panel/NavigationPanel.stories";
import {NavigationPanel} from "./components/navigation-panel/NavigationPanel";

import * as AllNavigationPanelSkeleton from "./components/navigation-panel/NavigationPanelSkeleton.stories";
import {NavigationPanelSkeleton} from "./components/navigation-panel/NavigationPanelSkeleton";

import * as AllNewEntityCard from "./components/new-entity-card/NewEntityCard.stories";
import {NewEntityCard} from "./components/new-entity-card/NewEntityCard";

import * as AllNotFound from "./components/not-found/NotFound.stories";
import {NotFound} from "./components/not-found/NotFound";

import * as AllNotFoundCard from "./components/not-found/not-found-card/NotFoundCard.stories";
import {NotFoundCard} from "./components/not-found/not-found-card/NotFoundCard";

import * as AllNotFoundPage from "./components/not-found/not-found-page/NotFoundPage.stories";
import {NotFoundPage} from "./components/not-found/not-found-page/NotFoundPage";

import * as AllCheckOptionsSlider from "./components/options-slider/CheckOptionsSlider/CheckOptionsSlider.stories";
import {CheckOptionsSlider} from "./components/options-slider/CheckOptionsSlider/CheckOptionsSlider";

import * as AllRadioOptionsSlider from "./components/options-slider/RadioOptionsSlider/RadioOptionsSlider.stories";
import {RadioOptionsSlider} from "./components/options-slider/RadioOptionsSlider/RadioOptionsSlider";

import * as AllPaginationBar from "./components/pagination-bar/PaginationBar.stories";
import {PaginationBar} from "./components/pagination-bar/PaginationBar";

import * as AllConfirmationPopoverButton from "./components/popover/ConfirmationPopoverButton.stories";
import {ConfirmationPopoverButton} from "./components/popover/ConfirmationPopoverButton";

import * as AllPlusPopoverButtonForm from "./components/popover/PlusPopoverButtonForm.stories";
import {PlusPopoverButtonForm} from "./components/popover/PlusPopoverButtonForm";

import * as AllPopover from "./components/popover/Popover.stories";
import {Popover} from "./components/popover/Popover";

import * as AllPopoverContent from "./components/popover/PopoverContent.stories";
import {PopoverContent} from "./components/popover/PopoverContent";

import * as AllPopoverHeader from "./components/popover/PopoverHeader.stories";
import {PopoverHeader} from "./components/popover/PopoverHeader";

import * as AllProgressBar from "./components/progress-bar/ProgressBar.stories";
import {ProgressBar} from "./components/progress-bar/ProgressBar";

import * as AllRadioButton from "./components/radio-button/RadioButton.stories";
import {RadioButton} from "./components/radio-button/RadioButton";

import * as AllRadioButtonGroup from "./components/radio-button-group/RadioButtonGroup.stories";
import {RadioButtonGroup} from "./components/radio-button-group/RadioButtonGroup";

import * as AllResultMessage from "./components/result-message/ResultMessage.stories";
import {ResultMessage} from "./components/result-message/ResultMessage";

import * as AllSelect from "./components/select/Select.stories";
import {Select} from "./components/select/Select";

import * as AllSelectEntityCard from "./components/select-entity-card/SelectEntityCard.stories";
import {SelectEntityCard} from "./components/select-entity-card/SelectEntityCard";

import * as AllSkeletonButton from "./components/skeletons/skeleton-button/SkeletonButton.stories";
import {SkeletonButton} from "./components/skeletons/skeleton-button/SkeletonButton";

import * as AllSkeletonButtonList from "./components/skeletons/skeleton-button-list/SkeletonButtonList.stories";
import {SkeletonButtonList} from "./components/skeletons/skeleton-button-list/SkeletonButtonList";

import * as AllSkeletonCard from "./components/skeletons/skeleton-card/SkeletonCard.stories";
import {SkeletonCard} from "./components/skeletons/skeleton-card/SkeletonCard";

import * as AllSkeletonCardList from "./components/skeletons/skeleton-card-list/SkeletonCardList.stories";
import {SkeletonCardList} from "./components/skeletons/skeleton-card-list/SkeletonCardList";

import * as AllSkeletonCircleImage from "./components/skeletons/skeleton-circle-image/SkeletonCircleImage.stories";
import {SkeletonCircleImage} from "./components/skeletons/skeleton-circle-image/SkeletonCircleImage";

import * as AllSkeletonInput from "./components/skeletons/skeleton-input/SkeletonInput.stories";
import {SkeletonInput} from "./components/skeletons/skeleton-input/SkeletonInput";

import * as AllSkeletonLabel from "./components/skeletons/skeleton-label/SkeletonLabel.stories";
import {SkeletonLabel} from "./components/skeletons/skeleton-label/SkeletonLabel";

import * as AllSkeletonList from "./components/skeletons/skeleton-list/SkeletonList.stories";
import {SkeletonList} from "./components/skeletons/skeleton-list/SkeletonList";

import * as AllSkeletonLoader from "./components/skeletons/skeleton-loader/SkeletonLoader.stories";
import {SkeletonLoader} from "./components/skeletons/skeleton-loader/SkeletonLoader";

import * as AllSkeletonPagination from "./components/skeletons/skeleton-pagination/SkeletonPagination.stories";
import {SkeletonPagination} from "./components/skeletons/skeleton-pagination/SkeletonPagination";

import * as AllSkeletonRadioButtonGroup
    from "./components/skeletons/skeleton-radio-button-group/SkeletonRadioButtonGroup.stories";
import {SkeletonRadioButtonGroup} from "./components/skeletons/skeleton-radio-button-group/SkeletonRadioButtonGroup";

import * as AllSkeletonSquare from "./components/skeletons/skeleton-square/SkeletonSquare.stories";
import {SkeletonSquare} from "./components/skeletons/skeleton-square/SkeletonSquare";

import * as AllSkeletonSquareCard from "./components/skeletons/skeleton-square-card/SkeletonSquareCard.stories";
import {SkeletonSquareCard} from "./components/skeletons/skeleton-square-card/SkeletonSquareCard";

import * as AllSkeletonText from "./components/skeletons/skeleton-text/SkeletonText.stories";
import {SkeletonText} from "./components/skeletons/skeleton-text/SkeletonText";

import * as AllSpacing from "./components/spacing/Spacing.stories";
import {Spacing} from "./components/spacing/Spacing";

import * as AllStepper from "./components/stepper/Stepper.stories";
import {Stepper} from "./components/stepper/Stepper";

import * as AllSwitch from "./components/switch/Switch.stories";
import {Switch} from "./components/switch/Switch";

import * as AllTable from "./components/table/Table.stories";
import {Table} from "./components/table/Table";

import * as AllTag from "./components/tag/Tag.stories";
import {Tag} from "./components/tag/Tag";

import * as AllTagList from "./components/tag-list/TagList.stories";
import {TagList} from "./components/tag-list/TagList";

import * as AllText from "./components/text/Text.stories";

import * as AllToast from "./components/toast/Toast.stories";

import * as AllVideoFrame from "./components/video-frame/VideoFrame.stories";
import {VideoFrame} from "./components/video-frame/VideoFrame";

const Generic = () => <div></div>;

const meta: Meta<typeof Generic> = {
    title: 'All',
    component: Generic,
};
export default meta;

type Story = StoryObj<typeof Generic>;

const VariantsContainer = styled.div<{ direction: string }>`
    display: flex;
    gap: 12px;
    flex-direction: column;
    justify-content: start;

    ${({direction}: any) =>
            direction === 'row' &&
            css`
                flex-direction: row;
                align-items: center;
            `}
`

const VariantContainer = styled.div`
    display: grid;
    grid-template-columns: 1fr;
    gap: 8px;
`

const ComponentContainer = styled.div`
    display: flex;
    gap: 12px;
    flex-direction: column;
    background-color: white;
    padding: 1rem;
    margin: 0 -1rem;
    border-top: 1px solid #e0e0e0;
    border-bottom: 1px solid #e0e0e0;
`

interface RenderStoryOptions {
    layout?: {
        direction: 'column' | 'row'
    },
    name?: string
}

const renderStories = (module: any, Component: any, options: RenderStoryOptions = {}) => {
    const {layout = {direction: 'column'}, name = Component.name} = options;

    const componentVariantsKeys = Object.keys(module).filter((key) => {
        return key !== 'default' && key !== '__namedExportsOrder';
    });

    const componentVariants = componentVariantsKeys.map((key) => {
        const {args, decorators = [], render} = module[key];
        if ((Component.name === 'Generic' || args === undefined) && !!render) {
            return render();
        }
        if (decorators.length > 0) {
            return decorators.reduce((acc: any, decorator: any) => {
                return () => decorator(acc)
            }, () => <VariantContainer>
                {
                    componentVariantsKeys.length > 1 && <Text size={'xs'} color={'fade'}>{key}</Text>
                }
                <Component {...args} />
            </VariantContainer>)();
        }
        return (<VariantContainer>
            {
                componentVariantsKeys.length > 1 && <Text size={'xs'} color={'fade'}>{key}</Text>
            }
            <Component {...args} />
        </VariantContainer>)
    });

    return <ComponentContainer key={name}>
        <Text size={'lg'} weight={'semiBold'}>{name}</Text>
        <VariantsContainer direction={layout.direction}>
            {componentVariants}
        </VariantsContainer>
    </ComponentContainer>
}

const ComponentsContainer = styled.div`
    display: flex;
    gap: 32px;
    flex-direction: column;
`

const storiesToRender = [
    [AllBadges, Badge, {layout: {direction: 'row'}}],
    [AllActionsPanel, ActionsPanel],
    [AllActiveIndicator, ActiveIndicator],
    [AllBackofficeTable, BackofficeTable],
    [AllBackofficeTagList, BackofficeTagList, {name: 'BackofficeTagList'}],
    [AllButton, Button],
    [AllButtonTabs, ButtonTabs],
    [AllButtonWrapper, ButtonWrapper],
    [AllButtonsPanel, ButtonsPanel],
    [AllButtonsPanelSkeleton, ButtonsPanelSkeleton],
    [AllCard, Card],
    [AllCardFooter, CardFooter],
    [AllCheckOptionsSelector, CheckOptionsSelector],
    [AllCheckbox, Checkbox],
    [AllCircleImage, CircleImage],
    [AllCollapse, Collapse],
    [AllContainer, Container],
    [AllDatePicker, DatePicker],
    [AllRangeDatePicker, RangeDatePicker],
    [AllDescription, Description],
    [AllDetailsHeader, DetailsHeader],
    [AllDetailsHeaderSkeleton, DetailsHeaderSkeleton],
    [AllDetailsTitleHeader, DetailsTitleHeader],
    [AllDetailsTitleHeaderSkeleton, DetailsTitleHeaderSkeleton],
    [AllDivider, Divider],
    [AllDraggable, Generic, {name: 'DragAndDrop'}],
    [AllEmptyMessageCard, EmptyMessageCard],
    [AllEntityListCard, EntityListCard],
    [AllErrorImage, ErrorImage],
    [AllFormFrame, FormFrame],
    [AllHeader, Header],
    [AllHeaderTitleCard, HeaderTitleCard],
    [AllIcon, Icon],
    [AllIconButton, IconButton],
    [AllMinusButton, MinusButton],
    [AllPlusButton, PlusButton],
    [AllSubstractButton, SubstractButton],
    [AllToggleButton, ToggleButton],
    [AllTrashButton, TrashButton],
    [AllEditButton, EditButton],
    [AllImage, Image],
    [AllInfoCard, InfoCard],
    [AllInput, Input],
    [AllInteractiveButton, InteractiveButton],
    [AllInteractiveButtonsPanel, InteractiveButtonsPanel],
    [AllInteractiveRemoveButton, InteractiveRemoveButton],
    [AllInteractiveRemovePopover, InteractiveRemovePopover],
    [AllList, List],
    [AllListItems, ListItems],
    [AllListCard, ListCard],
    [AllListPaginationBar, ListPaginationBar],
    [AllLoader, Loader, {layout: {direction: 'row'}}],
    [AllLoaderWrapper, LoaderWrapper],
    [AllModalHeader, ModalHeader],
    [AllNavigationPanel, NavigationPanel],
    [AllNavigationPanelSkeleton, NavigationPanelSkeleton],
    [AllNewEntityCard, NewEntityCard],
    [AllNotFound, NotFound],
    [AllNotFoundCard, NotFoundCard],
    [AllNotFoundPage, NotFoundPage],
    [AllCheckOptionsSlider, CheckOptionsSlider],
    [AllRadioOptionsSlider, RadioOptionsSlider],
    [AllPaginationBar, PaginationBar],
    [AllConfirmationPopoverButton, ConfirmationPopoverButton],
    [AllPlusPopoverButtonForm, PlusPopoverButtonForm],
    [AllPopover, Popover],
    [AllPopoverHeader, PopoverHeader],
    [AllPopoverContent, PopoverContent],
    [AllProgressBar, ProgressBar],
    [AllRadioButton, RadioButton],
    [AllRadioButtonGroup, RadioButtonGroup],
    [AllResultMessage, ResultMessage],
    [AllSelect, Select],
    [AllSelectEntityCard, SelectEntityCard],
    [AllSkeletonButton, SkeletonButton],
    [AllSkeletonButtonList, SkeletonButtonList],
    [AllSkeletonCard, SkeletonCard],
    [AllSkeletonCardList, SkeletonCardList],
    [AllSkeletonCircleImage, SkeletonCircleImage],
    [AllSkeletonInput, SkeletonInput],
    [AllSkeletonLabel, SkeletonLabel],
    [AllSkeletonList, SkeletonList],
    [AllSkeletonLoader, SkeletonLoader],
    [AllSkeletonPagination, SkeletonPagination],
    [AllSkeletonRadioButtonGroup, SkeletonRadioButtonGroup],
    [AllSkeletonSquare, SkeletonSquare],
    [AllSkeletonSquareCard, SkeletonSquareCard],
    [AllSkeletonText, SkeletonText],
    [AllSpacing, Spacing],
    [AllStepper, Stepper],
    [AllSwitch, Switch],
    [AllTable, Table],
    [AllTag, Tag],
    [AllTagList, TagList],
    [AllText, Text],
    [AllToast, Generic, {name: 'Toast'}],
    [AllVideoFrame, VideoFrame],
]

export const All: Story = {
    render: () => <ComponentsContainer>
        {
            storiesToRender.map(([module, Component, layout]) => {
                return renderStories(module, Component, layout as any)
            })
        }
    </ComponentsContainer>,
    // parameters: { options: { showPanel: false } }
}
