import type {Decorator, Preview} from "@storybook/react";
import {theme} from "../src/utils/theme";
import {ThemeProvider} from "styled-components";
import {ConsusGlobalStyles} from "../src/components/global-styles/ConsusGlobalStyles";
import {disableDummyArgTypes} from "../src/storybook.utils";

const preview: Preview = {
    parameters: {
        actions: {argTypesRegex: "^on[A-Z].*"},
        controls: {
            matchers: {
                color: /(background|color)$/i,
                date: /Date$/,
            },
        },
    },
    argTypes: {
        ...disableDummyArgTypes
    },
};

const withTheme: Decorator = (StoryFn, context) => {
    return (
        <ThemeProvider theme={theme}>
            <StoryFn/>
        </ThemeProvider>
    )
}

const withGlobalStyles: Decorator = (StoryFn, context) => {
    return (
        <div>
            <ThemeProvider theme={theme}>
                <ConsusGlobalStyles />
            </ThemeProvider>
            <StoryFn/>
        </div>
    )
}

export const decorators = [
    withTheme,
    withGlobalStyles
]

export default preview;
