import {render as testingLibraryRender} from '@testing-library/react';
import {theme} from "../src/utils/theme";
import {ThemeProvider} from "styled-components";
import {MemoryRouter} from 'react-router-dom'

const TestThemeProvider = ({children}: any) =>
    <ThemeProvider theme={theme}>
        {children}
    </ThemeProvider>

export const RouterProvider = ({children}: any) => (
    <TestThemeProvider>
        <MemoryRouter initialEntries={["/my/initial/route"]}>
            {children}
        </MemoryRouter>
    </TestThemeProvider>
)

export const render = (component: any, options = {}) => {
    return testingLibraryRender(component, {wrapper: TestThemeProvider, ...options})
}

export const renderWithRouter = (component: any, options = {}) => {
    return testingLibraryRender(component, {wrapper: RouterProvider, ...options})
}
